package com.proyectmanager.searches;

import com.proyectmanager.inputs.InputConsultaActividades;
import com.proyectmanager.inputs.InputFindGeneralSF;
import com.proyectmanager.methods.ConsultaActividades;
import com.proyectmanager.methods.ConsultaProyectos;

import com.proyectmanager.methods.DetalleCSP;
import com.proyectmanager.methods.DetalleObjetoSF;
import com.proyectmanager.methods.FinalizaProyecto;
import com.proyectmanager.methods.FindGeneralSF;
import com.proyectmanager.objects.CotSitioPlan;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("ProyectSearches_ENL")
public class ProyectSearches_ENL {
    public ProyectSearches_ENL() {
        super();
    }
    
   // public static void main(String[] args) {

    /* consultaProyectos_PMO("{\n" +
         "  \"Login\" : {\n" +
         "    \"User\" : \"25631\",\n" +
         "    \"Password\" : \"Middle100$\",\n" +
         "    \"Ip\" : \"10\"\n" +
         "  },\n" +
         "  \"Tipo_bandeja\" : \"FC\",\n" +
        "   \"MesConsulta\" : \"2020-06-01\",\n" +
         "  \"Id_pm\" : \"a0hQ0000004LO9NIAW\"\n" +
         "}");*/
  
  
    /* findGeneralSF("{\n" + 
     "  \"Login\" : {\n" + 
     "    \"User\" : \"User1\",\n" + 
     "    \"Password\" : \"Password2\",\n" + 
     "    \"Ip\" : \"Ip3\"\n" + 
     "  },\n" + 
     "  \"parametroBusqueda\" : \"02002*\"\n" + 
     "}");*/
  
        /* long inicio = System.currentTimeMillis();*/
          
         
         
      /* consultaActividades("{\n" + 
        "  \"Login\" : {\n" + 
        "    \"User\" : \"User1\",\n" + 
        "    \"Password\" : \"Password2\",\n" + 
        "    \"Ip\" : \"Ip3\"\n" + 
        "  },\n" + 
        "  \"id_cuenta\" : \"001Q000001LBhfuIAD\",\n" + 
        "  \"id_csp\" : \"a0KQ0000009DM0FMAW\",\n" + 
        "  \"id_pm\" : \"a0hQ0000004LO9NIAW\",\n" + 
        "  \"folio_OS\" : \"OS-25708\"\n" + 
        "}");*/
          /*
         long fin = System.currentTimeMillis();
          
         double tiempo = (double) ((fin - inicio)/1000);
          
         System.out.println(tiempo +" segundos");
          */
         
         /*detalleCSP("{\n" + 
         "  \"Login\" : {\n" + 
         "    \"User\" : \"25631\",\n" + 
         "    \"Password\" : \"Middle100$\",\n" + 
         "    \"Ip\" : \"10\"\n" + 
         "  },\n" + 
         "  \"idOt\" : \"1041\",\n" + 
         "  \"idUnidadNegocio\" : \"2\"\n" +
         "}");*/
         
      /*   finalizaProyecto("{\n" + 
         "  \"Login\" : {\n" + 
         "    \"User\" : \"25631\",\n" + 
         "    \"Password\" : \"Middle100$\",\n" + 
         "    \"Ip\" : \"10\"\n" + 
         "  },\n" + 
         "  \"Id_cs\" : \"a124N000009PR8IQAW\",\n" + 
         "  \"Id_cot\" : \"a064N00000bpIbqQAE\"\n" + 
         "}");*/
    
     /* detalleObjetoSF("{\n" + 
      "	\"Loguin\": {\n" + 
      "	    \"User\": \"Middle100$\",\n" + 
      "	    \"Password\": \"25631\",\n" + 
      "	    \"IP\": \"12.0.0.1\"\n" + 
      "	},\n" + 
      "	\"idObjectSF\": \"0016100000O05qN\",\n" + 
      "	\"typeObjectSF\": \"CU\"\n" + 
      "}");*/     
  //  }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("FindGeneralSF")
    public Response findGeneralSF(String InputFindGeneralSF){
        return new FindGeneralSF().findGeneralSF(InputFindGeneralSF);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaProyectosPM")
    public Response consultaProyectos_PMO(String inputConsultaProyectos){
        return new ConsultaProyectos().consultaProyectos(inputConsultaProyectos);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("ConsultaActividadesPM")
    public Response consultaActividades(String InputConsultaActividades){
       return new ConsultaActividades().consultaActividades(InputConsultaActividades);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("DetalleInstalacion")
    public Response detalleCSP(String InputDetalleCSP){
        return new DetalleCSP().consultaDetalleInstalacion(InputDetalleCSP);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("FinalizaProyectoPM")
    public Response finalizaProyecto(String InputFinalizaProyecto){
        return new FinalizaProyecto().finalizaProyecto(InputFinalizaProyecto);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    @Path("DetalleObjetosSF")
    public Response detalleObjetoSF(String InputDetalleObjetoSF){
        return new DetalleObjetoSF().detalleObjetoSF(InputDetalleObjetoSF);                   
    }
}
