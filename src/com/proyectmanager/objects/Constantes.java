package com.proyectmanager.objects;

import com.proyectmanager.utilities.ConexionDB;
import com.proyectmanager.utilities.CrudFFM;
import com.proyectmanager.utilities.CrudSF;
import com.proyectmanager.utilities.Utilerias;

import java.util.Properties;

import weblogic.ejbgen.Select;

public class Constantes {
    
    public static final Utilerias UTILS = new Utilerias();
    public static final ConexionDB CONECTION_DB = new ConexionDB();
    public static final CrudSF CRUD_SF = new CrudSF();
    public static final CrudFFM CRUD_FFM = new CrudFFM();
    public static final String KEY_URL_DASHBOARD = "URL_DASHBOARD_RC";
    //----- contantes ws
    public static final String PATH_PROPERTIES = "/resources/Configuracion.properties";
    
    public static final Properties CONFIG_PROPERTIES = UTILS.getConfiguracion();
    public static final String GENERIC_CONSULT = CONFIG_PROPERTIES.getProperty("query.base_salesforce_validate");
    
    //----- Constantes desde Properties file    
    public static final String RESULT_SUSSES = CONFIG_PROPERTIES.getProperty("ws.susses_id");
    public static final String RESULT_ERROR = CONFIG_PROPERTIES.getProperty("ws.error_id");
    public static final String RESULTSESCRIPTION_SUSSES = CONFIG_PROPERTIES.getProperty("ws.msj_susses");
    public static final String WS_VERSION = CONFIG_PROPERTIES.getProperty("ws.version");
    
    //----- Constantes de conexion a bd
    public static final String DB_TYPE_CONECTION = CONFIG_PROPERTIES.getProperty("servidor.type_conection");
    public static final String DB_HOST = CONFIG_PROPERTIES.getProperty("servidor.host");
    public static final String DB_PORT = CONFIG_PROPERTIES.getProperty("servidor.port");
    public static final String DB_DBNAME = CONFIG_PROPERTIES.getProperty("servidor.namedb");
    public static final String DB_USER = CONFIG_PROPERTIES.getProperty("servidor.user");
    public static final String DB_PASSWORD = CONFIG_PROPERTIES.getProperty("servidor.password");
    public static final String DB_JNDI = CONFIG_PROPERTIES.getProperty("servidor.jndi");
    
    public static final String QR_GET_ID_SESSION_SF = CONFIG_PROPERTIES.getProperty("query.get_id_session_sf");
    public static final String QR_CONFIG_IN_DB = CONFIG_PROPERTIES.getProperty("query.conf_in_db");
    public static final String QR_UPDATE_SI_SF = CONFIG_PROPERTIES.getProperty("query.update_sessionid_sf");
    
    public static final String KEY_USER_SF = "AUTENTICATION_SF_US";
    public static final String KEY_PASSWORD_SF = "AUTENTICATION_SF_PS";
    public static final String KEY_END_POINT_SF = "END_POINT_WS_SF";
    
    
    //----- Constantes generales 
    public static final String ID_UNIDAD_REC = "1";
    public static final String ID_UNIDAD_ENL = "2";
    public static final String ID_UNIDAD_PE = "3";
    
    public final static String LABEL_DIA = "D";
    public final static String LABEL_SEMANA = "S";
    public final static String LABEL_MES = "M";
    
    
    public static final String INACTIVE_REG = "0";
    public static final String SYSTEM_USER = "1";
    public static final String STATUS_PENDIENTE_ACT = "1";
    
    //----- Consta de utilerias 
    public static final String KEY_COLOR_FUERA_TIEMPO = "FUERATIEMPO";
    public static final String KEY_COLOR_RIESGO = "ENRIESGO";
    public static final String KEY_COLOR_ENTIEMPO = "ENTIEMPO";
    public static final String KEY_PORCENTAJE_OK = "PORCENTAJE_OK";
    public static final String KEY_PORCENTAJE_RIESGO = "PORCENTAJE_RIESGO";
    
    //---- reglas de porcentaje para ots 
    public static final String PENDIENTE = "6,7,8";
    public static final String ASIGNADA = "9";
    public static final String TRANCITO = "10";
    public static final String ST_SITIO = "107";
    public static final String TRABAJO = "11";
    public static final String TERMINADA = "14,15,16,17,72";
    public static final String CANCELADA = "18";
    public static final String PAUSA = "113,126";
    public static final String CERO_PORCIENTO = "0";
    public static final String DIEZ_PORCIENTO = "10";
    public static final String VEI_PORCIENTO = "20";
    public static final String CUA_PORCIENTO = "40";
    public static final String SE_PORCIENTO = "60";
    public static final String SET_PORCIENTO = "75";
    public static final String CIEN_PORCIENTO = "100";
    public static final String CIN_PORCIENTO = "50";
    
    
    //----- Metodo ConsultaProyectos
    public static final String KEY_SEGUNDOS_DOS_DIAS = "SEG_TWO_DAY";
    public static final String KEY_SEGUNDOS_MEDIODIA = "SEG_DAY_M";
    public static final String KEY_SEGUNDOS_UNDIA = "SEG_DAY";
    public static final String KEY_QR_BANDEJA_FACTURANDO = "QR_PROYECTOS_FC";
    public static final String KEY_QR_BANDEJA_CANCELADO = "QR_PROYECTO_CA";
    public static final String KEY_QR_PROYECTOS_MESA = "QR_PROYECTOS_MESA";
    public static final String KEY_CONDICION_PLAN_PROYECTO = "QR_PROYECTOS_EPL";
    public static final String PROYECTOS_PLANING_POR_PM = CONFIG_PROPERTIES.getProperty("query.proyectos_pm");
    public static final String QR_GET_PROYECTO_PLANING_DB = CONFIG_PROPERTIES.getProperty("query.get_py_enplaneacion");
    public static final String BANDEJA_PLANEACION = "EPL";
    public static final String BANDEJA_MESA_CONTROL = "MC";
    public static final String BANDEJA_FACTURANDO = "FC";
    public static final String BANDEJA_CANCELADOS = "CA";
    public static final String OS_STATUS_PENDIENTE = "Pendiente";
    public static final String OS_STATUS_CONFIRMADO = "Confirmado";
    public static final String OS_STATUS_PROCESO = "En proceso";
    public static final String OS_STATUS_RESCATE = "Rescate";
    public static final String OS_STATUS_CANCELADO = "Cancelado";
    public static final String OS_STATUS_DETENIDA = "Detenida";
    public static final String OS_STATUS_GESTORIA = "Gestoria";
    public static final String OS_STATUS_SUSPENDIDO = "Suspendido";
    public static final String OS_STATUS_COMPLETADO = "Completado";
    public static final String OS_STATUS_CALENDARIZADO = "Calendarizado";
    public static final String OS_STATUS_OTRO = "Otro";
    public static final String QR_SET_DETALLE_PUNTAS = CONFIG_PROPERTIES.getProperty("query.set_detail_puntas");
    
    
    public static final String C_OS_PENDIENTE = "Pendiente";
    public static final String C_OS_INICIO = "Inicio";
    public static final String C_OS_AGENDADO = "Agendado";
    public static final String C_OS_CONFIRMADO = "Confirmado";
    public static final String C_OS_PROCESO = "En proceso";
    public static final String C_OS_COMPLETADO = "Completado";
    public static final String C_OS_RESCATE = "Rescate";
    public static final String C_OS_GESTORIA = "Gestoria";
    public static final String C_OS_DETENIDA = "Detenida";
    public static final String C_OS_CANCELADO = "Cancelado";
    public static final String C_OS_CALENDARIZADO = "Calendarizado";
    public static final String C_OS_SUSPENDIDA = "Suspendido";
    
    public static final String C_POR_INSTALAR = "Por instalar";
    public static final String C_CALENDARIZADO = "Calendarizado";
    public static final String C_DETENIDO = "Detenido";
    public static final String C_DEVUELTO_VENTAS = "Devuelto a ventas";
    public static final String C_CANCELADO = "Cancelado";
    public static final String C_INSTALADO = "Instalado";
    
    //----- Metodo consulta actividades 
    public static final String QR_GET_ACIVIDADES_PMO = CONFIG_PROPERTIES.getProperty("query.get_actividades_existentes");
    public static final String QR_GET_OTS_PE = CONFIG_PROPERTIES.getProperty("query.get_ots_ffm_pe");
    public static final String QR_GET_CSP_INFO = CONFIG_PROPERTIES.getProperty("query.get_csp_info");
    public static final String QR_GET_OTS_ENL = CONFIG_PROPERTIES.getProperty("query.get_ots_ffm_enl");
    public static final String QR_GET_OTS_REC = CONFIG_PROPERTIES.getProperty("query.get_ots_ffm_rec");
    public static final String QR_SET_RESPONSABLE = CONFIG_PROPERTIES.getProperty("query.set_responsable_sys");
    public static final String QR_SET_ACTIVIDAD_PE = CONFIG_PROPERTIES.getProperty("query.set_actividad_pe");
    public static final String QR_GET_SECUENCIA_ACT = CONFIG_PROPERTIES.getProperty("query.get_seq_actividades");
    public static final String QR_GET_SECUENCIA_RESP = CONFIG_PROPERTIES.getProperty("query.get_seq_responsable");
    public static final String ACTIVIDAD_REPARACIONESPE = "16";
    public static final String DESC_ACTIVIDAD_REPARACIONESPE = "Planta externa";
    public static final String ACTIVIDAD_IMPLEMENTACION = "2";
    public static final String DESC_ACTIVIDAD_IMPLEMENTACION = "Implementacion del servicio";
    public static final String KEY_KPI_PE_DIAS = "KPI_PE_DIAS";
    public static final String KEY_KPI_PI_DIAS = "KPI_PI_DIAS";
    public static final String QR_UPDATE_DETAILL_ACT = CONFIG_PROPERTIES.getProperty("query.update_detaill_activities");
    public static final String QR_UPDATE_DATA_OTSACTIVITIES = CONFIG_PROPERTIES.getProperty("query.update_data_activities");
    public static final String QR_GET_OT_ENL = CONFIG_PROPERTIES.getProperty("query.get_ot_enl");
    public static final String QR_GET_OT_REC = CONFIG_PROPERTIES.getProperty("query.get_ot_rec");
    public static final String QR_UPDATE_OTACT = CONFIG_PROPERTIES.getProperty("query.update_ot_act");
    public static final String QR_REGISTRA_OTACT = CONFIG_PROPERTIES.getProperty("query.insert_otfor_act");
    // Carga plantilla actividades
    public static final String QR_SET_ACTIVIDADES_BASE_DB = CONFIG_PROPERTIES.getProperty("query.set_activitdades_base_db");
    public static final String QR_SET_ACTIVIDADES_CSP_DB = CONFIG_PROPERTIES.getProperty("query.set_activities_forcsp");
    public static final String QR_ST_STRUCTURA_OTS = CONFIG_PROPERTIES.getProperty("query.set_structura_ots");
    public static final String KEY_INTERVENCION = "I";
    public static final String KEY_SUBINTERVENCION = "S";
    public static final String KEY_UNIDAD_NEGOCIO = "U";
    public static final String QR_GET_CONFIG_OT = CONFIG_PROPERTIES.getProperty("query.get_config_ots");
    public static final String KEY_TOTAL = "TT";
    public static final String KEY_TIEMPO = "T";
    public static final String KEY_RIESGO = "R";
    public static final String KEY_FUERATIEMPO = "F";
    public static final String KEY_PENDIENTE = "P";
    public static final String QR_GET_PROMEDIO_PUNTA = CONFIG_PROPERTIES.getProperty("query.get_promedio_avance_punta");
    public static final String QR_UPDATE_AVANCE_PUNTA = CONFIG_PROPERTIES.getProperty("query.update_avance_punta");
    public static final String QR_UPDATE_PUNTA_CERRADA = CONFIG_PROPERTIES.getProperty("query.update_avance_punta_cerrada");
    public static final String QR_CONT_PROYECT_DET = CONFIG_PROPERTIES.getProperty("query.cont_det_proyect");
    public static final String QR_AVANCE_PUNTA = CONFIG_PROPERTIES.getProperty("query.det_avance_punta");
    public static final String QR_AVANCE_COT = CONFIG_PROPERTIES.getProperty("query.det_avance_cot");
    
    //------ Metodo de consulta detalle instalacion
    public static final String QR_DET_INSTALACION_ENL = CONFIG_PROPERTIES.getProperty("query.get_detins_enl");   
    public static final String QR_DET_INSTALACION_REC = CONFIG_PROPERTIES.getProperty("query.get_detins_rec");
    public static final String QR_DET_INSTALACION_PE = CONFIG_PROPERTIES.getProperty("query.get_detins_pe");
    
    
    //------- Finaliza proyecto
    public static final String QR_UPDATE_END_ACTIVITIES = CONFIG_PROPERTIES.getProperty("query.up_termino_actividades");
    public static final String QR_UPDATE_END_PROYECT = CONFIG_PROPERTIES.getProperty("query.up_termino_proyecto");
    public static final String QR_NUM_PUNTAS_VIVAS = CONFIG_PROPERTIES.getProperty("query.get_puntas_vivas");
    public static final String QR_GET_ID_OP = CONFIG_PROPERTIES.getProperty("query.get_id_op");
    public final static String OPPORTUNITY_TYPE = "Opportunity";
    public static final String STATUS_IMPLEMENTACION = "Implementada";
    
    
    //------- Nuevas para consultar el destalle de los proyectos
    public static final String Q_CONSULTA_DETALLE_ACTIVIDADES = CONFIG_PROPERTIES.getProperty("query.c_consulta_actividades");
    public static final String Q_REGISTRA_INFORMACION_PROYECTO = CONFIG_PROPERTIES.getProperty("query.i_registra_data_proyecto");
    public static final String Q_REGISTRA_ACTIVIDADES_BASE = CONFIG_PROPERTIES.getProperty("query.i_registra_actividades_base");
    public static final String C_ESTATUS_PROYECTO_NUEVO = "1";
    public static final String C_COMENTARIO_BASE_PR = "Proyecto nuevo";
    public static final String C_TIENE_CSP = "1";
    public static final String C_NO_TIENE_CSP = "0";
    public static final String C_VALOR_VACIO = "";
    public static final String C_SIN_AVANCE = "0.0";
    public static final String C_ENTIEMPO = "ET";
    public static final String C_ENRIESGO = "ER";
    public static final String C_FUERATIEMPO = "FT";
    public static final String C_PENDIENTES = "PE";
    public static final String Q_CONSULTA_CONF_OTS = CONFIG_PROPERTIES.getProperty("query.c_consulta_conf_ots");
    public static final String C_TIPO_OT_UNIVERSAL = "141";
    public static final String C_SUBTIPO_OT_INSSOLUCION = "160";
    
    //-------- Nuevo metodo de consulta de actividades
    public static final String Q_CONSULTA_ACTIVIDADES_IMP = CONFIG_PROPERTIES.getProperty("query.c_detalle_actividades");
    public static final String Q_REGISTRA_NEW_ACTIVIDAD = CONFIG_PROPERTIES.getProperty("query.i_reg_new_actividad");
    public static final String Q_OTS_EN_PE = CONFIG_PROPERTIES.getProperty("query.c_ots_en_pe");
    public static final String Q_REG_OTS_OF_PE = CONFIG_PROPERTIES.getProperty("query.i_reg_ots_implementacion");
    public static final String C_RESPONSABLE_PI_REC = "2";
    public static final String C_RESPONSABLE_PI_ENL = "4";
    public static final String C_RESPONSABLE_PLANTA_EXTERNA_DISTRITAL = "8";
    public static final String C_RESPONSABLE_PLANTA_EXTERNA_TX = "22";
    public static final String C_ACTIVIDAD_IMPLEMENTACION = "2";
    public static final String C_ACTIVIDAD_PLANTA_EXTERNA = "16";
    public static final String C_CONSULTA_ID_IMPLEMENTACION = CONFIG_PROPERTIES.getProperty("query.c_id_implementacion");
    public static final String C_OTS_EN_ENL = CONFIG_PROPERTIES.getProperty("query.c_ots_en_enl");
    public static final String C_OTS_EN_REC = CONFIG_PROPERTIES.getProperty("query.c_ots_en_rec");
    public static final String C_RELACION_IMPLEMENTACION = CONFIG_PROPERTIES.getProperty("query.c_relacion_implementacion");
    public static final String C_DATA_ACT_ENL = CONFIG_PROPERTIES.getProperty("query.c_get_info_enl");
    public static final String C_DATA_ACT_REC = CONFIG_PROPERTIES.getProperty("query.c_get_info_rec");
    public static final String C_DATA_ACT_PE = CONFIG_PROPERTIES.getProperty("query.c_get_info_pe");
    public static final String C_ACT_ACTIVIDADES = CONFIG_PROPERTIES.getProperty("query.u_actualiza_actividades");
    
    
    //-------- Nuevo metodo Find general salesforce 
    public static final String KEY_FIND_GENERAL_ENL = "SEARCH_GENERAL_ENL_SF";
    public static final String C_BASE_FIND_SF = CONFIG_PROPERTIES.getProperty("query.s_search_general");
    
    
    //------- Metodo para consultar el detalle de los Objetos en sf
    public static final String TYPE_CUENTA = "CU";
    public static final String TYPE_OPORTUNIDAD = "OP";
    public static final String TYPE_COTIZACION = "CO";
    public static final String TYPE_COTSITIO = "CS";
    public static final String TYPE_COTSITIOPLAN = "CP";
    public static final String TYPE_CUENTAFACTURA = "CF";
    public static final String TYPE_OS = "OS";
    public static final String TYPE_USER = "US";
    public static final String TYPE_TK = "TK";
    public static final String Q_DETALLE_CUENTA_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_cuenta");
    public static final String Q_DETALLE_OPORTUNIDAD_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_oportunidad");
    public static final String Q_DETALLE_COTIZACION_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_cotizacion");
    public static final String Q_DETALLE_COTSITIO_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_cs");
    public static final String Q_DETALLE_COTSITIOPLAN_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_csp");
    public static final String Q_DETALLE_CUENTAFACTURA_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_cuentafactura");
    public static final String Q_DETALLE_OS_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_os");
    public static final String Q_DETALLE_TK_SF = CONFIG_PROPERTIES.getProperty("query.s_detalle_tk");
    public static final String Q_DETALLE_USUARIO = CONFIG_PROPERTIES.getProperty("query.s_detalle_usuario");
    public static final String Q_ARR_OPORTUNIDAD_CU = CONFIG_PROPERTIES.getProperty("query.s_arr_oportunidad_cu");
    public static final String Q_ARR_CUENTAFACTURA_CU = CONFIG_PROPERTIES.getProperty("query.s_arr_cuentafactura_cu");
    public static final String Q_ARR_COTSITIO_COT = CONFIG_PROPERTIES.getProperty("query.s_arr_cotsitio_cot");
    public static final String Q_ARR_COTSITIOPLAN_COT = CONFIG_PROPERTIES.getProperty("query.s_arr_cotsitioplan_cot");
    public static final String Q_ARR_COTSITIOPLAN_CS = CONFIG_PROPERTIES.getProperty("query.s_arr_cotsitioplan_cs");
    public static final String Q_ARR_OS_CF = CONFIG_PROPERTIES.getProperty("query.s_arr_os_cf");
    public static final String Q_ARR_TK_CF = CONFIG_PROPERTIES.getProperty("query.s_arr_tk_cf");
    
}

