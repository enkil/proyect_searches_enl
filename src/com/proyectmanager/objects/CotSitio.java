package com.proyectmanager.objects;

public class CotSitio {
    private String id;
    private String nombre;
    private String direccionSitio;
    private String plaza;
    private String totalRentaConImpuesto;
    private String subtotalRenta;
    private String keyObject;
    private Cotizacion detalleCotizacion;
    private Sitio detalleSitio;
    private CotSitioPlan[] arrCotSitioPlan;
    private Contacto creadoPor;
    private Contacto editadoPor;
    private Contacto propietario;
        
    
    public CotSitio() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setDireccionSitio(String direccionSitio) {
        this.direccionSitio = direccionSitio;
    }

    public String getDireccionSitio() {
        return direccionSitio;
    }

    public void setTotalRentaConImpuesto(String totalRentaConImpuesto) {
        this.totalRentaConImpuesto = totalRentaConImpuesto;
    }

    public String getTotalRentaConImpuesto() {
        return totalRentaConImpuesto;
    }

    public void setSubtotalRenta(String subtotalRenta) {
        this.subtotalRenta = subtotalRenta;
    }

    public String getSubtotalRenta() {
        return subtotalRenta;
    }

    public void setDetalleCotizacion(Cotizacion detalleCotizacion) {
        this.detalleCotizacion = detalleCotizacion;
    }

    public Cotizacion getDetalleCotizacion() {
        return detalleCotizacion;
    }

    public void setDetalleSitio(Sitio detalleSitio) {
        this.detalleSitio = detalleSitio;
    }

    public Sitio getDetalleSitio() {
        return detalleSitio;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setEditadoPor(Contacto editadoPor) {
        this.editadoPor = editadoPor;
    }

    public Contacto getEditadoPor() {
        return editadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }

    public void setArrCotSitioPlan(CotSitioPlan[] arrCotSitioPlan) {
        this.arrCotSitioPlan = arrCotSitioPlan;
    }

    public CotSitioPlan[] getArrCotSitioPlan() {
        return arrCotSitioPlan;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }
}
