package com.proyectmanager.objects;

public class Punta {
    public Punta() {
        super();
    }
    
    private String Id_cuenta;
    private String Id_cotizacion;
    private String Folio_cotizacion;
    private String Id_oportunidad;
    private String Numero_oportunidad;
    private String Id_cot_sitio;
    private String Nombre_cot_sitio;
    private String Id_sitio;
    private String Folio_sitio;
    private String Nombre_sitio;
    private String Id_CSP;
    private String Folio_CSP;
    private String Id_OS;
    private String Folio_OS;
    private String Id_cuentaFactura;
    private String Numero_cuentaFactura;
    private String Canal_venta;
    private String Paquete;
    private String Fecha_aprovisionamiento;
    private String Tipo_cuadrilla;
    private String Tipo_acceso;
    private String Fecha_cierre;
    private String Plazo;
    private String Nombre_responsable_sitio;
    private String Region;
    private String Ciudad;
    private String Distrito;
    private String Cluster;
    private String Calle;
    private String Colonia;
    private String Estado;
    private String Municipio;
    private String Num_interior;
    private String Num_exterior;
    private String Longitud;
    private String Latitud;
    private String CP;
    private String Plaza;
    private String Direccion_sitio;
    private String Telefono_contacto;
    
    private String Status_punta;    
    private String Fecha_inicio;
    private String Fecha_fin;
    private String Fecha_fin_planeada;
    private String Porcentaje_avance;
    private String Punta_planeada;
    private String OSs[];
    private String CSPs[];
    private String C_FACTs[];
    private String OTs[];
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;    
    private String Num_fuera_tiempo;
    private String Num_en_riesgo;
    private String Num_en_tiempo;
    private String Num_pendientes;
    private String OS_pendientes;
    private String Num_confirmado;
    private String Num_proceso;
    private String Num_restcate;
    private String Num_Cancelado;
    private String Num_detenida;
    private String Num_gestoria;
    private String Num_suspendido;
    private String Num_completado;
    private String Num_calendarizado;
    private String Num_otro;
    private String Num_total;
    private String totalPuntas;
    private String totalRenta;
    private String totalPuntasPorInstalar;
    private String totalRentaPorInstalar;
    private String totalPuntasCalendarizado;
    private String totalRentaCalendarizado;
    private String totalPuntasDetenido;
    private String totalRentaDetenido;
    private String totalPuntasDevueltoVentas;
    private String totalRentaDevueltoVentas;
    private String totalPuntasCancelado;
    private String totalRentaCancelado;
    private String totalPuntasInstalado;
    private String totalRentaInstalado;
    private String estatusPlaneacion;
    private String enTiempo;
    private BanerSitio informacionContrato;
    private CotSitioPlan Planes[];


    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setNum_total(String Num_total) {
        this.Num_total = Num_total;
    }

    public String getNum_total() {
        return Num_total;
    }

    public void setNum_otro(String Num_otro) {
        this.Num_otro = Num_otro;
    }

    public String getNum_otro() {
        return Num_otro;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setId_oportunidad(String Id_oportunidad) {
        this.Id_oportunidad = Id_oportunidad;
    }

    public String getId_oportunidad() {
        return Id_oportunidad;
    }

    public void setNumero_oportunidad(String Numero_oportunidad) {
        this.Numero_oportunidad = Numero_oportunidad;
    }

    public String getNumero_oportunidad() {
        return Numero_oportunidad;
    }

    public void setId_cot_sitio(String Id_cot_sitio) {
        this.Id_cot_sitio = Id_cot_sitio;
    }

    public String getId_cot_sitio() {
        return Id_cot_sitio;
    }

    public void setNombre_cot_sitio(String Nombre_cot_sitio) {
        this.Nombre_cot_sitio = Nombre_cot_sitio;
    }

    public String getNombre_cot_sitio() {
        return Nombre_cot_sitio;
    }

    public void setNombre_sitio(String Nombre_sitio) {
        this.Nombre_sitio = Nombre_sitio;
    }

    public String getNombre_sitio() {
        return Nombre_sitio;
    }

    public void setNombre_responsable_sitio(String Nombre_responsable_sitio) {
        this.Nombre_responsable_sitio = Constantes.UTILS.toInitCap(Nombre_responsable_sitio);
    }

    public String getNombre_responsable_sitio() {
        return Nombre_responsable_sitio;
    }

    public void setDireccion_sitio(String Direccion_sitio) {
        this.Direccion_sitio = Constantes.UTILS.toInitCap(Direccion_sitio);
    }

    public String getDireccion_sitio() {
        return Direccion_sitio;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setStatus_punta(String Status_punta) {
        this.Status_punta = Constantes.UTILS.toInitCap(Status_punta);
    }

    public String getStatus_punta() {
        return Status_punta;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setFecha_fin_planeada(String Fecha_fin_planeada) {
        this.Fecha_fin_planeada = Fecha_fin_planeada;
    }

    public String getFecha_fin_planeada() {
        return Fecha_fin_planeada;
    }

    public void setSemaforo(String Semaforo) {
        this.semaforo = Semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setPorcentaje_avance(String Porcentaje_avance) {
        this.Porcentaje_avance = Porcentaje_avance;
    }

    public String getPorcentaje_avance() {
        return Porcentaje_avance;
    }

    public void setId_CSP(String Id_CSP) {
        this.Id_CSP = Id_CSP;
    }

    public String getId_CSP() {
        return Id_CSP;
    }

    public void setFolio_CSP(String Folio_CSP) {
        this.Folio_CSP = Folio_CSP;
    }

    public String getFolio_CSP() {
        return Folio_CSP;
    }

    public void setId_OS(String Id_OS) {
        this.Id_OS = Id_OS;
    }

    public String getId_OS() {
        return Id_OS;
    }

    public void setFolio_OS(String Folio_OS) {
        this.Folio_OS = Folio_OS;
    }

    public String getFolio_OS() {
        return Folio_OS;
    }

    public void setPaquete(String Paquete) {
        this.Paquete = Constantes.UTILS.toInitCap(Paquete);
    }

    public String getPaquete() {
        return Paquete;
    }

    public void setFecha_aprovisionamiento(String Fecha_aprovisionamiento) {
        this.Fecha_aprovisionamiento = Fecha_aprovisionamiento;
    }

    public String getFecha_aprovisionamiento() {
        return Fecha_aprovisionamiento;
    }

    public void setTipo_cuadrilla(String Tipo_cuadrilla) {
        this.Tipo_cuadrilla = Tipo_cuadrilla;
    }

    public String getTipo_cuadrilla() {
        return Tipo_cuadrilla;
    }

    public void setTipo_acceso(String Tipo_acceso) {
        this.Tipo_acceso = Constantes.UTILS.toInitCap(Tipo_acceso);
    }

    public String getTipo_acceso() {
        return Tipo_acceso;
    }

    public void setFecha_cierre(String Fecha_cierre) {
        this.Fecha_cierre = Fecha_cierre;
    }

    public String getFecha_cierre() {
        return Fecha_cierre;
    }

    public void setPlazo(String Plazo) {
        this.Plazo = Constantes.UTILS.toInitCap(Plazo);
    }

    public String getPlazo() {
        return Plazo;
    }

    public void setPlaza(String Plaza) {
        this.Plaza = Constantes.UTILS.toInitCap(Plaza);
    }

    public String getPlaza() {
        return Plaza;
    }

    public void setId_sitio(String Id_sitio) {
        this.Id_sitio = Id_sitio;
    }

    public String getId_sitio() {
        return Id_sitio;
    }

    public void setFolio_sitio(String Folio_sitio) {
        this.Folio_sitio = Folio_sitio;
    }

    public String getFolio_sitio() {
        return Folio_sitio;
    }

    public void setId_cuentaFactura(String Id_cuentaFactura) {
        this.Id_cuentaFactura = Id_cuentaFactura;
    }

    public String getId_cuentaFactura() {
        return Id_cuentaFactura;
    }

    public void setNumero_cuentaFactura(String Numero_cuentaFactura) {
        this.Numero_cuentaFactura = Numero_cuentaFactura;
    }

    public String getNumero_cuentaFactura() {
        return Numero_cuentaFactura;
    }

    public void setCanal_venta(String Canal_venta) {
        this.Canal_venta = Constantes.UTILS.toInitCap(Canal_venta);
    }

    public String getCanal_venta() {
        return Canal_venta;
    }

    public void setId_cuenta(String Id_cuenta) {
        this.Id_cuenta = Id_cuenta;
    }

    public String getId_cuenta() {
        return Id_cuenta;
    }

    public void setPunta_planeada(String Punta_planeada) {
        this.Punta_planeada = Punta_planeada;
    }

    public String getPunta_planeada() {
        return Punta_planeada;
    }

    public void setOSs(String[] OSs) {
        this.OSs = OSs;
    }

    public String[] getOSs() {
        return OSs;
    }

    public void setCSPs(String[] CSPs) {
        this.CSPs = CSPs;
    }

    public String[] getCSPs() {
        return CSPs;
    }

    public void setC_FACTs(String[] C_FACTs) {
        this.C_FACTs = C_FACTs;
    }

    public String[] getC_FACTs() {
        return C_FACTs;
    }

    public void setOTs(String[] OTs) {
        this.OTs = OTs;
    }

    public String[] getOTs() {
        return OTs;
    }

    public void setPlanes(CotSitioPlan[] Planes) {
        this.Planes = Planes;
    }

    public CotSitioPlan[] getPlanes() {
        return Planes;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return porcentajeEsperado;
    }

    public void setOS_pendientes(String OS_pendientes) {
        this.OS_pendientes = OS_pendientes;
    }

    public String getOS_pendientes() {
        return OS_pendientes;
    }

    public void setNum_confirmado(String Num_confirmado) {
        this.Num_confirmado = Num_confirmado;
    }

    public String getNum_confirmado() {
        return Num_confirmado;
    }

    public void setNum_proceso(String Num_proceso) {
        this.Num_proceso = Num_proceso;
    }

    public String getNum_proceso() {
        return Num_proceso;
    }

    public void setNum_restcate(String Num_restcate) {
        this.Num_restcate = Num_restcate;
    }

    public String getNum_restcate() {
        return Num_restcate;
    }

    public void setNum_Cancelado(String Num_Cancelado) {
        this.Num_Cancelado = Num_Cancelado;
    }

    public String getNum_Cancelado() {
        return Num_Cancelado;
    }

    public void setNum_detenida(String Num_detenida) {
        this.Num_detenida = Num_detenida;
    }

    public String getNum_detenida() {
        return Num_detenida;
    }

    public void setNum_gestoria(String Num_gestoria) {
        this.Num_gestoria = Num_gestoria;
    }

    public String getNum_gestoria() {
        return Num_gestoria;
    }

    public void setNum_suspendido(String Num_suspendido) {
        this.Num_suspendido = Num_suspendido;
    }

    public String getNum_suspendido() {
        return Num_suspendido;
    }

    public void setNum_completado(String Num_completado) {
        this.Num_completado = Num_completado;
    }

    public String getNum_completado() {
        return Num_completado;
    }

    public void setNum_calendarizado(String Num_calendarizado) {
        this.Num_calendarizado = Num_calendarizado;
    }

    public String getNum_calendarizado() {
        return Num_calendarizado;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getRegion() {
        return Region;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setDistrito(String Distrito) {
        this.Distrito = Distrito;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setCluster(String Cluster) {
        this.Cluster = Cluster;
    }

    public String getCluster() {
        return Cluster;
    }

    public void setCalle(String Calle) {
        this.Calle = Calle;
    }

    public String getCalle() {
        return Calle;
    }

    public void setColonia(String Colonia) {
        this.Colonia = Colonia;
    }

    public String getColonia() {
        return Colonia;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getEstado() {
        return Estado;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setNum_interior(String Num_interior) {
        this.Num_interior = Num_interior;
    }

    public String getNum_interior() {
        return Num_interior;
    }

    public void setNum_exterior(String Num_exterior) {
        this.Num_exterior = Num_exterior;
    }

    public String getNum_exterior() {
        return Num_exterior;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getCP() {
        return CP;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setEnTiempo(String enTiempo) {
        this.enTiempo = enTiempo;
    }

    public String getEnTiempo() {
        return enTiempo;
    }

    public void setInformacionContrato(BanerSitio informacionContrato) {
        this.informacionContrato = informacionContrato;
    }

    public BanerSitio getInformacionContrato() {
        return informacionContrato;
    }

    public void setTotalPuntas(String totalPuntas) {
        this.totalPuntas = totalPuntas;
    }

    public String getTotalPuntas() {
        return totalPuntas;
    }

    public void setTotalRenta(String totalRenta) {
        this.totalRenta = totalRenta;
    }

    public String getTotalRenta() {
        return totalRenta;
    }

    public void setTotalPuntasPorInstalar(String totalPuntasPorInstalar) {
        this.totalPuntasPorInstalar = totalPuntasPorInstalar;
    }

    public String getTotalPuntasPorInstalar() {
        return totalPuntasPorInstalar;
    }

    public void setTotalRentaPorInstalar(String totalRentaPorInstalar) {
        this.totalRentaPorInstalar = totalRentaPorInstalar;
    }

    public String getTotalRentaPorInstalar() {
        return totalRentaPorInstalar;
    }

    public void setTotalPuntasCalendarizado(String totalPuntasCalendarizado) {
        this.totalPuntasCalendarizado = totalPuntasCalendarizado;
    }

    public String getTotalPuntasCalendarizado() {
        return totalPuntasCalendarizado;
    }

    public void setTotalRentaCalendarizado(String totalRentaCalendarizado) {
        this.totalRentaCalendarizado = totalRentaCalendarizado;
    }

    public String getTotalRentaCalendarizado() {
        return totalRentaCalendarizado;
    }

    public void setTotalPuntasDetenido(String totalPuntasDetenido) {
        this.totalPuntasDetenido = totalPuntasDetenido;
    }

    public String getTotalPuntasDetenido() {
        return totalPuntasDetenido;
    }

    public void setTotalRentaDetenido(String totalRentaDetenido) {
        this.totalRentaDetenido = totalRentaDetenido;
    }

    public String getTotalRentaDetenido() {
        return totalRentaDetenido;
    }

    public void setTotalPuntasDevueltoVentas(String totalPuntasDevueltoVentas) {
        this.totalPuntasDevueltoVentas = totalPuntasDevueltoVentas;
    }

    public String getTotalPuntasDevueltoVentas() {
        return totalPuntasDevueltoVentas;
    }

    public void setTotalRentaDevueltoVentas(String totalRentaDevueltoVentas) {
        this.totalRentaDevueltoVentas = totalRentaDevueltoVentas;
    }

    public String getTotalRentaDevueltoVentas() {
        return totalRentaDevueltoVentas;
    }

    public void setTotalPuntasCancelado(String totalPuntasCancelado) {
        this.totalPuntasCancelado = totalPuntasCancelado;
    }

    public String getTotalPuntasCancelado() {
        return totalPuntasCancelado;
    }

    public void setTotalRentaCancelado(String totalRentaCancelado) {
        this.totalRentaCancelado = totalRentaCancelado;
    }

    public String getTotalRentaCancelado() {
        return totalRentaCancelado;
    }

    public void setTotalPuntasInstalado(String totalPuntasInstalado) {
        this.totalPuntasInstalado = totalPuntasInstalado;
    }

    public String getTotalPuntasInstalado() {
        return totalPuntasInstalado;
    }

    public void setTotalRentaInstalado(String totalRentaInstalado) {
        this.totalRentaInstalado = totalRentaInstalado;
    }

    public String getTotalRentaInstalado() {
        return totalRentaInstalado;
    }

}
