package com.proyectmanager.objects;

public class Ticket {
    
    private String id;
    private String caseNumber;
    private String nivel1;
    private String nivel2;
    private String nivel3;
    private String status;
    private String motivo;
    private String solucionOT;
    private String primerFechaAgendamiento;
    private String fechaAgendamiento;
    private String origin;
    private String asunto;
    private String description;
    private String folioSd;
    private String comentariosSd;
    private String createdDate;
    private String keyObject;
    private CuentaFactura detalleCuentaFactura;
    private Os detalleOs;
    private Contacto creadoPor;
    private Contacto editadoPor;
    private Contacto propietario;

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }

    public Ticket() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setNivel1(String nivel1) {
        this.nivel1 = nivel1;
    }

    public String getNivel1() {
        return nivel1;
    }

    public void setNivel2(String nivel2) {
        this.nivel2 = nivel2;
    }

    public String getNivel2() {
        return nivel2;
    }

    public void setNivel3(String nivel3) {
        this.nivel3 = nivel3;
    }

    public String getNivel3() {
        return nivel3;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setSolucionOT(String solucionOT) {
        this.solucionOT = solucionOT;
    }

    public String getSolucionOT() {
        return solucionOT;
    }

    public void setPrimerFechaAgendamiento(String primerFechaAgendamiento) {
        this.primerFechaAgendamiento = primerFechaAgendamiento;
    }

    public String getPrimerFechaAgendamiento() {
        return primerFechaAgendamiento;
    }

    public void setFechaAgendamiento(String fechaAgendamiento) {
        this.fechaAgendamiento = fechaAgendamiento;
    }

    public String getFechaAgendamiento() {
        return fechaAgendamiento;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setFolioSd(String folioSd) {
        this.folioSd = folioSd;
    }

    public String getFolioSd() {
        return folioSd;
    }

    public void setComentariosSd(String comentariosSd) {
        this.comentariosSd = comentariosSd;
    }

    public String getComentariosSd() {
        return comentariosSd;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setDetalleCuentaFactura(CuentaFactura detalleCuentaFactura) {
        this.detalleCuentaFactura = detalleCuentaFactura;
    }

    public CuentaFactura getDetalleCuentaFactura() {
        return detalleCuentaFactura;
    }

    public void setDetalleOs(Os detalleOs) {
        this.detalleOs = detalleOs;
    }

    public Os getDetalleOs() {
        return detalleOs;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setEditadoPor(Contacto editadoPor) {
        this.editadoPor = editadoPor;
    }

    public Contacto getEditadoPor() {
        return editadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }
}
