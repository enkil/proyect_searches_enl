package com.proyectmanager.objects;

import com.sun.xml.internal.bind.v2.model.core.ID;

public class CuentaFactura {
    
    private String id;
    private String nombre;
    private String numCuentaFactura;
    private String nombreResponsableComercial;
    private String celular;
    private String telefonoPrincipal;
    private String email;
    private String emailFacturacion1;
    private String emailFacturacion2;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String rfc;
    private String portabilidad;
    private String fechaVencimiento;
    private String tipoFacturacion;
    private String plazo;
    private String fechaActivacion;
    private String folioVenta;
    private String formaPago;
    private String tipoPago;
    private String planServicio;
    private String planPostVenta;
    private String rentaMensualPL;
    private String rentaMensualPP;
    private String activada;
    private String keyObject;
    private LocalizacionInstalacion detalleGeograficoInstalacion;
    private Cuenta detalleCuenta;
    private Os[] arrOs;
    private Ticket[] arrTicket;
    private Contacto creadoPor;
    private Contacto editadoPor;
    private Contacto propietario;
    
    
    public CuentaFactura() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNumCuentaFactura(String numCuentaFactura) {
        this.numCuentaFactura = numCuentaFactura;
    }

    public String getNumCuentaFactura() {
        return numCuentaFactura;
    }

    public void setNombreResponsableComercial(String nombreResponsableComercial) {
        this.nombreResponsableComercial = nombreResponsableComercial;
    }

    public String getNombreResponsableComercial() {
        return nombreResponsableComercial;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCelular() {
        return celular;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    public String getTelefonoPrincipal() {
        return telefonoPrincipal;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmailFacturacion1(String emailFacturacion1) {
        this.emailFacturacion1 = emailFacturacion1;
    }

    public String getEmailFacturacion1() {
        return emailFacturacion1;
    }

    public void setEmailFacturacion2(String emailFacturacion2) {
        this.emailFacturacion2 = emailFacturacion2;
    }

    public String getEmailFacturacion2() {
        return emailFacturacion2;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRfc() {
        return rfc;
    }

    public void setPortabilidad(String portabilidad) {
        this.portabilidad = portabilidad;
    }

    public String getPortabilidad() {
        return portabilidad;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setTipoFacturacion(String tipoFacturacion) {
        this.tipoFacturacion = tipoFacturacion;
    }

    public String getTipoFacturacion() {
        return tipoFacturacion;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setFechaActivacion(String fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    public String getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFolioVenta(String folioVenta) {
        this.folioVenta = folioVenta;
    }

    public String getFolioVenta() {
        return folioVenta;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setPlanServicio(String planServicio) {
        this.planServicio = planServicio;
    }

    public String getPlanServicio() {
        return planServicio;
    }

    public void setPlanPostVenta(String planPostVenta) {
        this.planPostVenta = planPostVenta;
    }

    public String getPlanPostVenta() {
        return planPostVenta;
    }

    public void setRentaMensualPL(String rentaMensualPL) {
        this.rentaMensualPL = rentaMensualPL;
    }

    public String getRentaMensualPL() {
        return rentaMensualPL;
    }

    public void setRentaMensualPP(String rentaMensualPP) {
        this.rentaMensualPP = rentaMensualPP;
    }

    public String getRentaMensualPP() {
        return rentaMensualPP;
    }

    public void setActivada(String activada) {
        this.activada = activada;
    }

    public String getActivada() {
        return activada;
    }

    public void setDetalleGeograficoInstalacion(LocalizacionInstalacion detalleGeograficoInstalacion) {
        this.detalleGeograficoInstalacion = detalleGeograficoInstalacion;
    }

    public LocalizacionInstalacion getDetalleGeograficoInstalacion() {
        return detalleGeograficoInstalacion;
    }

    public void setDetalleCuenta(Cuenta detalleCuenta) {
        this.detalleCuenta = detalleCuenta;
    }

    public Cuenta getDetalleCuenta() {
        return detalleCuenta;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setEditadoPor(Contacto editadoPor) {
        this.editadoPor = editadoPor;
    }

    public Contacto getEditadoPor() {
        return editadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }

    public void setArrTicket(Ticket[] arrTicket) {
        this.arrTicket = arrTicket;
    }

    public Ticket[] getArrTicket() {
        return arrTicket;
    }

    public void setArrOs(Os[] arrOs) {
        this.arrOs = arrOs;
    }

    public Os[] getArrOs() {
        return arrOs;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }
}
