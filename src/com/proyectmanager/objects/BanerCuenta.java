package com.proyectmanager.objects;

public class BanerCuenta {
    
    private String razonSocial;
    private String rfc;
    private String top5000;
    private String ciudad;
    private String estado;
    private String delegacionMunicipio;
    private String colonia;
    private String calle;
    private String numeroExterior;
    private String numeroInterior;
    private String codigoPostal;
    private String phone;
    private String montoPotencial;
    private String contactoPrincipalNombreCompleto;
    private String contactoPrincipalPhone;
    private String contactoPrincipalMobilePhone;
    private String contactoPrincipalEmail;
    
    public BanerCuenta() {
        super();
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRfc() {
        return rfc;
    }

    public void setTop5000(String top5000) {
        this.top5000 = top5000;
    }

    public String getTop5000() {
        return top5000;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setDelegacionMunicipio(String delegacionMunicipio) {
        this.delegacionMunicipio = delegacionMunicipio;
    }

    public String getDelegacionMunicipio() {
        return delegacionMunicipio;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getColonia() {
        return colonia;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCalle() {
        return calle;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    public String getNumeroInterior() {
        return numeroInterior;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setMontoPotencial(String montoPotencial) {
        this.montoPotencial = montoPotencial;
    }

    public String getMontoPotencial() {
        return montoPotencial;
    }

    public void setContactoPrincipalNombreCompleto(String contactoPrincipalNombreCompleto) {
        this.contactoPrincipalNombreCompleto = contactoPrincipalNombreCompleto;
    }

    public String getContactoPrincipalNombreCompleto() {
        return contactoPrincipalNombreCompleto;
    }

    public void setContactoPrincipalPhone(String contactoPrincipalPhone) {
        this.contactoPrincipalPhone = contactoPrincipalPhone;
    }

    public String getContactoPrincipalPhone() {
        return contactoPrincipalPhone;
    }

    public void setContactoPrincipalMobilePhone(String contactoPrincipalMobilePhone) {
        this.contactoPrincipalMobilePhone = contactoPrincipalMobilePhone;
    }

    public String getContactoPrincipalMobilePhone() {
        return contactoPrincipalMobilePhone;
    }

    public void setContactoPrincipalEmail(String contactoPrincipalEmail) {
        this.contactoPrincipalEmail = contactoPrincipalEmail;
    }

    public String getContactoPrincipalEmail() {
        return contactoPrincipalEmail;
    }
}
