package com.proyectmanager.objects;

public class Os {
    
    private String id;
    private String nombre;
    private String numeroCuentaFactura;
    private String estatus;
    private String motivoCancelacion;
    private String tSCompletado;
    private String tSConfirmado;
    private String propietarioOportunidad;
    private String idOt;
    private String canalVenta;
    private String fechaAgendada;
    private String turnoAg;
    private String osConfirmada;
    private String tipoOrden;
    private String comentariosOs;
    private String keyObject;
    private Oportunidad detalleOportunidad;
    private Cotizacion detalleCotizacion;
    private Sitio detalleSitio;
    private CotSitioPlan detalleCotSitioPlan;
    private CuentaFactura detalleCuentaFactura;
    private Contacto creadoPor;
    private Contacto editadoPor;
    private Contacto propietario;
    
    public Os() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNumeroCuentaFactura(String numeroCuentaFactura) {
        this.numeroCuentaFactura = numeroCuentaFactura;
    }

    public String getNumeroCuentaFactura() {
        return numeroCuentaFactura;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setMotivoCancelacion(String motivoCancelacion) {
        this.motivoCancelacion = motivoCancelacion;
    }

    public String getMotivoCancelacion() {
        return motivoCancelacion;
    }

    public void setTSCompletado(String tSCompletado) {
        this.tSCompletado = tSCompletado;
    }

    public String getTSCompletado() {
        return tSCompletado;
    }

    public void setTSConfirmado(String tSConfirmado) {
        this.tSConfirmado = tSConfirmado;
    }

    public String getTSConfirmado() {
        return tSConfirmado;
    }

    public void setPropietarioOportunidad(String propietarioOportunidad) {
        this.propietarioOportunidad = propietarioOportunidad;
    }

    public String getPropietarioOportunidad() {
        return propietarioOportunidad;
    }

    public void setIdOt(String idOt) {
        this.idOt = idOt;
    }

    public String getIdOt() {
        return idOt;
    }

    public void setCanalVenta(String canalVenta) {
        this.canalVenta = canalVenta;
    }

    public String getCanalVenta() {
        return canalVenta;
    }

    public void setFechaAgendada(String fechaAgendada) {
        this.fechaAgendada = fechaAgendada;
    }

    public String getFechaAgendada() {
        return fechaAgendada;
    }

    public void setTurnoAg(String turnoAg) {
        this.turnoAg = turnoAg;
    }

    public String getTurnoAg() {
        return turnoAg;
    }

    public void setOsConfirmada(String osConfirmada) {
        this.osConfirmada = osConfirmada;
    }

    public String getOsConfirmada() {
        return osConfirmada;
    }

    public void setTipoOrden(String tipoOrden) {
        this.tipoOrden = tipoOrden;
    }

    public String getTipoOrden() {
        return tipoOrden;
    }

    public void setComentariosOs(String comentariosOs) {
        this.comentariosOs = comentariosOs;
    }

    public String getComentariosOs() {
        return comentariosOs;
    }

    public void setDetalleOportunidad(Oportunidad detalleOportunidad) {
        this.detalleOportunidad = detalleOportunidad;
    }

    public Oportunidad getDetalleOportunidad() {
        return detalleOportunidad;
    }

    public void setDetalleCotizacion(Cotizacion detalleCotizacion) {
        this.detalleCotizacion = detalleCotizacion;
    }

    public Cotizacion getDetalleCotizacion() {
        return detalleCotizacion;
    }

    public void setDetalleCotSitioPlan(CotSitioPlan detalleCotSitioPlan) {
        this.detalleCotSitioPlan = detalleCotSitioPlan;
    }

    public CotSitioPlan getDetalleCotSitioPlan() {
        return detalleCotSitioPlan;
    }

    public void setDetalleCuentaFactura(CuentaFactura detalleCuentaFactura) {
        this.detalleCuentaFactura = detalleCuentaFactura;
    }

    public CuentaFactura getDetalleCuentaFactura() {
        return detalleCuentaFactura;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setEditadoPor(Contacto editadoPor) {
        this.editadoPor = editadoPor;
    }

    public Contacto getEditadoPor() {
        return editadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }

    public void setDetalleSitio(Sitio detalleSitio) {
        this.detalleSitio = detalleSitio;
    }

    public Sitio getDetalleSitio() {
        return detalleSitio;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }
}
