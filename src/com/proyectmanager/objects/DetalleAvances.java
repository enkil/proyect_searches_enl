package com.proyectmanager.objects;

public class DetalleAvances {
    public DetalleAvances() {
        super();
    }
    
    private String puntaFechaFinPlaneada;
    private String puntaFechaInicioReal;
    private String puntaFechaFinReal;
    private String puntaPorcentaje;
    private String puntaPorcentajeEsperado;
    private String puntaSemaforo;
    private String cotFechaFinPlaneada;
    private String cotFechaInicioReal;
    private String cotFechaFinReal;
    private String cotPorcentaje;
    private String cotPorcentajeEsperado;
    private String cotSemaforo;

    public void setPuntaFechaFinPlaneada(String puntaFechaFinPlaneada) {
        this.puntaFechaFinPlaneada = puntaFechaFinPlaneada;
    }

    public String getPuntaFechaFinPlaneada() {
        return puntaFechaFinPlaneada;
    }

    public void setPuntaFechaInicioReal(String puntaFechaInicioReal) {
        this.puntaFechaInicioReal = puntaFechaInicioReal;
    }

    public String getPuntaFechaInicioReal() {
        return puntaFechaInicioReal;
    }

    public void setPuntaFechaFinReal(String puntaFechaFinReal) {
        this.puntaFechaFinReal = puntaFechaFinReal;
    }

    public String getPuntaFechaFinReal() {
        return puntaFechaFinReal;
    }

    public void setPuntaPorcentaje(String puntaPorcentaje) {
        this.puntaPorcentaje = puntaPorcentaje;
    }

    public String getPuntaPorcentaje() {
        return puntaPorcentaje;
    }

    public void setPuntaPorcentajeEsperado(String puntaPorcentajeEsperado) {
        this.puntaPorcentajeEsperado = puntaPorcentajeEsperado;
    }

    public String getPuntaPorcentajeEsperado() {
        return puntaPorcentajeEsperado;
    }

    public void setPuntaSemaforo(String puntaSemaforo) {
        this.puntaSemaforo = puntaSemaforo;
    }

    public String getPuntaSemaforo() {
        return puntaSemaforo;
    }

    public void setCotFechaFinPlaneada(String cotFechaFinPlaneada) {
        this.cotFechaFinPlaneada = cotFechaFinPlaneada;
    }

    public String getCotFechaFinPlaneada() {
        return cotFechaFinPlaneada;
    }

    public void setCotFechaInicioReal(String cotFechaInicioReal) {
        this.cotFechaInicioReal = cotFechaInicioReal;
    }

    public String getCotFechaInicioReal() {
        return cotFechaInicioReal;
    }

    public void setCotFechaFinReal(String cotFechaFinReal) {
        this.cotFechaFinReal = cotFechaFinReal;
    }

    public String getCotFechaFinReal() {
        return cotFechaFinReal;
    }

    public void setCotPorcentaje(String cotPorcentaje) {
        this.cotPorcentaje = cotPorcentaje;
    }

    public String getCotPorcentaje() {
        return cotPorcentaje;
    }

    public void setCotPorcentajeEsperado(String cotPorcentajeEsperado) {
        this.cotPorcentajeEsperado = cotPorcentajeEsperado;
    }

    public String getCotPorcentajeEsperado() {
        return cotPorcentajeEsperado;
    }

    public void setCotSemaforo(String cotSemaforo) {
        this.cotSemaforo = cotSemaforo;
    }

    public String getCotSemaforo() {
        return cotSemaforo;
    }
}
