package com.proyectmanager.objects;

public class Login {
    
    private String User;
    private String Password;
    private String Ip;
    
    public Login() {
        super();
    }

    public void setUser(String User) {
        this.User = User;
    }

    public String getUser() {
        return User;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getPassword() {
        return Password;
    }

    public void setIp(String Ip) {
        this.Ip = Ip;
    }

    public String getIp() {
        return Ip;
    }
}
