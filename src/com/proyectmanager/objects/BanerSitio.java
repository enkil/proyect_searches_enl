package com.proyectmanager.objects;

public class BanerSitio {
    
    private String folioCotSitio;
    private String direccionSitio;
    private String plaza;
    private String subtotalRenta;
    private String totalRentaConImpuesto;
    private String tipoOportunidad;
    private String folioCotizacion;
    private String nombreSitio;
    private String folioSitio;
    private String tipoCobertura;
    private String latitude;
    private String longitude;
    
    public BanerSitio() {
        super();
    }

    public void setFolioCotSitio(String folioCotSitio) {
        this.folioCotSitio = folioCotSitio;
    }

    public String getFolioCotSitio() {
        return folioCotSitio;
    }

    public void setDireccionSitio(String direccionSitio) {
        this.direccionSitio = direccionSitio;
    }

    public String getDireccionSitio() {
        return direccionSitio;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setSubtotalRenta(String subtotalRenta) {
        this.subtotalRenta = subtotalRenta;
    }

    public String getSubtotalRenta() {
        return subtotalRenta;
    }

    public void setTotalRentaConImpuesto(String totalRentaConImpuesto) {
        this.totalRentaConImpuesto = totalRentaConImpuesto;
    }

    public String getTotalRentaConImpuesto() {
        return totalRentaConImpuesto;
    }

    public void setTipoOportunidad(String tipoOportunidad) {
        this.tipoOportunidad = tipoOportunidad;
    }

    public String getTipoOportunidad() {
        return tipoOportunidad;
    }

    public void setFolioCotizacion(String folioCotizacion) {
        this.folioCotizacion = folioCotizacion;
    }

    public String getFolioCotizacion() {
        return folioCotizacion;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setFolioSitio(String folioSitio) {
        this.folioSitio = folioSitio;
    }

    public String getFolioSitio() {
        return folioSitio;
    }

    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public String getTipoCobertura() {
        return tipoCobertura;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
