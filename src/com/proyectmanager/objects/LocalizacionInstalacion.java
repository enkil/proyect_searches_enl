package com.proyectmanager.objects;

public class LocalizacionInstalacion {
    
    private String regionInstalacionId;
    private String regionInstalacion;
    private String plazaInstalacion;
    private String distritoInstalacion;
    private String zonaInstalacion;
    private String clusterInstalacion;
    private String ciudadInstalacion;
    private String estadoInstalacion;
    private String delegacionInstalacion;
    private String coloniaInstalacion;
    private String calleInstalacion;
    private String numeroExteriorInstalacion;
    private String numeroInteriorInstalacion;
    private String cpInstalacion;
    private String latitudeInstalacion;
    private String longitudeInstalacion;
    
    public LocalizacionInstalacion() {
        super();
    }

    public void setRegionInstalacionId(String regionInstalacionId) {
        this.regionInstalacionId = regionInstalacionId;
    }

    public String getRegionInstalacionId() {
        return regionInstalacionId;
    }

    public void setRegionInstalacion(String regionInstalacion) {
        this.regionInstalacion = regionInstalacion;
    }

    public String getRegionInstalacion() {
        return regionInstalacion;
    }

    public void setPlazaInstalacion(String plazaInstalacion) {
        this.plazaInstalacion = plazaInstalacion;
    }

    public String getPlazaInstalacion() {
        return plazaInstalacion;
    }

    public void setDistritoInstalacion(String distritoInstalacion) {
        this.distritoInstalacion = distritoInstalacion;
    }

    public String getDistritoInstalacion() {
        return distritoInstalacion;
    }

    public void setZonaInstalacion(String zonaInstalacion) {
        this.zonaInstalacion = zonaInstalacion;
    }

    public String getZonaInstalacion() {
        return zonaInstalacion;
    }

    public void setClusterInstalacion(String clusterInstalacion) {
        this.clusterInstalacion = clusterInstalacion;
    }

    public String getClusterInstalacion() {
        return clusterInstalacion;
    }

    public void setCiudadInstalacion(String ciudadInstalacion) {
        this.ciudadInstalacion = ciudadInstalacion;
    }

    public String getCiudadInstalacion() {
        return ciudadInstalacion;
    }

    public void setEstadoInstalacion(String estadoInstalacion) {
        this.estadoInstalacion = estadoInstalacion;
    }

    public String getEstadoInstalacion() {
        return estadoInstalacion;
    }

    public void setDelegacionInstalacion(String delegacionInstalacion) {
        this.delegacionInstalacion = delegacionInstalacion;
    }

    public String getDelegacionInstalacion() {
        return delegacionInstalacion;
    }

    public void setColoniaInstalacion(String coloniaInstalacion) {
        this.coloniaInstalacion = coloniaInstalacion;
    }

    public String getColoniaInstalacion() {
        return coloniaInstalacion;
    }

    public void setCalleInstalacion(String calleInstalacion) {
        this.calleInstalacion = calleInstalacion;
    }

    public String getCalleInstalacion() {
        return calleInstalacion;
    }

    public void setNumeroExteriorInstalacion(String numeroExteriorInstalacion) {
        this.numeroExteriorInstalacion = numeroExteriorInstalacion;
    }

    public String getNumeroExteriorInstalacion() {
        return numeroExteriorInstalacion;
    }

    public void setNumeroInteriorInstalacion(String numeroInteriorInstalacion) {
        this.numeroInteriorInstalacion = numeroInteriorInstalacion;
    }

    public String getNumeroInteriorInstalacion() {
        return numeroInteriorInstalacion;
    }

    public void setCpInstalacion(String cpInstalacion) {
        this.cpInstalacion = cpInstalacion;
    }

    public String getCpInstalacion() {
        return cpInstalacion;
    }

    public void setLatitudeInstalacion(String latitudeInstalacion) {
        this.latitudeInstalacion = latitudeInstalacion;
    }

    public String getLatitudeInstalacion() {
        return latitudeInstalacion;
    }

    public void setLongitudeInstalacion(String longitudeInstalacion) {
        this.longitudeInstalacion = longitudeInstalacion;
    }

    public String getLongitudeInstalacion() {
        return longitudeInstalacion;
    }
}
