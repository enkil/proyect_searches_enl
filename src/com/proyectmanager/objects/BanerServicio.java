package com.proyectmanager.objects;

public class BanerServicio {
    
    private String folioCsp;
    private String familiaPlan;
    private String nombrePlan;
    private String tipoOportunidad;
    private String tipoRegistro;
    private String acessoPrincipal;
    private String fechaCerradaGanada;
    private String TipoCotizacionPm;
    private String cuentaFactura;
    private String cuadrillaFFM;
    private String folioOs;
    private String estatusActivacion;
    private String fechaAprovisionamiento;
    private String ot;
    
    public BanerServicio() {
        super();
    }

    public void setFolioCsp(String folioCsp) {
        this.folioCsp = folioCsp;
    }

    public String getFolioCsp() {
        return folioCsp;
    }

    public void setFamiliaPlan(String familiaPlan) {
        this.familiaPlan = familiaPlan;
    }

    public String getFamiliaPlan() {
        return familiaPlan;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setTipoOportunidad(String tipoOportunidad) {
        this.tipoOportunidad = tipoOportunidad;
    }

    public String getTipoOportunidad() {
        return tipoOportunidad;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setAcessoPrincipal(String acessoPrincipal) {
        this.acessoPrincipal = acessoPrincipal;
    }

    public String getAcessoPrincipal() {
        return acessoPrincipal;
    }

    public void setFechaCerradaGanada(String fechaCerradaGanada) {
        this.fechaCerradaGanada = fechaCerradaGanada;
    }

    public String getFechaCerradaGanada() {
        return fechaCerradaGanada;
    }

    public void setTipoCotizacionPm(String TipoCotizacionPm) {
        this.TipoCotizacionPm = TipoCotizacionPm;
    }

    public String getTipoCotizacionPm() {
        return TipoCotizacionPm;
    }

    public void setCuentaFactura(String cuentaFactura) {
        this.cuentaFactura = cuentaFactura;
    }

    public String getCuentaFactura() {
        return cuentaFactura;
    }

    public void setCuadrillaFFM(String cuadrillaFFM) {
        this.cuadrillaFFM = cuadrillaFFM;
    }

    public String getCuadrillaFFM() {
        return cuadrillaFFM;
    }

    public void setFolioOs(String folioOs) {
        this.folioOs = folioOs;
    }

    public String getFolioOs() {
        return folioOs;
    }

    public void setEstatusActivacion(String estatusActivacion) {
        this.estatusActivacion = estatusActivacion;
    }

    public String getEstatusActivacion() {
        return estatusActivacion;
    }

    public void setFechaAprovisionamiento(String fechaAprovisionamiento) {
        this.fechaAprovisionamiento = fechaAprovisionamiento;
    }

    public String getFechaAprovisionamiento() {
        return fechaAprovisionamiento;
    }

    public void setOt(String ot) {
        this.ot = ot;
    }

    public String getOt() {
        return ot;
    }
}
