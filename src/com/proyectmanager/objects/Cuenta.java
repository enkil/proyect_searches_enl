package com.proyectmanager.objects;

public class Cuenta {
    
    private String id;
    private String tipoPersona;
    private String nombre;
    private String razonSocial;
    private String rfc;
    private String top5000;
    private String telefono;
    private String sector;
    private String folioCuenta;
    private String segmentoFacturacion;
    private String montoFacturacion;
    private String keyObject;
    private CotSitioPlan domicilioCuenta;
    private Oportunidad[] arrOportinidades;
    private CuentaFactura[] arrCuentasFacturas;
    private Contacto contactoPrincipal;
    private Contacto creadoPor;
    private Contacto modificadoPor;
    private Contacto propietarioCuenta;
    
    public Cuenta() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setFolioCuenta(String folioCuenta) {
        this.folioCuenta = folioCuenta;
    }

    public String getFolioCuenta() {
        return folioCuenta;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getRfc() {
        return rfc;
    }

    public void setTop5000(String top5000) {
        this.top5000 = top5000;
    }

    public String getTop5000() {
        return top5000;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSector() {
        return sector;
    }

    public void setSegmentoFacturacion(String segmentoFacturacion) {
        this.segmentoFacturacion = segmentoFacturacion;
    }

    public String getSegmentoFacturacion() {
        return segmentoFacturacion;
    }

    public void setMontoFacturacion(String montoFacturacion) {
        this.montoFacturacion = montoFacturacion;
    }

    public String getMontoFacturacion() {
        return montoFacturacion;
    }

    public void setDomicilioCuenta(CotSitioPlan domicilioCuenta) {
        this.domicilioCuenta = domicilioCuenta;
    }

    public CotSitioPlan getDomicilioCuenta() {
        return domicilioCuenta;
    }

    public void setContactoPrincipal(Contacto contactoPrincipal) {
        this.contactoPrincipal = contactoPrincipal;
    }

    public Contacto getContactoPrincipal() {
        return contactoPrincipal;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setModificadoPor(Contacto modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public Contacto getModificadoPor() {
        return modificadoPor;
    }

    public void setPropietarioCuenta(Contacto propietarioCuenta) {
        this.propietarioCuenta = propietarioCuenta;
    }

    public Contacto getPropietarioCuenta() {
        return propietarioCuenta;
    }

    public void setArrOportinidades(Oportunidad[] arrOportinidades) {
        this.arrOportinidades = arrOportinidades;
    }

    public Oportunidad[] getArrOportinidades() {
        return arrOportinidades;
    }

    public void setArrCuentasFacturas(CuentaFactura[] arrCuentasFacturas) {
        this.arrCuentasFacturas = arrCuentasFacturas;
    }

    public CuentaFactura[] getArrCuentasFacturas() {
        return arrCuentasFacturas;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }

}
