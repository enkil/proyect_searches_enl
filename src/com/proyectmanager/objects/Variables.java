package com.proyectmanager.objects;


public class Variables {
    
    private static String SF_USER;
    private static String SF_PASSWORD;
    private static String SF_END_POINT;
    
    //--- Metodo ConsultaProyectos
    private static String QR_BANDEJA_MESA_CONTROL;
    private static String QR_BANDEJA_FACTURANDO;
    private static String QR_BANDEJA_CANCELADO;
    private static String CONDICION_CONSULTAPROYECTOS;
    
    // Utilerias
    private static String COLOR_FUERA_TIEMPO;
    private static String COLOR_RIESGO;
    private static String COLOR_ENTIEMPO;      
    private static String PORCENTAJE_OK;
    private static String PORCENTAJE_RIESGO;
    private static String URL_DASHBOARD; 
    private static String SEGUNDOS_DOS_DIAS;
    private static String SEGUNDOS_MEDIODIA;
    private static String SEGUNDOS_UNDIA;
    
    //--- consulta actividades
    private static String KPI_PE;
    private static String KPI_PI;
    
    //--- Find proyect general salesforce enl
    private static String SEARCH_GENERAL_ENL_SF;

    public static void setURL_DASHBOARD(String URL_DASHBOARD) {
        Variables.URL_DASHBOARD = URL_DASHBOARD;
    }

    public static String getURL_DASHBOARD() {
        return URL_DASHBOARD;
    }

    public static void setKPI_PE(String KPI_PE) {
        Variables.KPI_PE = KPI_PE;
    }

    public static String getKPI_PE() {
        return KPI_PE;
    }

    public static void setKPI_PI(String KPI_PI) {
        Variables.KPI_PI = KPI_PI;
    }

    public static String getKPI_PI() {
        return KPI_PI;
    }

    public Variables() {
        super();
    }

    public static void setSF_USER(String SF_USER) {
        Variables.SF_USER = SF_USER;
    }

    public static String getSF_USER() {
        return SF_USER;
    }

    public static void setSF_PASSWORD(String SF_PASSWORD) {
        Variables.SF_PASSWORD = SF_PASSWORD;
    }

    public static String getSF_PASSWORD() {
        return SF_PASSWORD;
    }

    public static void setSF_END_POINT(String SF_END_POINT) {
        Variables.SF_END_POINT = SF_END_POINT;
    }

    public static String getSF_END_POINT() {
        return SF_END_POINT;
    }

    public static void setCONDICION_CONSULTAPROYECTOS(String CONDICION_CONSULTAPROYECTOS) {
        Variables.CONDICION_CONSULTAPROYECTOS = CONDICION_CONSULTAPROYECTOS;
    }

    public static String getCONDICION_CONSULTAPROYECTOS() {
        return CONDICION_CONSULTAPROYECTOS;
    }

    public static void setCOLOR_FUERA_TIEMPO(String COLOR_FUERA_TIEMPO) {
        Variables.COLOR_FUERA_TIEMPO = COLOR_FUERA_TIEMPO;
    }

    public static String getCOLOR_FUERA_TIEMPO() {
        return COLOR_FUERA_TIEMPO;
    }

    public static void setCOLOR_RIESGO(String COLOR_RIESGO) {
        Variables.COLOR_RIESGO = COLOR_RIESGO;
    }

    public static String getCOLOR_RIESGO() {
        return COLOR_RIESGO;
    }

    public static void setCOLOR_ENTIEMPO(String COLOR_ENTIEMPO) {
        Variables.COLOR_ENTIEMPO = COLOR_ENTIEMPO;
    }

    public static String getCOLOR_ENTIEMPO() {
        return COLOR_ENTIEMPO;
    }

    public static void setPORCENTAJE_OK(String PORCENTAJE_OK) {
        Variables.PORCENTAJE_OK = PORCENTAJE_OK;
    }

    public static String getPORCENTAJE_OK() {
        return PORCENTAJE_OK;
    }

    public static void setPORCENTAJE_RIESGO(String PORCENTAJE_RIESGO) {
        Variables.PORCENTAJE_RIESGO = PORCENTAJE_RIESGO;
    }

    public static String getPORCENTAJE_RIESGO() {
        return PORCENTAJE_RIESGO;
    }

    public static void setQR_BANDEJA_MESA_CONTROL(String QR_BANDEJA_MESA_CONTROL) {
        Variables.QR_BANDEJA_MESA_CONTROL = QR_BANDEJA_MESA_CONTROL;
    }

    public static String getQR_BANDEJA_MESA_CONTROL() {
        return QR_BANDEJA_MESA_CONTROL;
    }

    public static void setQR_BANDEJA_FACTURANDO(String QR_BANDEJA_FACTURANDO) {
        Variables.QR_BANDEJA_FACTURANDO = QR_BANDEJA_FACTURANDO;
    }

    public static String getQR_BANDEJA_FACTURANDO() {
        return QR_BANDEJA_FACTURANDO;
    }

    public static void setSEGUNDOS_DOS_DIAS(String SEGUNDOS_DOS_DIAS) {
        Variables.SEGUNDOS_DOS_DIAS = SEGUNDOS_DOS_DIAS;
    }

    public static String getSEGUNDOS_DOS_DIAS() {
        return SEGUNDOS_DOS_DIAS;
    }

    public static void setSEGUNDOS_MEDIODIA(String SEGUNDOS_MEDIODIA) {
        Variables.SEGUNDOS_MEDIODIA = SEGUNDOS_MEDIODIA;
    }

    public static String getSEGUNDOS_MEDIODIA() {
        return SEGUNDOS_MEDIODIA;
    }

    public static void setSEGUNDOS_UNDIA(String SEGUNDOS_UNDIA) {
        Variables.SEGUNDOS_UNDIA = SEGUNDOS_UNDIA;
    }

    public static String getSEGUNDOS_UNDIA() {
        return SEGUNDOS_UNDIA;
    }

    public static void setSEARCH_GENERAL_ENL_SF(String SEARCH_GENERAL_ENL_SF) {
        Variables.SEARCH_GENERAL_ENL_SF = SEARCH_GENERAL_ENL_SF;
    }

    public static String getSEARCH_GENERAL_ENL_SF() {
        return SEARCH_GENERAL_ENL_SF;
    }

    public static void setQR_BANDEJA_CANCELADO(String QR_BANDEJA_CANCELADO) {
        Variables.QR_BANDEJA_CANCELADO = QR_BANDEJA_CANCELADO;
    }

    public static String getQR_BANDEJA_CANCELADO() {
        return QR_BANDEJA_CANCELADO;
    }
}
