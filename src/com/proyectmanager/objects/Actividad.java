package com.proyectmanager.objects;

public class Actividad {
    public Actividad() {
        super();
    }
    
    private String idDetallePlaneacion;
    private String Id_sitio;
    private String Id_actividad;
    private String Id_tipo_actividad;
    private String Nombre_actividad;
    private String Nombre_responsable;
    private String Fecha_entrega;
    private String Porcentaje;
    private String Fecha_inicio_planeada;
    private String Fecha_fin_planeada;
    private String Fecha_inicio_real;
    private String Fecha_fin_real;
    private String Status;
    private String Id_dependencia;
    private String Fecha_creacion;
    private String Usuario_crea;
    private String Id_responsable;
    private String Semaforo;
    private String PorcentajeEsperado;
    private String Se_puede_eliminar;
    private String Tiene_Ot;
    private String Id_OT;
    private String Id_implementacion;
    private String Unidad_negocio;
    private String Tipo_intervencion;
    private String Subtipo_intervencion;
    private String Id_csp;
    private String Id_cuenta_factura;
    private String Num_cuenta_factura;
    private String Region;
    private String Ciudad;
    private String Distrito;
    private String Cluster;
    private String nombre_cot_sitio;
    private String paquete;
    private String Calle;
    private String Colonia;
    private String Estado;
    private String Municipio;
    private String Cp;
    private String Num_interior;
    private String Num_exterior;
    private String Latitud;
    private String Longitud;
    private String Nombre_responsable_sitio;
    private String Telefono_contacto;
    private String Tipo_archivo;
    private String Horas_favor;
    private String fechaActual;
    private String horasFavor;
    private String hoy;
    private String planeacionCerrada;
    private String planTermindo;
    private String estatusPlaneacion;
    private String enTiempo;


    public void setId_tipo_actividad(String Id_tipo_actividad) {
        this.Id_tipo_actividad = Id_tipo_actividad;
    }

    public String getId_tipo_actividad() {
        return Id_tipo_actividad;
    }

    public void setHorasFavor(String horasFavor) {
        this.horasFavor = horasFavor;
    }

    public String getHorasFavor() {
        return horasFavor;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Id_actividad;
    }

    public String getId_actividad() {
        return Id_actividad;
    }

    public void setNombre_actividad(String Nombre_actividad) {
        this.Nombre_actividad = Constantes.UTILS.toInitCap(Nombre_actividad);
    }

    public String getNombre_actividad() {
        return Nombre_actividad;
    }

    public void setNombre_responsable(String Nombre_responsable) {
        this.Nombre_responsable = Constantes.UTILS.toInitCap(Nombre_responsable);
    }

    public String getNombre_responsable() {
        return Nombre_responsable;
    }

    public void setFecha_entrega(String Fecha_entrega) {
        this.Fecha_entrega = Fecha_entrega;
    }

    public String getFecha_entrega() {
        return Fecha_entrega;
    }

    public void setPorcentaje(String Porcentaje) {
        this.Porcentaje = Porcentaje;
    }

    public String getPorcentaje() {
        return Porcentaje;
    }

    public void setFecha_inicio_planeada(String Fecha_inicio_planeada) {
        this.Fecha_inicio_planeada = Fecha_inicio_planeada;
    }

    public String getFecha_inicio_planeada() {
        return Fecha_inicio_planeada;
    }

    public void setFecha_fin_planeada(String Fecha_fin_planeada) {
        this.Fecha_fin_planeada = Fecha_fin_planeada;
    }

    public String getFecha_fin_planeada() {
        return Fecha_fin_planeada;
    }

    public void setFecha_inicio_real(String Fecha_inicio_real) {
        this.Fecha_inicio_real = Fecha_inicio_real;
    }

    public String getFecha_inicio_real() {
        return Fecha_inicio_real;
    }

    public void setFecha_fin_real(String Fecha_fin_real) {
        this.Fecha_fin_real = Fecha_fin_real;
    }

    public String getFecha_fin_real() {
        return Fecha_fin_real;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }

    public void setId_dependencia(String Id_dependencia) {
        this.Id_dependencia = Id_dependencia;
    }

    public String getId_dependencia() {
        return Id_dependencia;
    }

    public void setFecha_creacion(String Fecha_creacion) {
        this.Fecha_creacion = Fecha_creacion;
    }

    public String getFecha_creacion() {
        return Fecha_creacion;
    }

    public void setUsuario_crea(String Usuario_crea) {
        this.Usuario_crea = Constantes.UTILS.toInitCap(Usuario_crea);
    }

    public String getUsuario_crea() {
        return Usuario_crea;
    }

    public void setId_responsable(String Id_responsable) {
        this.Id_responsable = Id_responsable;
    }

    public String getId_responsable() {
        return Id_responsable;
    }

    public void setSemaforo(String Semaforo) {
        this.Semaforo = Semaforo;
    }

    public String getSemaforo() {
        return Semaforo;
    }

    public void setPorcentajeEsperado(String PorcentajeEsperado) {
        this.PorcentajeEsperado = PorcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return PorcentajeEsperado;
    }

    public void setSe_puede_eliminar(String Se_puede_eliminar) {
        this.Se_puede_eliminar = Se_puede_eliminar;
    }

    public String getSe_puede_eliminar() {
        return Se_puede_eliminar;
    }

    public void setTiene_Ot(String Tiene_Ot) {
        this.Tiene_Ot = Tiene_Ot;
    }

    public String getTiene_Ot() {
        return Tiene_Ot;
    }

    public void setUnidad_negocio(String Unidad_negocio) {
        this.Unidad_negocio = Unidad_negocio;
    }

    public String getUnidad_negocio() {
        return Unidad_negocio;
    }

    public void setTipo_intervencion(String Tipo_intervencion) {
        this.Tipo_intervencion = Tipo_intervencion;
    }

    public String getTipo_intervencion() {
        return Tipo_intervencion;
    }

    public void setSubtipo_intervencion(String Subtipo_intervencion) {
        this.Subtipo_intervencion = Subtipo_intervencion;
    }

    public String getSubtipo_intervencion() {
        return Subtipo_intervencion;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setId_cuenta_factura(String Id_cuenta_factura) {
        this.Id_cuenta_factura = Id_cuenta_factura;
    }

    public String getId_cuenta_factura() {
        return Id_cuenta_factura;
    }

    public void setNum_cuenta_factura(String Num_cuenta_factura) {
        this.Num_cuenta_factura = Num_cuenta_factura;
    }

    public String getNum_cuenta_factura() {
        return Num_cuenta_factura;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getRegion() {
        return Region;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setDistrito(String Distrito) {
        this.Distrito = Distrito;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setCluster(String Cluster) {
        this.Cluster = Cluster;
    }

    public String getCluster() {
        return Cluster;
    }

    public void setNombre_cot_sitio(String nombre_cot_sitio) {
        this.nombre_cot_sitio = nombre_cot_sitio;
    }

    public String getNombre_cot_sitio() {
        return nombre_cot_sitio;
    }

    public void setPaquete(String paquete) {
        this.paquete = Constantes.UTILS.toInitCap(paquete);
    }

    public String getPaquete() {
        return paquete;
    }

    public void setCalle(String Calle) {
        this.Calle = Constantes.UTILS.toInitCap(Calle);
    }

    public String getCalle() {
        return Calle;
    }

    public void setColonia(String Colonia) {
        this.Colonia = Constantes.UTILS.toInitCap(Colonia);
    }

    public String getColonia() {
        return Colonia;
    }

    public void setEstado(String Estado) {
        this.Estado = Constantes.UTILS.toInitCap(Estado);
    }

    public String getEstado() {
        return Estado;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Constantes.UTILS.toInitCap(Municipio);
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setCp(String Cp) {
        this.Cp = Cp;
    }

    public String getCp() {
        return Cp;
    }

    public void setNum_interior(String Num_interior) {
        this.Num_interior = Num_interior;
    }

    public String getNum_interior() {
        return Num_interior;
    }

    public void setNum_exterior(String Num_exterior) {
        this.Num_exterior = Num_exterior;
    }

    public String getNum_exterior() {
        return Num_exterior;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setNombre_responsable_sitio(String Nombre_responsable_sitio) {
        this.Nombre_responsable_sitio = Constantes.UTILS.toInitCap(Nombre_responsable_sitio);
    }

    public String getNombre_responsable_sitio() {
        return Nombre_responsable_sitio;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setTipo_archivo(String Tipo_archivo) {
        this.Tipo_archivo = Tipo_archivo;
    }

    public String getTipo_archivo() {
        return Tipo_archivo;
    }

    public void setId_sitio(String Id_sitio) {
        this.Id_sitio = Id_sitio;
    }

    public String getId_sitio() {
        return Id_sitio;
    }

    public void setHoras_favor(String Horas_favor) {
        this.Horas_favor = Horas_favor;
    }

    public String getHoras_favor() {
        return Horas_favor;
    }

    public void setId_OT(String Id_OT) {
        this.Id_OT = Id_OT;
    }

    public String getId_OT() {
        return Id_OT;
    }

    public void setHoy(String hoy) {
        this.hoy = hoy;
    }

    public String getHoy() {
        return hoy;
    }

    public void setPlaneacionCerrada(String planeacionCerrada) {
        this.planeacionCerrada = planeacionCerrada;
    }

    public String getPlaneacionCerrada() {
        return planeacionCerrada;
    }

    public void setIdDetallePlaneacion(String idDetallePlaneacion) {
        this.idDetallePlaneacion = idDetallePlaneacion;
    }

    public String getIdDetallePlaneacion() {
        return idDetallePlaneacion;
    }

    public void setPlanTermindo(String planTermindo) {
        this.planTermindo = planTermindo;
    }

    public String getPlanTermindo() {
        return planTermindo;
    }

    public void setEnTiempo(String enTiempo) {
        this.enTiempo = enTiempo;
    }

    public String getEnTiempo() {
        return enTiempo;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setId_implementacion(String Id_implementacion) {
        this.Id_implementacion = Id_implementacion;
    }

    public String getId_implementacion() {
        return Id_implementacion;
    }
}
