package com.proyectmanager.objects;

public class CotSitioPlan {
    public CotSitioPlan() {
        super();
    }
    
    private String nombreCliente;
    private String id;
    private String nombre;
    private String nombrePlan;
    private String accesoPrincipal;
    
    private String idDetallePlaneacion;
    private String Id_cuenta;
    private String Id_cot_sitio;
    private String Id_csp;
    private String Folio_CSP;
    private String Paquete;
    private String Canal_venta;
    private String Tipo_cuadrilla;
    private String Tipo_acceso;
    private String Unidad_negocio;
    private String Tipo_intervencion;
    private String Subtipo_intervencion;
    private String Region;
    private String Ciudad;
    private String Distrito;
    private String Cluster;
    private String Calle;
    private String Colonia;
    private String Estado;
    private String Municipio;
    private String Num_interior;
    private String Num_exterior;
    private String Latitud;
    private String Longitud;
    private String CP;
    private String Telefono_contacto;
    private String Fecha_aprovisionamiento;   
    private String Id_cuenta_factura;
    private String Num_cuenta_factura;
    private String Id_os;
    private String Folio_OS;
    private String Status_OS;
    private String statusCsp;
    private String tipoPlan;
    private String fechaOsCompletada;
    private String fechaOsCancelada;
    
    private String Id_sitio;
    private String Nombre_csp;
    private String Nombre_responsable_sitio;
    private String Fecha_Aprovisionamiento;
    private String Cuandrilla;
    private String Id_cotizacion;
    private String Id_pm;
    private String Id_OS;
    private String Folio_os;
    private String Folio_cotizacion;
    private String Id_oportunidad;
    private String Numero_oportunidad;
    private String Fecha_cierre;
    private String Plazo;
    private String Numero_cuentaFactura;
    private String top5000;
    
    
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;
    private String Num_fuera_tiempo;
    private String Num_en_riesgo;
    private String Num_en_tiempo;
    private String Num_pendientes;
    private String PlaneacionCerrada;
    private String implementacionterminada;
    private String estatusActivacion;
    private String tipoRegistroCf;
    private String tipo_Oportunidad;
    private String aprovisionamientoTe;
    private String oportunidad_Tipo;
    private String nombreCuentaComercial;
    private String nombreOportunidad;
    private String tipoCotizacionPM;
    private String sobrePrecio;
    private String keyObject;
    private Cotizacion detalleCotizacion;
    private CotSitio detalleCotSitio;
    private CuentaFactura detalleCuentaFactura;
    private Os detalleOs;
    private Contacto creadoPor;
    private Contacto editadoPor;
    private Contacto propietario;
    private String estatusPlaneacion;
    private String enTiempo;
    private BanerServicio informacionServicio;
    
    private String folioCotSitio;
    private String direccionSitio;
    private String plaza;
    private String subtotalRenta;
    private String totalRentaConImpuesto;
    private String tipoOportunidad;
    private String folioCotizacion;
    private String nombreSitio;
    private String folioSitio;
    private String tipoCobertura;
    private String latitude;
    private String longitude;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setAccesoPrincipal(String accesoPrincipal) {
        this.accesoPrincipal = accesoPrincipal;
    }

    public String getAccesoPrincipal() {
        return accesoPrincipal;
    }

    public void setSemaforo(String semaforo) {
        this.semaforo = semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setId_pm(String Id_pm) {
        this.Id_pm = Id_pm;
    }

    public String getId_pm() {
        return Id_pm;
    }

    public void setId_sitio(String Id_sitio) {
        this.Id_sitio = Id_sitio;
    }

    public String getId_sitio() {
        return Id_sitio;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }

    public void setNombre_csp(String Nombre_csp) {
        this.Nombre_csp = Nombre_csp;
    }

    public String getNombre_csp() {
        return Nombre_csp;
    }

    public void setId_cuenta_factura(String Id_cuenta_factura) {
        this.Id_cuenta_factura = Id_cuenta_factura;
    }

    public String getId_cuenta_factura() {
        return Id_cuenta_factura;
    }

    public void setNum_cuenta_factura(String Num_cuenta_factura) {
        this.Num_cuenta_factura = Num_cuenta_factura;
    }

    public String getNum_cuenta_factura() {
        return Num_cuenta_factura;
    }

    public void setId_os(String Id_os) {
        this.Id_os = Id_os;
    }

    public String getId_os() {
        return Id_os;
    }

    public void setFolio_os(String Folio_os) {
        this.Folio_os = Folio_os;
    }

    public String getFolio_os() {
        return Folio_os;
    }

    public void setPaquete(String Paquete) {
        this.Paquete = Paquete;
    }

    public String getPaquete() {
        return Paquete;
    }

    public void setNombre_responsable_sitio(String Nombre_responsable_sitio) {
        this.Nombre_responsable_sitio = Nombre_responsable_sitio;
    }

    public String getNombre_responsable_sitio() {
        return Nombre_responsable_sitio;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setUnidad_negocio(String Unidad_negocio) {
        this.Unidad_negocio = Unidad_negocio;
    }

    public String getUnidad_negocio() {
        return Unidad_negocio;
    }

    public void setTipo_intervencion(String Tipo_intervencion) {
        this.Tipo_intervencion = Tipo_intervencion;
    }

    public String getTipo_intervencion() {
        return Tipo_intervencion;
    }

    public void setSubtipo_intervencion(String Subtipo_intervencion) {
        this.Subtipo_intervencion = Subtipo_intervencion;
    }

    public String getSubtipo_intervencion() {
        return Subtipo_intervencion;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getRegion() {
        return Region;
    }

    public void setCiudad(String Ciudad) {
        this.Ciudad = Ciudad;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setDistrito(String Distrito) {
        this.Distrito = Distrito;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setCluster(String Cluster) {
        this.Cluster = Cluster;
    }

    public String getCluster() {
        return Cluster;
    }

    public void setCalle(String Calle) {
        this.Calle = Calle;
    }

    public String getCalle() {
        return Calle;
    }

    public void setColonia(String Colonia) {
        this.Colonia = Colonia;
    }

    public String getColonia() {
        return Colonia;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getEstado() {
        return Estado;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setNum_interior(String Num_interior) {
        this.Num_interior = Num_interior;
    }

    public String getNum_interior() {
        return Num_interior;
    }

    public void setNum_exterior(String Num_exterior) {
        this.Num_exterior = Num_exterior;
    }

    public String getNum_exterior() {
        return Num_exterior;
    }

    public void setLatitud(String Latitud) {
        this.Latitud = Latitud;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLongitud(String Longitud) {
        this.Longitud = Longitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public void setCuandrilla(String Cuandrilla) {
        this.Cuandrilla = Cuandrilla;
    }

    public String getCuandrilla() {
        return Cuandrilla;
    }

    public String getCP() {
        return CP;
    }

    public void setFecha_Aprovisionamiento(String Fecha_Aprovisionamiento) {
        this.Fecha_Aprovisionamiento = Fecha_Aprovisionamiento;
    }

    public String getFecha_Aprovisionamiento() {
        return Fecha_Aprovisionamiento;
    }

    public void setFolio_CSP(String Folio_CSP) {
        this.Folio_CSP = Folio_CSP;
    }

    public String getFolio_CSP() {
        return Folio_CSP;
    }

    public void setFecha_aprovisionamiento(String Fecha_aprovisionamiento) {
        this.Fecha_aprovisionamiento = Fecha_aprovisionamiento;
    }

    public String getFecha_aprovisionamiento() {
        return Fecha_aprovisionamiento;
    }

    public void setTipo_cuadrilla(String Tipo_cuadrilla) {
        this.Tipo_cuadrilla = Tipo_cuadrilla;
    }

    public String getTipo_cuadrilla() {
        return Tipo_cuadrilla;
    }

    public void setTipo_acceso(String Tipo_acceso) {
        this.Tipo_acceso = Tipo_acceso;
    }

    public String getTipo_acceso() {
        return Tipo_acceso;
    }

    public void setId_OS(String Id_OS) {
        this.Id_OS = Id_OS;
    }

    public String getId_OS() {
        return Id_OS;
    }

    public void setFolio_OS(String Folio_OS) {
        this.Folio_OS = Folio_OS;
    }

    public String getFolio_OS() {
        return Folio_OS;
    }

    public void setStatus_OS(String Status_punta) {
        this.Status_OS = Status_punta;
    }

    public String getStatus_OS() {
        return Status_OS;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setId_oportunidad(String Id_oportunidad) {
        this.Id_oportunidad = Id_oportunidad;
    }

    public String getId_oportunidad() {
        return Id_oportunidad;
    }

    public void setNumero_oportunidad(String Numero_oportunidad) {
        this.Numero_oportunidad = Numero_oportunidad;
    }

    public String getNumero_oportunidad() {
        return Numero_oportunidad;
    }

    public void setFecha_cierre(String Fecha_cierre) {
        this.Fecha_cierre = Fecha_cierre;
    }

    public String getFecha_cierre() {
        return Fecha_cierre;
    }

    public void setPlazo(String Plazo) {
        this.Plazo = Plazo;
    }

    public String getPlazo() {
        return Plazo;
    }

    public void setId_cuenta(String Id_cuenta) {
        this.Id_cuenta = Id_cuenta;
    }

    public String getId_cuenta() {
        return Id_cuenta;
    }

    public void setNumero_cuentaFactura(String Numero_cuentaFactura) {
        this.Numero_cuentaFactura = Numero_cuentaFactura;
    }

    public String getNumero_cuentaFactura() {
        return Numero_cuentaFactura;
    }

    public void setCanal_venta(String Canal_venta) {
        this.Canal_venta = Canal_venta;
    }

    public String getCanal_venta() {
        return Canal_venta;
    }

    public void setId_cot_sitio(String Id_cot_sitio) {
        this.Id_cot_sitio = Id_cot_sitio;
    }

    public String getId_cot_sitio() {
        return Id_cot_sitio;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return porcentajeEsperado;
    }

    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setPlaneacionCerrada(String PlaneacionCerrada) {
        this.PlaneacionCerrada = PlaneacionCerrada;
    }

    public String getPlaneacionCerrada() {
        return PlaneacionCerrada;
    }

    public void setTipoPlan(String tipoPlan) {
        this.tipoPlan = tipoPlan;
    }

    public String getTipoPlan() {
        return tipoPlan;
    }

    public void setEstatusActivacion(String estatusActivacion) {
        this.estatusActivacion = estatusActivacion;
    }

    public String getEstatusActivacion() {
        return estatusActivacion;
    }

    public void setTipoRegistroCf(String tipoRegistroCf) {
        this.tipoRegistroCf = tipoRegistroCf;
    }

    public String getTipoRegistroCf() {
        return tipoRegistroCf;
    }

    public void setTipo_Oportunidad(String tipo_Oportunidad) {
        this.tipo_Oportunidad = tipo_Oportunidad;
    }

    public String getTipo_Oportunidad() {
        return tipo_Oportunidad;
    }

    public void setAprovisionamientoTe(String aprovisionamientoTe) {
        this.aprovisionamientoTe = aprovisionamientoTe;
    }

    public String getAprovisionamientoTe() {
        return aprovisionamientoTe;
    }

    public void setOportunidad_Tipo(String oportunidad_Tipo) {
        this.oportunidad_Tipo = oportunidad_Tipo;
    }

    public String getOportunidad_Tipo() {
        return oportunidad_Tipo;
    }

    public void setNombreCuentaComercial(String nombreCuentaComercial) {
        this.nombreCuentaComercial = nombreCuentaComercial;
    }

    public String getNombreCuentaComercial() {
        return nombreCuentaComercial;
    }

    public void setNombreOportunidad(String nombreOportunidad) {
        this.nombreOportunidad = nombreOportunidad;
    }

    public String getNombreOportunidad() {
        return nombreOportunidad;
    }

    public void setTipoCotizacionPM(String tipoCotizacionPM) {
        this.tipoCotizacionPM = tipoCotizacionPM;
    }

    public String getTipoCotizacionPM() {
        return tipoCotizacionPM;
    }

    public void setDetalleCotizacion(Cotizacion detalleCotizacion) {
        this.detalleCotizacion = detalleCotizacion;
    }

    public Cotizacion getDetalleCotizacion() {
        return detalleCotizacion;
    }

    public void setDetalleCotSitio(CotSitio detalleCotSitio) {
        this.detalleCotSitio = detalleCotSitio;
    }

    public CotSitio getDetalleCotSitio() {
        return detalleCotSitio;
    }

    public void setDetalleCuentaFactura(CuentaFactura detalleCuentaFactura) {
        this.detalleCuentaFactura = detalleCuentaFactura;
    }

    public CuentaFactura getDetalleCuentaFactura() {
        return detalleCuentaFactura;
    }

    public void setDetalleOs(Os detalleOs) {
        this.detalleOs = detalleOs;
    }

    public Os getDetalleOs() {
        return detalleOs;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setEditadoPor(Contacto editadoPor) {
        this.editadoPor = editadoPor;
    }

    public Contacto getEditadoPor() {
        return editadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }

    public void setSobrePrecio(String sobrePrecio) {
        this.sobrePrecio = sobrePrecio;
    }

    public String getSobrePrecio() {
        return sobrePrecio;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }

    public void setIdDetallePlaneacion(String idDetallePlaneacion) {
        this.idDetallePlaneacion = idDetallePlaneacion;
    }

    public String getIdDetallePlaneacion() {
        return idDetallePlaneacion;
    }

    public void setImplementacionterminada(String implementacionterminada) {
        this.implementacionterminada = implementacionterminada;
    }

    public String getImplementacionterminada() {
        return implementacionterminada;
    }

    public void setEnTiempo(String enTiempo) {
        this.enTiempo = enTiempo;
    }

    public String getEnTiempo() {
        return enTiempo;
    }

    public void setEstatusPlaneacion(String estatusPlaneacion) {
        this.estatusPlaneacion = estatusPlaneacion;
    }

    public String getEstatusPlaneacion() {
        return estatusPlaneacion;
    }

    public void setInformacionServicio(BanerServicio informacionServicio) {
        this.informacionServicio = informacionServicio;
    }

    public BanerServicio getInformacionServicio() {
        return informacionServicio;
    }

    public void setFolioCotSitio(String folioCotSitio) {
        this.folioCotSitio = folioCotSitio;
    }

    public String getFolioCotSitio() {
        return folioCotSitio;
    }

    public void setDireccionSitio(String direccionSitio) {
        this.direccionSitio = direccionSitio;
    }

    public String getDireccionSitio() {
        return direccionSitio;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setSubtotalRenta(String subtotalRenta) {
        this.subtotalRenta = subtotalRenta;
    }

    public String getSubtotalRenta() {
        return subtotalRenta;
    }

    public void setTotalRentaConImpuesto(String totalRentaConImpuesto) {
        this.totalRentaConImpuesto = totalRentaConImpuesto;
    }

    public String getTotalRentaConImpuesto() {
        return totalRentaConImpuesto;
    }

    public void setTipoOportunidad(String tipoOportunidad) {
        this.tipoOportunidad = tipoOportunidad;
    }

    public String getTipoOportunidad() {
        return tipoOportunidad;
    }

    public void setFolioCotizacion(String folioCotizacion) {
        this.folioCotizacion = folioCotizacion;
    }

    public String getFolioCotizacion() {
        return folioCotizacion;
    }

    public void setNombreSitio(String nombreSitio) {
        this.nombreSitio = nombreSitio;
    }

    public String getNombreSitio() {
        return nombreSitio;
    }

    public void setFolioSitio(String folioSitio) {
        this.folioSitio = folioSitio;
    }

    public String getFolioSitio() {
        return folioSitio;
    }

    public void setTipoCobertura(String tipoCobertura) {
        this.tipoCobertura = tipoCobertura;
    }

    public String getTipoCobertura() {
        return tipoCobertura;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setStatusCsp(String statusCsp) {
        this.statusCsp = statusCsp;
    }

    public String getStatusCsp() {
        return statusCsp;
    }

    public void setTop5000(String top5000) {
        this.top5000 = top5000;
    }

    public String getTop5000() {
        return top5000;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setFechaOsCompletada(String fechaOsCompletada) {
        this.fechaOsCompletada = fechaOsCompletada;
    }

    public String getFechaOsCompletada() {
        return fechaOsCompletada;
    }

    public void setFechaOsCancelada(String fechaOsCancelada) {
        this.fechaOsCancelada = fechaOsCancelada;
    }

    public String getFechaOsCancelada() {
        return fechaOsCancelada;
    }
}
