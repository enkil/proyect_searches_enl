package com.proyectmanager.objects;

public class DetalleInstalacion {
    public DetalleInstalacion() {
        super();
    }
    
    private String Nombre;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String Nombre_Tecnico;
    private String NumTelefonico;
    private String Automovil;
    
    public void setNombre_Tecnico(String Nombre_Tecnico) {
        this.Nombre_Tecnico = Nombre_Tecnico;
    }

    public String getNombre_Tecnico() {
        return Nombre_Tecnico;
    }

    public void setNumTelefonico(String NumTelefonico) {
        this.NumTelefonico = NumTelefonico;
    }

    public String getNumTelefonico() {
        return NumTelefonico;
    }

    public void setAutomovil(String Automovil) {
        this.Automovil = Automovil;
    }

    public String getAutomovil() {
        return Automovil;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }


}
