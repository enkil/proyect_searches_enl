package com.proyectmanager.objects;

public class Cotizacion {
    
    private String id;
    private String nombre;
    private String segmentoFacturacion;
    private String tipoCotizacion;
    private String propietarioOportunidad;
    private String estatus;
    private String vigenciaCotizacion;    
    private String plazo;
    private String cotizacionPrincipal;
    private String tipoCotizacionPm;
    private String region;
    private String plaza;
    private String segmentoGerencia;
    private String inicioProyecto;
    private String puntasTotales;
    private String top5mil;
    private String keyObject;
    private Contacto asignadoPM;
    private ResumenCotizacion resumenCotizacion;
    private Oportunidad detalleOportunidad;
    private Cuenta detalleCuenta;
    private CotSitioPlan detalleCotSitioPlan;
    private CotSitio[] arrCotSitio;
    private CotSitioPlan[] arrCotSitioPlan;
    private Contacto creadoPor;
    private Contacto modificadoPor;
    private Contacto propietario;
        
    
    public Cotizacion() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setVigenciaCotizacion(String vigenciaCotizacion) {
        this.vigenciaCotizacion = vigenciaCotizacion;
    }

    public String getVigenciaCotizacion() {
        return vigenciaCotizacion;
    }

    public void setSegmentoFacturacion(String segmentoFacturacion) {
        this.segmentoFacturacion = segmentoFacturacion;
    }

    public String getSegmentoFacturacion() {
        return segmentoFacturacion;
    }

    public void setTipoCotizacion(String tipoCotizacion) {
        this.tipoCotizacion = tipoCotizacion;
    }

    public String getTipoCotizacion() {
        return tipoCotizacion;
    }

    public void setPropietarioOportunidad(String propietarioOportunidad) {
        this.propietarioOportunidad = propietarioOportunidad;
    }

    public String getPropietarioOportunidad() {
        return propietarioOportunidad;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setCotizacionPrincipal(String cotizacionPrincipal) {
        this.cotizacionPrincipal = cotizacionPrincipal;
    }

    public String getCotizacionPrincipal() {
        return cotizacionPrincipal;
    }

    public void setTipoCotizacionPm(String tipoCotizacionPm) {
        this.tipoCotizacionPm = tipoCotizacionPm;
    }

    public String getTipoCotizacionPm() {
        return tipoCotizacionPm;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setSegmentoGerencia(String segmentoGerencia) {
        this.segmentoGerencia = segmentoGerencia;
    }

    public String getSegmentoGerencia() {
        return segmentoGerencia;
    }

    public void setInicioProyecto(String inicioProyecto) {
        this.inicioProyecto = inicioProyecto;
    }

    public String getInicioProyecto() {
        return inicioProyecto;
    }

    public void setPuntasTotales(String puntasTotales) {
        this.puntasTotales = puntasTotales;
    }

    public String getPuntasTotales() {
        return puntasTotales;
    }

    public void setTop5mil(String top5mil) {
        this.top5mil = top5mil;
    }

    public String getTop5mil() {
        return top5mil;
    }

    public void setAsignadoPM(Contacto asignadoPM) {
        this.asignadoPM = asignadoPM;
    }

    public Contacto getAsignadoPM() {
        return asignadoPM;
    }

    public void setDetalleOportunidad(Oportunidad detalleOportunidad) {
        this.detalleOportunidad = detalleOportunidad;
    }

    public Oportunidad getDetalleOportunidad() {
        return detalleOportunidad;
    }

    public void setDetalleCuenta(Cuenta detalleCuenta) {
        this.detalleCuenta = detalleCuenta;
    }

    public Cuenta getDetalleCuenta() {
        return detalleCuenta;
    }

    public void setDetalleCotSitioPlan(CotSitioPlan detalleCotSitioPlan) {
        this.detalleCotSitioPlan = detalleCotSitioPlan;
    }

    public CotSitioPlan getDetalleCotSitioPlan() {
        return detalleCotSitioPlan;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setModificadoPor(Contacto modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public Contacto getModificadoPor() {
        return modificadoPor;
    }

    public void setPropietario(Contacto propietario) {
        this.propietario = propietario;
    }

    public Contacto getPropietario() {
        return propietario;
    }

    public void setArrCotSitio(CotSitio[] arrCotSitio) {
        this.arrCotSitio = arrCotSitio;
    }

    public CotSitio[] getArrCotSitio() {
        return arrCotSitio;
    }

    public void setArrCotSitioPlan(CotSitioPlan[] arrCotSitioPlan) {
        this.arrCotSitioPlan = arrCotSitioPlan;
    }

    public CotSitioPlan[] getArrCotSitioPlan() {
        return arrCotSitioPlan;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }

    public void setResumenCotizacion(ResumenCotizacion resumenCotizacion) {
        this.resumenCotizacion = resumenCotizacion;
    }

    public ResumenCotizacion getResumenCotizacion() {
        return resumenCotizacion;
    }
}
