package com.proyectmanager.objects;

public class Oportunidad {
    
    private String id;
    private String nombre;
    private String numeroOportunidad;
    private String etapa;
    private String tipo;
    private String subTipo;
    private String origenProspecto;
    private String segmentoFacturacion;
    private String montoFacturacion;
    private String fechaCierre;
    private String plazo;
    private String plaza;
    private String segmento;
    private String keyObject;
    private Cuenta datosCuenta;
    private Contacto creadoPor;
    private Contacto modificadoPor;
    private Contacto propietarioCuenta;
    
    
    public Oportunidad() {
        super();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNumeroOportunidad(String numeroOportunidad) {
        this.numeroOportunidad = numeroOportunidad;
    }

    public String getNumeroOportunidad() {
        return numeroOportunidad;
    }

    public void setEtapa(String etapa) {
        this.etapa = etapa;
    }

    public String getEtapa() {
        return etapa;
    }

    public void setDatosCuenta(Cuenta datosCuenta) {
        this.datosCuenta = datosCuenta;
    }

    public Cuenta getDatosCuenta() {
        return datosCuenta;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }

    public String getSubTipo() {
        return subTipo;
    }

    public void setOrigenProspecto(String origenProspecto) {
        this.origenProspecto = origenProspecto;
    }

    public String getOrigenProspecto() {
        return origenProspecto;
    }

    public void setSegmentoFacturacion(String segmentoFacturacion) {
        this.segmentoFacturacion = segmentoFacturacion;
    }

    public String getSegmentoFacturacion() {
        return segmentoFacturacion;
    }

    public void setMontoFacturacion(String montoFacturacion) {
        this.montoFacturacion = montoFacturacion;
    }

    public String getMontoFacturacion() {
        return montoFacturacion;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlaza(String plaza) {
        this.plaza = plaza;
    }

    public String getPlaza() {
        return plaza;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setCreadoPor(Contacto creadoPor) {
        this.creadoPor = creadoPor;
    }

    public Contacto getCreadoPor() {
        return creadoPor;
    }

    public void setModificadoPor(Contacto modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public Contacto getModificadoPor() {
        return modificadoPor;
    }

    public void setPropietarioCuenta(Contacto propietarioCuenta) {
        this.propietarioCuenta = propietarioCuenta;
    }

    public Contacto getPropietarioCuenta() {
        return propietarioCuenta;
    }

    public void setKeyObject(String keyObject) {
        this.keyObject = keyObject;
    }

    public String getKeyObject() {
        return keyObject;
    }
}
