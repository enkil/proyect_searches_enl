package com.proyectmanager.objects;

public class OrtdenTrabajo {
    public OrtdenTrabajo() {
        super();
    }
    
    private String Id_ot;
    private String Fecha_agenda;
    private String Turno;
    private String Os;
    private String Tecnico;
    private String Automovil;
    private String Desc_status;
    private String Id_estado;
    private String Desc_estado;
    private String Desc_motivo;
    private String Fecha_termino;
    private String Nombre_responsable;
    private String ApellidoPaterno_responsable;
    private String ApellidoMaterno_responsable;
    private String Telefono_responsable;
    private String Detencion_pe;
    private String Id_intervencion;
    private String Id_subIntervencion;
    private String Unidad_negocio;
    private String Descripcion;
    private String Avance;
    
    private String Id_reg_ot_act;
    private String Id_actividad;
    private String Id_csp;

    public void setId_ot(String Id_ot) {
        this.Id_ot = Id_ot;
    }

    public void setAvance(String Avance) {
        this.Avance = Avance;
    }

    public String getAvance() {
        return Avance;
    }

    public String getId_ot() {
        return Id_ot;
    }

    public void setFecha_agenda(String Fecha_agenda) {
        this.Fecha_agenda = Fecha_agenda;
    }

    public String getFecha_agenda() {
        return Fecha_agenda;
    }

    public void setTurno(String Turno) {
        this.Turno = Turno;
    }

    public String getTurno() {
        return Turno;
    }

    public void setOs(String Os) {
        this.Os = Os;
    }

    public String getOs() {
        return Os;
    }

    public void setTecnico(String Tecnico) {
        this.Tecnico = Tecnico;
    }

    public String getTecnico() {
        return Tecnico;
    }

    public void setAutomovil(String Automovil) {
        this.Automovil = Automovil;
    }

    public String getAutomovil() {
        return Automovil;
    }

    public void setDesc_status(String Desc_status) {
        this.Desc_status = Desc_status;
    }

    public String getDesc_status() {
        return Desc_status;
    }

    public void setId_estado(String Id_estado) {
        this.Id_estado = Id_estado;
    }

    public String getId_estado() {
        return Id_estado;
    }

    public void setDesc_estado(String Desc_estado) {
        this.Desc_estado = Desc_estado;
    }

    public String getDesc_estado() {
        return Desc_estado;
    }

    public void setDesc_motivo(String Desc_motivo) {
        this.Desc_motivo = Desc_motivo;
    }

    public String getDesc_motivo() {
        return Desc_motivo;
    }

    public void setFecha_termino(String Fecha_termino) {
        this.Fecha_termino = Fecha_termino;
    }

    public String getFecha_termino() {
        return Fecha_termino;
    }

    public void setNombre_responsable(String Nombre_responsable) {
        this.Nombre_responsable = Nombre_responsable;
    }

    public String getNombre_responsable() {
        return Nombre_responsable;
    }

    public void setApellidoPaterno_responsable(String ApellidoPaterno_responsable) {
        this.ApellidoPaterno_responsable = ApellidoPaterno_responsable;
    }

    public String getApellidoPaterno_responsable() {
        return ApellidoPaterno_responsable;
    }

    public void setApellidoMaterno_responsable(String ApellidoMaterno_responsable) {
        this.ApellidoMaterno_responsable = ApellidoMaterno_responsable;
    }

    public String getApellidoMaterno_responsable() {
        return ApellidoMaterno_responsable;
    }

    public void setTelefono_responsable(String Telefono_responsable) {
        this.Telefono_responsable = Telefono_responsable;
    }

    public String getTelefono_responsable() {
        return Telefono_responsable;
    }

    public void setDetencion_pe(String Detencion_pe) {
        this.Detencion_pe = Detencion_pe;
    }

    public String getDetencion_pe() {
        return Detencion_pe;
    }

    public void setId_intervencion(String Id_intervencion) {
        this.Id_intervencion = Id_intervencion;
    }

    public String getId_intervencion() {
        return Id_intervencion;
    }

    public void setId_subIntervencion(String Id_subIntervencion) {
        this.Id_subIntervencion = Id_subIntervencion;
    }

    public String getId_subIntervencion() {
        return Id_subIntervencion;
    }

    public void setUnidad_negocio(String Unidad_negocio) {
        this.Unidad_negocio = Unidad_negocio;
    }

    public String getUnidad_negocio() {
        return Unidad_negocio;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setId_reg_ot_act(String Id_reg_ot_act) {
        this.Id_reg_ot_act = Id_reg_ot_act;
    }

    public String getId_reg_ot_act() {
        return Id_reg_ot_act;
    }

    public void setId_actividad(String Id_actividad) {
        this.Id_actividad = Id_actividad;
    }

    public String getId_actividad() {
        return Id_actividad;
    }

    public void setId_csp(String Id_csp) {
        this.Id_csp = Id_csp;
    }

    public String getId_csp() {
        return Id_csp;
    }
}
