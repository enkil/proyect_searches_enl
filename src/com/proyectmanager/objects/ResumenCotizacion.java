package com.proyectmanager.objects;

public class ResumenCotizacion {
    
    private String totalIncluidos;
    private String totalAdicionales;
    private String subtotalRenta;
    private String totalRentaConDescuento;
    private String totalRentaConImpuesto;
    
    public ResumenCotizacion() {
        super();
    }

    public void setTotalIncluidos(String totalIncluidos) {
        this.totalIncluidos = totalIncluidos;
    }

    public String getTotalIncluidos() {
        return totalIncluidos;
    }

    public void setTotalAdicionales(String totalAdicionales) {
        this.totalAdicionales = totalAdicionales;
    }

    public String getTotalAdicionales() {
        return totalAdicionales;
    }

    public void setSubtotalRenta(String subtotalRenta) {
        this.subtotalRenta = subtotalRenta;
    }

    public String getSubtotalRenta() {
        return subtotalRenta;
    }

    public void setTotalRentaConDescuento(String totalRentaConDescuento) {
        this.totalRentaConDescuento = totalRentaConDescuento;
    }

    public String getTotalRentaConDescuento() {
        return totalRentaConDescuento;
    }

    public void setTotalRentaConImpuesto(String totalRentaConImpuesto) {
        this.totalRentaConImpuesto = totalRentaConImpuesto;
    }

    public String getTotalRentaConImpuesto() {
        return totalRentaConImpuesto;
    }
}
