package com.proyectmanager.objects;

public class Proyecto {
    public Proyecto() {
        super();
    }
    
    private String Id_cuenta;
    private String Nombre_cliente;
    private String Es_Top_5000;
    private String Segmento;
    private String Sector;
    private String Nombre_contacto;
    private String Tel_contacto;
    private String Fecha_inicio;
    private String Fecha_fin;
    private String Fecha_finPlaneada;
    private String Avance;
    private String Total_proyectos;
    private String Num_fuera_tiempo;
    private String Num_en_riesgo;
    private String Num_en_tiempo;
    private String Num_pendientes;
    private String OS_pendientes;
    private String Num_confirmado;
    private String Num_proceso;
    private String Num_restcate;
    private String Num_Cancelado;
    private String Num_detenida;
    private String Num_gestoria;
    private String Num_suspendido;
    private String Num_completado;
    private String Num_calendarizado;
    private String Num_otro;
    private String totalPuntas;
    private String totalPuntasPorInstalar;
    private String totalPuntasCalendarizado;
    private String totalPuntasDetenido;
    private String totalPuntasDevueltoVentas;
    private String totalPuntasCancelado;
    private String totalPuntasInstalado;
    
    private String fechaInicioPlaneada;
    private String fechaFinPlaneada;
    private String fechaInicioReal;
    private String fechaFinReal;
    private String fechaActual;
    private String porcentajeAvance;
    private String porcentajeEsperado;
    private String semaforo;
    private BanerCuenta informacionCliente;
    private Punta Puntas[];
    
    //-- Se utilizan solo en bandeja mesa de control
    private String Id_cotizacion;
    private String Folio_cotizacion;
    private String Nombre_Contacto;
    private String Telefono_contacto;
    private String Fecha_creacion;
    private String Tiempo_mesa;
    
    //-- Se utilizan solo en bandeja de facturados
    private String[] Cuentas_Factura;
    private String Numero_sitios;
    private String Num_total;


    public void setNum_total(String Num_total) {
        this.Num_total = Num_total;
    }

    public String getNum_total() {
        return Num_total;
    }

    public void setNum_otro(String Num_otro) {
        this.Num_otro = Num_otro;
    }

    public String getNum_otro() {
        return Num_otro;
    }

    public void setFechaInicioPlaneada(String fechaInicioPlaneada) {
        this.fechaInicioPlaneada = fechaInicioPlaneada;
    }

    public String getFechaInicioPlaneada() {
        return fechaInicioPlaneada;
    }

    public void setFechaFinPlaneada(String fechaFinPlaneada) {
        this.fechaFinPlaneada = fechaFinPlaneada;
    }

    public String getFechaFinPlaneada() {
        return fechaFinPlaneada;
    }

    public void setFechaInicioReal(String fechaInicioReal) {
        this.fechaInicioReal = fechaInicioReal;
    }

    public String getFechaInicioReal() {
        return fechaInicioReal;
    }

    public void setFechaFinReal(String fechaFinReal) {
        this.fechaFinReal = fechaFinReal;
    }

    public String getFechaFinReal() {
        return fechaFinReal;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setPorcentajeAvance(String porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public String getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeEsperado(String porcentajeEsperado) {
        this.porcentajeEsperado = porcentajeEsperado;
    }

    public String getPorcentajeEsperado() {
        return porcentajeEsperado;
    }

    public void setId_cuenta(String Id_cuenta) {
        this.Id_cuenta = Id_cuenta;
    }

    public String getId_cuenta() {
        return Id_cuenta;
    }

    public void setNombre_cliente(String Nombre_cliente) {
        this.Nombre_cliente = Constantes.UTILS.toInitCap(Nombre_cliente);
    }

    public String getNombre_cliente() {
        return Nombre_cliente;
    }

    public void setEs_Top_5000(String Es_Top_5000) {
        this.Es_Top_5000 = Es_Top_5000;
    }

    public String getEs_Top_5000() {
        return Es_Top_5000;
    }

    public void setSegmento(String Segmento) {
        this.Segmento = Constantes.UTILS.toInitCap(Segmento);
    }

    public String getSegmento() {
        return Segmento;
    }

    public void setSector(String Sector) {
        this.Sector = Constantes.UTILS.toInitCap(Sector);
    }

    public String getSector() {
        return Sector;
    }

    public void setNombre_contacto(String Nombre_contacto) {
        this.Nombre_contacto = Constantes.UTILS.toInitCap(Nombre_contacto);
    }

    public String getNombre_contacto() {
        return Nombre_contacto;
    }

    public void setTel_contacto(String Tel_contacto) {
        this.Tel_contacto = Tel_contacto;
    }

    public String getTel_contacto() {
        return Tel_contacto;
    }

    public void setFecha_inicio(String Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public String getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_fin(String Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }

    public String getFecha_fin() {
        return Fecha_fin;
    }

    public void setFecha_finPlaneada(String Fecha_finPlaneada) {
        this.Fecha_finPlaneada = Fecha_finPlaneada;
    }

    public String getFecha_finPlaneada() {
        return Fecha_finPlaneada;
    }

    public void setAvance(String Avance) {
        this.Avance = Avance;
    }

    public String getAvance() {
        return Avance;
    }

    public void setPuntas(Punta[] Puntas) {
        this.Puntas = Puntas;
    }

    public Punta[] getPuntas() {
        return Puntas;
    }

    public void setTotal_proyectos(String Total_proyectos) {
        this.Total_proyectos = Total_proyectos;
    }

    public String getTotal_proyectos() {
        return Total_proyectos;
    }

    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setSemaforo(String Semaforo) {
        this.semaforo = Semaforo;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setNum_confirmado(String Num_confirmado) {
        this.Num_confirmado = Num_confirmado;
    }

    public String getNum_confirmado() {
        return Num_confirmado;
    }

    public void setNum_proceso(String Num_proceso) {
        this.Num_proceso = Num_proceso;
    }

    public String getNum_proceso() {
        return Num_proceso;
    }

    public void setNum_restcate(String Num_restcate) {
        this.Num_restcate = Num_restcate;
    }

    public String getNum_restcate() {
        return Num_restcate;
    }

    public void setNum_Cancelado(String Num_Cancelado) {
        this.Num_Cancelado = Num_Cancelado;
    }

    public String getNum_Cancelado() {
        return Num_Cancelado;
    }

    public void setNum_detenida(String Num_detenida) {
        this.Num_detenida = Num_detenida;
    }

    public String getNum_detenida() {
        return Num_detenida;
    }

    public void setNum_gestoria(String Num_gestoria) {
        this.Num_gestoria = Num_gestoria;
    }

    public String getNum_gestoria() {
        return Num_gestoria;
    }

    public void setNum_suspendido(String Num_suspendido) {
        this.Num_suspendido = Num_suspendido;
    }

    public String getNum_suspendido() {
        return Num_suspendido;
    }

    public void setNum_completado(String Num_completado) {
        this.Num_completado = Num_completado;
    }

    public String getNum_completado() {
        return Num_completado;
    }

    public void setOS_pendientes(String OS_pendientes) {
        this.OS_pendientes = OS_pendientes;
    }

    public String getOS_pendientes() {
        return OS_pendientes;
    }

    public void setId_cotizacion(String Id_cotizacion) {
        this.Id_cotizacion = Id_cotizacion;
    }

    public String getId_cotizacion() {
        return Id_cotizacion;
    }

    public void setFolio_cotizacion(String Folio_cotizacion) {
        this.Folio_cotizacion = Folio_cotizacion;
    }

    public String getFolio_cotizacion() {
        return Folio_cotizacion;
    }

    public void setNombre_Contacto(String Nombre_Contacto) {
        this.Nombre_Contacto = Nombre_Contacto;
    }

    public String getNombre_Contacto() {
        return Nombre_Contacto;
    }

    public void setTelefono_contacto(String Telefono_contacto) {
        this.Telefono_contacto = Telefono_contacto;
    }

    public String getTelefono_contacto() {
        return Telefono_contacto;
    }

    public void setFecha_creacion(String Fecha_creacion) {
        this.Fecha_creacion = Fecha_creacion;
    }

    public String getFecha_creacion() {
        return Fecha_creacion;
    }

    public void setTiempo_mesa(String Tiempo_mesa) {
        this.Tiempo_mesa = Tiempo_mesa;
    }

    public String getTiempo_mesa() {
        return Tiempo_mesa;
    }

    public void setCuentas_Factura(String[] Cuentas_Factura) {
        this.Cuentas_Factura = Cuentas_Factura;
    }

    public String[] getCuentas_Factura() {
        return Cuentas_Factura;
    }

    public void setNumero_sitios(String Numero_sitios) {
        this.Numero_sitios = Numero_sitios;
    }

    public String getNumero_sitios() {
        return Numero_sitios;
    }

    public void setNum_calendarizado(String Num_calendarizado) {
        this.Num_calendarizado = Num_calendarizado;
    }

    public String getNum_calendarizado() {
        return Num_calendarizado;
    }

    public void setInformacionCliente(BanerCuenta informacionCliente) {
        this.informacionCliente = informacionCliente;
    }

    public BanerCuenta getInformacionCliente() {
        return informacionCliente;
    }

    public void setTotalPuntas(String totalPuntas) {
        this.totalPuntas = totalPuntas;
    }

    public String getTotalPuntas() {
        return totalPuntas;
    }

    public void setTotalPuntasPorInstalar(String totalPuntasPorInstalar) {
        this.totalPuntasPorInstalar = totalPuntasPorInstalar;
    }

    public String getTotalPuntasPorInstalar() {
        return totalPuntasPorInstalar;
    }

    public void setTotalPuntasCalendarizado(String totalPuntasCalendarizado) {
        this.totalPuntasCalendarizado = totalPuntasCalendarizado;
    }

    public String getTotalPuntasCalendarizado() {
        return totalPuntasCalendarizado;
    }

    public void setTotalPuntasDetenido(String totalPuntasDetenido) {
        this.totalPuntasDetenido = totalPuntasDetenido;
    }

    public String getTotalPuntasDetenido() {
        return totalPuntasDetenido;
    }

    public void setTotalPuntasDevueltoVentas(String totalPuntasDevueltoVentas) {
        this.totalPuntasDevueltoVentas = totalPuntasDevueltoVentas;
    }

    public String getTotalPuntasDevueltoVentas() {
        return totalPuntasDevueltoVentas;
    }

    public void setTotalPuntasCancelado(String totalPuntasCancelado) {
        this.totalPuntasCancelado = totalPuntasCancelado;
    }

    public String getTotalPuntasCancelado() {
        return totalPuntasCancelado;
    }

    public void setTotalPuntasInstalado(String totalPuntasInstalado) {
        this.totalPuntasInstalado = totalPuntasInstalado;
    }

    public String getTotalPuntasInstalado() {
        return totalPuntasInstalado;
    }

}
