package com.proyectmanager.hilos;

import com.proyectmanager.logica.CargaEstructuraBase;
import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.BanerServicio;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.CotSitioPlan;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HiloPlanData extends Thread{
    
    private QueryResult pm_proyectos;
    private String id_pm_sf;
    private Map<String,CotSitioPlan> map_csp;
    private Map<String,Actividad> map_actividades;
    
    public HiloPlanData(QueryResult pm_proyectos,Map<String,Actividad> map_actividades,String id_pm_sf) {
        this.pm_proyectos = pm_proyectos;
        this.map_actividades = map_actividades;
        this.id_pm_sf = id_pm_sf;
    }
    
    @Override
    public void run() {
        this.setMap_csp(this.loadDataPlan(pm_proyectos,map_actividades,id_pm_sf));
    }
    
    private Map<String,CotSitioPlan> loadDataPlan(QueryResult pm_proyectos,Map<String,Actividad> map_actividades,String id_pm_sf){
        
        Map<String,CotSitioPlan> map_csp = new HashMap<String,CotSitioPlan>();
        
        try{
            
            Map<String,Map<String,String>> map_confOts = this.getConfigOTs();
            
            for(SObject proyecto_sf : pm_proyectos.getRecords()){
                CotSitioPlan csp = new CotSitioPlan();
                BanerServicio infoServicio = new BanerServicio();
                csp.setId_csp((String) proyecto_sf.getField("Id"));
                csp.setFolio_CSP((String) proyecto_sf.getField("Name"));
                csp.setPaquete((String) proyecto_sf.getField("NombrePlan__c"));
                csp.setFecha_aprovisionamiento((String) proyecto_sf.getField("FechaAprovisionamiento__c"));
                csp.setTipo_cuadrilla(((String) proyecto_sf.getField("CuadrillaFFM__c")) == null ? "Empresarial" : (String) proyecto_sf.getField("CuadrillaFFM__c"));
                csp.setTipo_acceso((String) proyecto_sf.getField("AcessoPrincipal__c"));
                                
                infoServicio.setFolioCsp((String) proyecto_sf.getField("Name"));
                infoServicio.setFamiliaPlan((String) proyecto_sf.getField("Dp_Familia__c"));
                infoServicio.setNombrePlan((String) proyecto_sf.getField("NombrePlan__c"));
                infoServicio.setTipoOportunidad((String) proyecto_sf.getField("Tipo_Oportunidad__c"));
                infoServicio.setTipoRegistro((String) proyecto_sf.getField("Tipo_de_registro_CF__c"));
                infoServicio.setAcessoPrincipal((String) proyecto_sf.getField("AcessoPrincipal__c"));
                infoServicio.setFechaCerradaGanada((String) proyecto_sf.getField("TSganada__c"));
                infoServicio.setTipoCotizacionPm((String) proyecto_sf.getField("TipoCotizacionPM__c"));
                infoServicio.setCuadrillaFFM(((String) proyecto_sf.getField("CuadrillaFFM__c")) == null ? "Empresarial" : (String) proyecto_sf.getField("CuadrillaFFM__c"));
                infoServicio.setEstatusActivacion((String) proyecto_sf.getField("EstatusActivacion__c"));
                infoServicio.setFechaAprovisionamiento((String) proyecto_sf.getField("FechaAprovisionamiento__c"));
                
                
                if(((String) proyecto_sf.getField("Tipo_Plan__c")).contains("medida")){
                    csp.setTipoPlan("S");    
                }else{
                    csp.setTipoPlan("P");
                }
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf, "OrdenServicio__r")){
                    SObject ordenServicio = (SObject) proyecto_sf.getSObjectField("OrdenServicio__r"); 
                    csp.setId_OS((String) ordenServicio.getField("Id"));
                    csp.setFolio_OS((String) ordenServicio.getField("Name"));
                    csp.setStatus_OS((String) ordenServicio.getField("Estatus__c"));
                    infoServicio.setFolioOs((String) ordenServicio.getField("Name"));
                    infoServicio.setOt((String) ordenServicio.getField("IdOtGIM__c"));
                }else{
                    csp.setStatus_OS(Constantes.OS_STATUS_PENDIENTE);    
                }
                
                csp.setStatusCsp(this.calculaEstatusCsp(csp.getStatus_OS()));
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf,"Cot_Sitio__r")){
                    SObject cot_sitio = (SObject) proyecto_sf.getSObjectField("Cot_Sitio__r");
                    csp.setId_cot_sitio((String) cot_sitio.getField("Id"));
                    csp.setFolioCotSitio((String) cot_sitio.getField("Name"));
                    csp.setDireccionSitio((String) cot_sitio.getField("DireccionSitio__c"));
                    csp.setPlaza((String) cot_sitio.getField("Plaza__c"));
                    csp.setSubtotalRenta((String) cot_sitio.getField("SubtotalRenta__c"));
                    csp.setTotalRentaConImpuesto((String) cot_sitio.getField("TotalRenta_ConImpuesto__c")); 
                    csp.setTipoOportunidad((String) cot_sitio.getField("Tipo_Oportunidad__c"));
                            
                    if(Constantes.UTILS.validateDataSF(cot_sitio,"Cotizacion__r")){
                        SObject cot = (SObject) cot_sitio.getSObjectField("Cotizacion__r");
                        csp.setFolioCotizacion((String) cot.getField("Name"));
                    }
                    
                    if(Constantes.UTILS.validateDataSF(cot_sitio,"Sitio__r")){
                        SObject sitio = (SObject) cot_sitio.getSObjectField("Sitio__r");
                        csp.setNombreSitio((String) sitio.getField("IdSitio__c"));
                        csp.setFolioSitio((String) sitio.getField("Name"));
                        csp.setTipoCobertura((String) sitio.getField("TipoCobertura__c"));
                        csp.setLatitude((String) sitio.getField("Geolocalizacion__Latitude__s"));
                        csp.setLongitude((String) sitio.getField("Geolocalizacion__Longitude__s"));
                    }
                    
                    
                }
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf,"CuentaFactura__r")){
                    SObject cuenta_factura = (SObject) proyecto_sf.getSObjectField("CuentaFactura__r");
                    csp.setId_cuenta_factura((String) cuenta_factura.getField("Id"));
                    csp.setNumero_cuentaFactura((String) cuenta_factura.getField("IdCuentaBRM__c"));
                    csp.setCanal_venta(((String) cuenta_factura.getField("LeadSource__c")) == null ? "Sin espesificar" : (String) cuenta_factura.getField("LeadSource__c"));
                    csp.setTelefono_contacto((String) cuenta_factura.getField("TelefonoPrincipal__c"));
                    csp.setRegion((String) cuenta_factura.getField("RegionInstalacion__c"));
                    csp.setCiudad((String) cuenta_factura.getField("Plaza__c"));
                    csp.setDistrito((String) cuenta_factura.getField("DistritoInstalacion__c"));
                    csp.setCluster((String) cuenta_factura.getField("ClusterInstalacion__c"));
                    csp.setCalle((String) cuenta_factura.getField("CalleInstalacion__c"));
                    csp.setColonia((String) cuenta_factura.getField("ColoniaInstalacion__c"));
                    csp.setEstado((String) cuenta_factura.getField("EstadoInstalacion__c"));
                    csp.setMunicipio((String) cuenta_factura.getField("DelegacionInstalacion__c"));
                    csp.setNum_interior((String) cuenta_factura.getField("NumeroInteriorInstalacion__c"));
                    csp.setNum_exterior((String) cuenta_factura.getField("NumeroExterior__c"));
                    csp.setLatitud((String) cuenta_factura.getField("GeolocalizacionInstalacion__Latitude__s"));
                    csp.setLongitud((String) cuenta_factura.getField("GeolocalizacionInstalacion__Longitude__s"));
                    csp.setCP((String) cuenta_factura.getField("CodigoPostalInstalacion__c"));
                    infoServicio.setCuentaFactura((String) cuenta_factura.getField("IdCuentaBRM__c"));
                }
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf, "Cotizacion__r")){
                    SObject cotizacion_sf = (SObject) proyecto_sf.getSObjectField("Cotizacion__r"); 
                    
                    if(Constantes.UTILS.validateDataSF(cotizacion_sf, "Oportunidad__r")){
                        SObject oportunidad_sf = (SObject) cotizacion_sf.getSObjectField("Oportunidad__r"); 
                        
                        if(Constantes.UTILS.validateDataSF(oportunidad_sf, "Account")){
                            SObject cuenta_sf = (SObject) oportunidad_sf.getSObjectField("Account"); 
                            csp.setId_cuenta((String) cuenta_sf.getField("Id"));
                        }
                    }
                }
                    
                if(map_actividades.containsKey(proyecto_sf.getField("Id"))){
                    Actividad actividad = map_actividades.get(proyecto_sf.getField("Id"));
                    csp.setIdDetallePlaneacion(actividad.getIdDetallePlaneacion());
                    csp.setFechaInicioPlaneada(actividad.getFecha_inicio_planeada() != null ? actividad.getFecha_inicio_planeada() : Constantes.C_VALOR_VACIO);
                    csp.setFechaFinPlaneada(actividad.getFecha_fin_planeada() != null ? actividad.getFecha_fin_planeada() : Constantes.C_VALOR_VACIO);
                    csp.setFechaInicioReal(actividad.getFecha_inicio_real() != null ? actividad.getFecha_inicio_real() : Constantes.C_VALOR_VACIO);
                    csp.setFechaFinReal(actividad.getFecha_fin_real() != null ? actividad.getFecha_fin_real() : Constantes.C_VALOR_VACIO);
                    csp.setPorcentajeAvance(actividad.getPorcentaje() != null ? Constantes.UTILS.valorDosDecimales(actividad.getPorcentaje()) : Constantes.C_SIN_AVANCE);
                    csp.setFechaActual(actividad.getFechaActual() != null ? actividad.getFechaActual() : Constantes.C_VALOR_VACIO);
                    csp.setPorcentajeEsperado(actividad.getFecha_fin_real() != null ? Constantes.UTILS.getPorcentajeEsperado(actividad.getFecha_inicio_real(),actividad.getFecha_fin_real(),actividad.getFechaActual()) : Constantes.C_SIN_AVANCE);                    
                    csp.setSemaforo(Constantes.UTILS.getSemaforoEnPlaneacion(csp.getPorcentajeEsperado(), csp.getPorcentajeAvance(),actividad.getEstatusPlaneacion(),Constantes.UTILS.getEnTiempo(csp.getFechaFinPlaneada(), csp.getFechaFinReal())));
                    csp.setPlaneacionCerrada(actividad.getPlaneacionCerrada());
                    csp.setEstatusPlaneacion(actividad.getEstatusPlaneacion());
                    csp.setEnTiempo(actividad.getEnTiempo());
                    csp.setImplementacionterminada(actividad.getPlanTermindo().equals("4") || actividad.getPlanTermindo().equals("5") ? "true" : "false");
                }else{
                    csp.setFechaInicioPlaneada(Constantes.C_VALOR_VACIO);
                    csp.setFechaFinPlaneada(Constantes.C_VALOR_VACIO);
                    csp.setFechaInicioReal(Constantes.C_VALOR_VACIO);
                    csp.setFechaFinReal(Constantes.C_VALOR_VACIO);
                    csp.setPorcentajeAvance(Constantes.C_SIN_AVANCE);
                    csp.setFechaActual(Constantes.C_VALOR_VACIO);
                    csp.setPorcentajeEsperado(Constantes.C_SIN_AVANCE);
                    csp.setSemaforo(Constantes.C_VALOR_VACIO);
                    csp.setPlaneacionCerrada("false");
                    csp.setImplementacionterminada("false");
                    csp.setEstatusPlaneacion(Constantes.C_VALOR_VACIO);
                    csp.setEnTiempo(Constantes.C_VALOR_VACIO);
                    //evaluar si combiene la carga en este punto
                    //CargaEstructuraBase.cargaEstructuraBasica(csp.getId_cuenta(),(String) proyecto_sf.getField("Id"), id_pm_sf, Constantes.C_NO_TIENE_CSP);
                }
                
                Map<String,String> map_confOT = map_confOts.get(csp.getTipo_cuadrilla());
                
                if(!map_confOT.isEmpty()){
                    // --- datos de agendamiento
                    if(csp.getTipoPlan().equals("S")){
                        csp.setUnidad_negocio(Constantes.ID_UNIDAD_ENL);
                        csp.setTipo_intervencion(Constantes.C_TIPO_OT_UNIVERSAL);
                        csp.setSubtipo_intervencion(Constantes.C_SUBTIPO_OT_INSSOLUCION);        
                    }else{
                        csp.setUnidad_negocio(map_confOT.get(Constantes.KEY_UNIDAD_NEGOCIO));
                        csp.setTipo_intervencion(map_confOT.get(Constantes.KEY_INTERVENCION));
                        csp.setSubtipo_intervencion(map_confOT.get(Constantes.KEY_SUBINTERVENCION));        
                    }
                }
                
                csp.setInformacionServicio(infoServicio);
                
                if(csp.getId_cot_sitio()!= null){
                    map_csp.put((String) proyecto_sf.getField("Name"), csp);        
                }else{
                    System.err.println("No tienen cs : " + csp.getFolio_CSP());    
                }
            }
        }catch(Exception ex){
            System.err.println("Error al cargar csp : " + ex.toString());
        }
        
        return map_csp;    
    }
    
    private Map<String,Map<String,String>> getConfigOTs(){
        
        Map<String,Map<String,String>> conf_ots = new HashMap<String,Map<String,String>>();
        
        try{
            
            String config_ots [][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_CONSULTA_CONF_OTS);
            
            for(int fila = 0; fila < config_ots.length; fila++){
                Map<String,String> data_ot = new HashMap<String,String>();
                String conf [] = config_ots[fila][1].split(",");
                data_ot.put(Constantes.KEY_INTERVENCION, conf[0]);                        
                data_ot.put(Constantes.KEY_SUBINTERVENCION, conf[1]);
                data_ot.put(Constantes.KEY_UNIDAD_NEGOCIO, conf[2]);
                conf_ots.put(config_ots[fila][0], data_ot);
                            
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar conf OTs : " + ex.toString());
        }
        
        return conf_ots;
    }
    
    private String calculaEstatusCsp(String statusOS){
        String estatusCsp="";
        try{
            
            if(statusOS.equals(Constantes.C_OS_PENDIENTE) || statusOS.equals(Constantes.C_OS_PROCESO) || statusOS.equals(Constantes.C_OS_INICIO) || statusOS.equals(Constantes.C_OS_AGENDADO) || statusOS.equals(Constantes.C_OS_CONFIRMADO)){
                estatusCsp = Constantes.C_POR_INSTALAR;
            }else if(statusOS.equals(Constantes.C_OS_SUSPENDIDA) || statusOS.equals(Constantes.C_OS_CALENDARIZADO) || statusOS.equals("Reagendado")){
                estatusCsp = Constantes.C_CALENDARIZADO;    
            }else if(statusOS.equals(Constantes.C_OS_GESTORIA) || statusOS.equals(Constantes.C_OS_DETENIDA)){
                estatusCsp = Constantes.C_DETENIDO;    
            }else if(statusOS.equals(Constantes.C_OS_RESCATE)){
                estatusCsp = Constantes.C_DEVUELTO_VENTAS;    
            }else if(statusOS.equals(Constantes.C_OS_CANCELADO)){
                estatusCsp = Constantes.C_CANCELADO;
            }else if(statusOS.equals(Constantes.C_OS_COMPLETADO)){
                estatusCsp = Constantes.C_INSTALADO;    
            }else{
                System.out.println("Estatus no identificado : " + statusOS);    
            }
          
        }catch(Exception ex){
            System.err.println();
        }
        
        return estatusCsp;
    }
    
   
    public void setMap_csp(Map<String, CotSitioPlan> map_csp) {
        this.map_csp = map_csp;
    }

    public Map<String, CotSitioPlan> getMap_csp() {
        return map_csp;
    }
}
