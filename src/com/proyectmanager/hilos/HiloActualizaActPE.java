package com.proyectmanager.hilos;

import com.proyectmanager.logica.ActualizaActividades;
import com.proyectmanager.objects.Constantes;

public class HiloActualizaActPE extends Thread {
   
   private String idbridgesf;
   
    public HiloActualizaActPE(String idbridgesf){
        this.idbridgesf = idbridgesf;
    }
   
   @Override
   public void run() {
       this.actualizaActividadesPE(idbridgesf); 
   }
   
    public void actualizaActividadesPE(String idbridgesf){
        
        try{
            String actividades_pe[][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_DATA_ACT_PE.replaceAll("idbridgesf", idbridgesf));
            ActualizaActividades.actualizaActividades(actividades_pe, Constantes.C_RESPONSABLE_PLANTA_EXTERNA_DISTRITAL);
        
        }catch(Exception ex){
            System.err.println("Error actualizando act PE : " + ex.toString());
        }
           
    }
   
   
}
