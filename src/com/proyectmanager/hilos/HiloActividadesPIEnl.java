package com.proyectmanager.hilos;

import com.proyectmanager.logica.CargaActividadesNuevas;
import com.proyectmanager.logica.PMActividades;
import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.OrtdenTrabajo;
import com.proyectmanager.objects.Variables;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class HiloActividadesPIEnl extends Thread{
    
    private String idbridegsf;
    private String folio_os;
    private String idImplementacion;
    
    public HiloActividadesPIEnl(String folio_os,String idbridegsf,String idImplementacion) {
        this.idbridegsf = idbridegsf;
        this.folio_os = folio_os;
        this.idImplementacion = idImplementacion;
    }
    
    @Override
    public void run() {
        this.loadActividadesEnl(folio_os, idbridegsf, idImplementacion);    
    }  
    
    public void loadActividadesEnl(String folio_os,String idbridegsf,String idImplementacion){
        
        try{
            
            String nuevas_ots_enl [][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_OTS_EN_ENL.replaceAll("folios", folio_os).replaceAll("idbridgesf", idbridegsf)); 
            CargaActividadesNuevas.cargaActividadesNuevasPI(nuevas_ots_enl, Constantes.C_RESPONSABLE_PI_ENL,Constantes.C_ACTIVIDAD_IMPLEMENTACION, idImplementacion, Constantes.ID_UNIDAD_ENL,idbridegsf);
        
        }catch(Exception ex){
            System.out.println("Errpr al cargar actividades ENL : " + ex.toString());    
        }
        
    }
    
    
    /*public Boolean loadPIEnlActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_enl = false;
        
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPIEnl(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            PMActividades logica = new PMActividades();
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_ENL)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_ENL)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PI()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                       
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            carga_enl = logica.creaNuevaActividadPI(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }   
            
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PIEnl : " + ex.toString());
        }
        
        return carga_enl;
        
    }
    
    public Map<String,OrtdenTrabajo> loadMapOTsPIEnl(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesEnl [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_ENL.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesEnl != null){
                    
                    for(int fila = 0 ; fila < arr_actividadesEnl.length; fila++){
                        OrtdenTrabajo ot_en = new OrtdenTrabajo();
                        
                        ot_en.setId_ot(arr_actividadesEnl[fila][0]); 
                        ot_en.setFecha_agenda(arr_actividadesEnl[fila][1]); 
                        ot_en.setTurno(arr_actividadesEnl[fila][2]); 
                        ot_en.setOs(arr_actividadesEnl[fila][3]); 
                        ot_en.setTecnico(arr_actividadesEnl[fila][4]);                    
                        //ot_en.setAutomovil(arr_actividadesEnl[fila][5]); 
                        ot_en.setDesc_status(arr_actividadesEnl[fila][6]); 
                        ot_en.setId_estado(arr_actividadesEnl[fila][9]); 
                        ot_en.setDesc_estado(arr_actividadesEnl[fila][7]); 
                        ot_en.setDesc_motivo(arr_actividadesEnl[fila][8]); 
                        ot_en.setFecha_termino(arr_actividadesEnl[fila][10]);
                        ot_en.setDescripcion(arr_actividadesEnl[fila][11]);
                        ot_en.setNombre_responsable(arr_actividadesEnl[fila][12]);
                        ot_en.setApellidoPaterno_responsable(arr_actividadesEnl[fila][13]);
                        ot_en.setApellidoMaterno_responsable(arr_actividadesEnl[fila][14]);
                        ot_en.setTelefono_responsable(arr_actividadesEnl[fila][15]);
                        ot_en.setId_intervencion(arr_actividadesEnl[fila][16]);
                        ot_en.setId_subIntervencion(arr_actividadesEnl[fila][17]);
                        ot_en.setUnidad_negocio(Constantes.ID_UNIDAD_ENL);
                        ot_en.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesEnl[fila][9]));
                        
                        mapOTs.put(arr_actividadesEnl[fila][0], ot_en);
                    }
                    
                }else{
                    System.err.println("Error en la consulta de OTs Enl");        
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs ENL: " + ex.toString());    
        }
        
        return mapOTs;
    }*/
        
}
