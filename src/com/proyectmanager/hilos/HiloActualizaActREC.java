package com.proyectmanager.hilos;

import com.proyectmanager.logica.ActualizaActividades;
import com.proyectmanager.objects.Constantes;

public class HiloActualizaActREC extends Thread{
    
    private String idbridgesf;
    
     public HiloActualizaActREC(String idbridgesf){
         this.idbridgesf = idbridgesf;
     }
    
    @Override
    public void run() {
        this.actualizaActividadesREC(idbridgesf); 
    }
    
     public void actualizaActividadesREC(String idbridgesf){
         
         try{
             String actividades_pe[][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_DATA_ACT_REC.replaceAll("idbridgesf", idbridgesf));
             ActualizaActividades.actualizaActividades(actividades_pe, Constantes.C_RESPONSABLE_PI_REC);
         
         }catch(Exception ex){
             System.err.println("Error actualizando act PE : " + ex.toString());
         }
            
     }
}
