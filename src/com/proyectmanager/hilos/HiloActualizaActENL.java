package com.proyectmanager.hilos;

import com.proyectmanager.logica.ActualizaActividades;
import com.proyectmanager.objects.Constantes;

public class HiloActualizaActENL{
    
     public HiloActualizaActENL(){
         super();
     }
    
    
    public void actualizaActividades(String idbridgesf) {
        try{
            HiloActualizaActREC hiloREC = new HiloActualizaActREC(idbridgesf);
            HiloActualizaActPE hiloPE = new HiloActualizaActPE(idbridgesf);
            
           // System.out.println("Actualizando actividades ENL");
            this.actualizaActividadesENL(idbridgesf); 
            
           // System.out.println("Actualizando actividades REC");
            hiloREC.start();
            
          //  System.out.println("Actualizando actividades PE");
            hiloPE.start();
            
            hiloREC.join();
            hiloPE.join();
            
        }catch(Exception ex){
            System.err.println("Erro en hilo actualiza");
        }
    }
    
     public void actualizaActividadesENL(String idbridgesf){
         
         try{
             
             String actividades_pe[][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_DATA_ACT_ENL.replaceAll("idbridgesf", idbridgesf));
             ActualizaActividades.actualizaActividades(actividades_pe, Constantes.C_RESPONSABLE_PI_ENL);
         
         }catch(Exception ex){
             System.err.println("Error actualizando act PE : " + ex.toString());
         }
            
     }
}
