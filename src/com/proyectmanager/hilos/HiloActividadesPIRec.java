package com.proyectmanager.hilos;

import com.proyectmanager.logica.CargaActividadesNuevas;
import com.proyectmanager.logica.PMActividades;
import com.proyectmanager.objects.Actividad;

import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.OrtdenTrabajo;
import com.proyectmanager.objects.Variables;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class HiloActividadesPIRec{
    
    public HiloActividadesPIRec() {
        super();
    }
    
    public void cargaActividadesNuevas(String folio_os,String idbridegsf,String idImplementacion) {
        try {
            this.loadActividadesRec(folio_os, idbridegsf, idImplementacion);
            HiloActividadesPIEnl hiloENL = new HiloActividadesPIEnl(folio_os,idbridegsf,idImplementacion);
            HiloActividadesPE hiloPE = new HiloActividadesPE(folio_os,idbridegsf,idImplementacion);
            
           // System.out.println(" ===== Cargando nuevas actividades Empresarial");
            hiloENL.start();
           // System.out.println(" ===== Cargando nuevas actividades Planta externa");
            hiloPE.start();
            hiloPE.join();
        
            hiloENL.join();
        } catch (InterruptedException e) {
            System.err.println("Error en hilo actNue");
        }
        
    }
    
    public void loadActividadesRec(String folio_os,String idbridegsf,String idImplementacion){
        
        try{
            
            String nuevas_ots_enl [][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_OTS_EN_REC.replaceAll("folios", folio_os).replaceAll("idbridgesf", idbridegsf)); 
            CargaActividadesNuevas.cargaActividadesNuevasPI(nuevas_ots_enl, Constantes.C_RESPONSABLE_PI_REC,Constantes.C_ACTIVIDAD_IMPLEMENTACION, idImplementacion, Constantes.ID_UNIDAD_REC,idbridegsf);
        
        }catch(Exception ex){
            System.err.println("Error al cargar actividades REC : " + ex.toString());    
        }    
    }
    
    
    /*public Boolean loadPIRecActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_rec = false;
        PMActividades logica = new PMActividades();
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPIRec(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_REC)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_REC)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PI()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            logica.creaNuevaActividadPI(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }    
                
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PiRec : " + ex.toString());
        }
        
        return carga_rec;
    }
    
    public Map<String,OrtdenTrabajo> loadMapOTsPIRec(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesRec [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_REC.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesRec!= null){
                    
                    for(int fila = 0 ; fila < arr_actividadesRec.length; fila++){
                        OrtdenTrabajo ot_re = new OrtdenTrabajo();
                        ot_re.setId_ot(arr_actividadesRec[fila][0]); 
                        ot_re.setFecha_agenda(arr_actividadesRec[fila][1]); 
                        ot_re.setTurno(arr_actividadesRec[fila][2]); 
                        ot_re.setOs(arr_actividadesRec[fila][3]); 
                        ot_re.setTecnico(arr_actividadesRec[fila][4]); 
                        ot_re.setAutomovil(arr_actividadesRec[fila][5]); 
                        ot_re.setDesc_status(arr_actividadesRec[fila][6]); 
                        ot_re.setId_estado(arr_actividadesRec[fila][9]); 
                        ot_re.setDesc_estado(arr_actividadesRec[fila][7]); 
                        ot_re.setDesc_motivo(arr_actividadesRec[fila][8]); 
                        ot_re.setFecha_termino(arr_actividadesRec[fila][10]); 
                        ot_re.setUnidad_negocio(Constantes.ID_UNIDAD_REC);
                        ot_re.setDescripcion(arr_actividadesRec[fila][11]);
                        ot_re.setNombre_responsable(arr_actividadesRec[fila][12].equals("") ? "POR" : arr_actividadesRec[fila][12]);
                        ot_re.setApellidoPaterno_responsable(arr_actividadesRec[fila][13].equals("") ? "ASIGNAR" : arr_actividadesRec[fila][13]);
                        ot_re.setApellidoMaterno_responsable(arr_actividadesRec[fila][14].equals("") ? "RESPONSABLE" : arr_actividadesRec[fila][14]);
                        ot_re.setTelefono_responsable(arr_actividadesRec[fila][15].equals("") ? "PENDIENTE" : arr_actividadesRec[fila][15]);
                        ot_re.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesRec[fila][9]));
                        ot_re.setId_intervencion(arr_actividadesRec[fila][16]);
                        ot_re.setId_subIntervencion(arr_actividadesRec[fila][17]);
                        
                        mapOTs.put(arr_actividadesRec[fila][0], ot_re);
                    }
                    
                }else{
                    System.err.println("Error en la consulta de OTs Rec");    
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs REC: " + ex.toString());    
        }
        
        return mapOTs;
    }*/
}
