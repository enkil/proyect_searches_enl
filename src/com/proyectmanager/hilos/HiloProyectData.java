package com.proyectmanager.hilos;

import com.proyectmanager.objects.BanerCuenta;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Proyecto;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.HashMap;
import java.util.Map;

public class HiloProyectData extends Thread{
    
    private QueryResult pm_proyectos;
    private Map<String,Proyecto> map_proyect;
    
    public HiloProyectData(QueryResult pm_proyectos) {
        this.pm_proyectos = pm_proyectos;
    }
    
    @Override
    public void run() {
        this.setMap_proyect(this.loadDataProyect(pm_proyectos));
    }
    
    private Map<String,Proyecto> loadDataProyect(QueryResult pm_proyectos){
        
        Map<String,Proyecto> map_proyect = new HashMap<String,Proyecto>();
        
        try{
            
            for(SObject proyecto_sf : pm_proyectos.getRecords()){
                BanerCuenta informacionCliente = new BanerCuenta();
                if(Constantes.UTILS.validateDataSF(proyecto_sf, "Cotizacion__r")){
                    SObject cotizacion_sf = (SObject) proyecto_sf.getSObjectField("Cotizacion__r"); 
                    
                    if(Constantes.UTILS.validateDataSF(cotizacion_sf, "Oportunidad__r")){
                        SObject oportunidad_sf = (SObject) cotizacion_sf.getSObjectField("Oportunidad__r"); 
                        
                        if(Constantes.UTILS.validateDataSF(oportunidad_sf, "Account")){
                            SObject cuenta_sf = (SObject) oportunidad_sf.getSObjectField("Account"); 
                            
                            if(!map_proyect.containsKey((String) cuenta_sf.getField("Id"))){
                                Proyecto proyecto = new Proyecto();
                                                                
                                informacionCliente.setRazonSocial((String) cuenta_sf.getField("RazonSocial__c"));
                                informacionCliente.setRfc((String) cuenta_sf.getField("RFC__c"));
                                informacionCliente.setTop5000((String) cuenta_sf.getField("Top_5000__c"));
                                informacionCliente.setCiudad((String) cuenta_sf.getField("Ciudad__c"));
                                informacionCliente.setEstado((String) cuenta_sf.getField("Estado__c"));
                                informacionCliente.setDelegacionMunicipio((String) cuenta_sf.getField("DelegacionMunicipio__c"));
                                informacionCliente.setColonia((String) cuenta_sf.getField("Colonia__c"));
                                informacionCliente.setCalle((String) cuenta_sf.getField("Calle__c"));
                                informacionCliente.setNumeroExterior((String) cuenta_sf.getField("NumeroExterior__c"));
                                informacionCliente.setNumeroInterior((String) cuenta_sf.getField("NumeroInterior__c"));
                                informacionCliente.setCodigoPostal((String) cuenta_sf.getField("CodigoPostal__c"));
                                informacionCliente.setPhone((String) cuenta_sf.getField("Phone"));
                                informacionCliente.setMontoPotencial((String) cuenta_sf.getField("MontoPotencial__c"));
                                
                                proyecto.setId_cuenta((String) cuenta_sf.getField("Id"));
                                proyecto.setNombre_cliente((String) cuenta_sf.getField("RazonSocial__c"));
                                proyecto.setEs_Top_5000((String) cuenta_sf.getField("Top_5000__c"));
                                proyecto.setSegmento((String) cuenta_sf.getField("SegmentoFacturacion__c") == null ? "Sin segmento" : (String) cuenta_sf.getField("SegmentoFacturacion__c"));
                                proyecto.setSector((String) cuenta_sf.getField("Industry"));
                                proyecto.setTel_contacto((String) cuenta_sf.getField("Phone"));
                                
                                if(Constantes.UTILS.validateDataSF(cuenta_sf, "ContactoPrincipal__r")){
                                    SObject contacto_cuenta = (SObject) cuenta_sf.getSObjectField("ContactoPrincipal__r");
                                        proyecto.setNombre_contacto((String) contacto_cuenta.getField("NombreCompleto__c"));
                                        informacionCliente.setContactoPrincipalNombreCompleto((String) contacto_cuenta.getField("NombreCompleto__c"));
                                        informacionCliente.setContactoPrincipalPhone((String) contacto_cuenta.getField("Phone"));
                                        informacionCliente.setContactoPrincipalMobilePhone((String) contacto_cuenta.getField("MobilePhone"));
                                        informacionCliente.setContactoPrincipalEmail((String) contacto_cuenta.getField("Email"));
                                }
                                
                                proyecto.setInformacionCliente(informacionCliente);
                                
                                map_proyect.put((String) cuenta_sf.getField("Id"),proyecto);
                            }                
                        }
                    }
                }    
            }
        
        }catch(Exception ex){
            System.err.println("Error load proyect : " + ex.toString());
        }
        
        return map_proyect;
    }

    public void setMap_proyect(Map<String, Proyecto> map_proyect) {
        this.map_proyect = map_proyect;
    }

    public Map<String, Proyecto> getMap_proyect() {
        return map_proyect;
    }
}
