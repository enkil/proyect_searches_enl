package com.proyectmanager.hilos;

import com.proyectmanager.logica.CargaActividadesNuevas;
import com.proyectmanager.logica.PMActividades;
import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.OrtdenTrabajo;
import com.proyectmanager.objects.Variables;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class HiloActividadesPE extends Thread{
    private String idbridegsf;
    private String folio_os;
    private String idImplementacion;
    
    public HiloActividadesPE(String folio_os,String idbridegsf,String idImplementacion) {
        this.idbridegsf = idbridegsf;
        this.folio_os = folio_os;
        this.idImplementacion = idImplementacion;
    }
    
    @Override
    public void run() {
       this.loadPEActivities(folio_os,idbridegsf,idImplementacion);       
    }
    
    public void loadPEActivities(String folio_os,String idbridegsf,String idImplementacion){
        
        try{
            
            String nuevasActividades[][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_OTS_EN_PE.replaceAll("folios", folio_os).replaceAll("idbridgesf", idbridegsf));
            CargaActividadesNuevas.cargaActividadesNuevas(nuevasActividades, Constantes.C_RESPONSABLE_PLANTA_EXTERNA_DISTRITAL, Constantes.C_ACTIVIDAD_PLANTA_EXTERNA,idImplementacion);
            
        }catch(Exception ex){
            System.err.println("Error al cargar OTs PE");
        }
        
    }
    
    
    
    
    /*public Boolean loadPEActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_pe = false;
        PMActividades logica = new PMActividades();
        
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPE(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_PE)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_PE)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PE()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                       
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = logica.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            logica.creaNuevaActividadPE(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PE : " + ex.toString());
        }
        
        return carga_pe;
    }
    
    public Map<String,OrtdenTrabajo> loadMapOTsPE(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesPE [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_PE.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesPE.length > 0){
                    
                    for(int fila = 0 ; fila < arr_actividadesPE.length; fila++){
                        OrtdenTrabajo ot_pe = new OrtdenTrabajo();
                        ot_pe.setId_ot(arr_actividadesPE[fila][0]);
                        ot_pe.setOs(arr_actividadesPE[fila][1]);
                        ot_pe.setDesc_status(arr_actividadesPE[fila][2]);
                        ot_pe.setId_estado(arr_actividadesPE[fila][3]);
                        ot_pe.setDesc_estado(arr_actividadesPE[fila][4]);
                        ot_pe.setDesc_motivo(arr_actividadesPE[fila][5]);
                        ot_pe.setFecha_termino(arr_actividadesPE[fila][7]);
                        ot_pe.setNombre_responsable(arr_actividadesPE[fila][8].equals("") ? "POR" : arr_actividadesPE[fila][8]);
                        ot_pe.setApellidoPaterno_responsable(arr_actividadesPE[fila][9].equals("") ? "ASIGNAR" : arr_actividadesPE[fila][9]);
                        ot_pe.setApellidoMaterno_responsable(arr_actividadesPE[fila][10].equals("") ? "RESPONSABLE" : arr_actividadesPE[fila][10]);
                        ot_pe.setTelefono_responsable(arr_actividadesPE[fila][11].equals("") ? "PENDIENTE" : arr_actividadesPE[fila][11]);
                        ot_pe.setDetencion_pe(arr_actividadesPE[fila][6]);
                        ot_pe.setId_intervencion(arr_actividadesPE[fila][12]);
                        ot_pe.setId_subIntervencion(arr_actividadesPE[fila][13]);
                        ot_pe.setFecha_agenda(arr_actividadesPE[fila][14]);
                        ot_pe.setUnidad_negocio(Constantes.ID_UNIDAD_PE);
                        ot_pe.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesPE[fila][3]));
                        
                        mapOTs.put(arr_actividadesPE[fila][0], ot_pe);
                    }
                    
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs PE: " + ex.toString());    
        }
        
        return mapOTs;
    }*/    
}
