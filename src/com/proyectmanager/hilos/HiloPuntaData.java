package com.proyectmanager.hilos;

import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Punta;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HiloPuntaData extends Thread{
    
    private QueryResult pm_proyectos;
    private Map<String,Punta> mapaPuntas;
    
    public HiloPuntaData(QueryResult pm_proyectos) {
        this.pm_proyectos = pm_proyectos;
    }
    
    @Override
    public void run() {
        this.setMapaPuntas(this.loadDataPunta(pm_proyectos));
    }
    
    private Map<String,Punta> loadDataPunta(QueryResult pm_proyectos){
        
        Map<String,Punta> mapaPuntas = new HashMap<String,Punta>();
        
        try{ 
            Map<String,Punta> map_puntas = new HashMap<String,Punta>();
            
            for(SObject proyecto_sf : pm_proyectos.getRecords()){
                Punta punta = new Punta();
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf, "Cotizacion__r")){
                    SObject cotizacion_sf = (SObject) proyecto_sf.getSObjectField("Cotizacion__r"); 
                    punta.setId_cotizacion((String) cotizacion_sf.getField(("Id")));
                    punta.setFolio_cotizacion((String) cotizacion_sf.getField("Name"));
                    
                    if(Constantes.UTILS.validateDataSF(cotizacion_sf, "Oportunidad__r")){
                        SObject oportunidad_sf = (SObject) cotizacion_sf.getSObjectField("Oportunidad__r");
                        punta.setId_oportunidad((String) oportunidad_sf.getField(("Id")));
                        punta.setNumero_oportunidad((String) oportunidad_sf.getField(("NumeroOportunidad__c")));
                        punta.setFecha_cierre((String) oportunidad_sf.getField("CloseDate"));
                        punta.setPlazo((String) oportunidad_sf.getField("Plazo__c"));
                        
                        if(Constantes.UTILS.validateDataSF(oportunidad_sf, "Account")){
                            SObject cuenta_sf = (SObject) oportunidad_sf.getSObjectField("Account"); 
                            punta.setId_cuenta((String) cuenta_sf.getField("Id"));
                        }
                    }
                }
                
                if(Constantes.UTILS.validateDataSF(proyecto_sf,"Cot_Sitio__r")){
                    SObject cot_sitio = (SObject) proyecto_sf.getSObjectField("Cot_Sitio__r");
                    punta.setId_cot_sitio((String) cot_sitio.getField("Id"));
                    punta.setNombre_cot_sitio((String) cot_sitio.getField("Name"));                            
                    punta.setPlaza((String) cot_sitio.getField("Plaza__c"));
                    punta.setDireccion_sitio((String) cot_sitio.getField("DireccionSitio__c"));
                    
                    if(Constantes.UTILS.validateDataSF(cot_sitio,"Sitio__r")){
                        SObject sitio = (SObject) cot_sitio.getSObjectField("Sitio__r");
                        
                        punta.setId_sitio((String) sitio.getField("Id"));
                        punta.setFolio_sitio((String) sitio.getField("Name"));
                        punta.setNombre_sitio((String) sitio.getField("IdSitio__c"));
                       // punta.setLongitud((String) sitio.getField("Geolocalizacion__Latitude__s"));
                       // punta.setLatitud((String) sitio.getField("Geolocalizacion__Longitude__s"));
                    }
                    if(Constantes.UTILS.validateDataSF(cot_sitio,"Owner")){
                        SObject propietario = (SObject) cot_sitio.getSObjectField("Owner");
                        
                        punta.setNombre_responsable_sitio((String) propietario.getField("Name"));
                    }
                }
                
                map_puntas.put((String) proyecto_sf.getField("Id"), punta);
                
            }
            
            Iterator it_mapPuntas = map_puntas.entrySet().iterator();
            while (it_mapPuntas.hasNext()) {
                Map.Entry entryProyect = (Map.Entry)it_mapPuntas.next();
                Punta punta = (Punta) entryProyect.getValue();
                
                if(!mapaPuntas.containsKey(punta.getId_cot_sitio())){
                    if(punta.getId_cot_sitio()!=null){
                        mapaPuntas.put(punta.getId_cot_sitio(), punta);        
                    }
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar punta : " + ex.toString());
        }
        
        return mapaPuntas;
    }


    public void setMapaPuntas(Map<String, Punta> mapaPuntas) {
        this.mapaPuntas = mapaPuntas;
    }

    public Map<String, Punta> getMapaPuntas() {
        return mapaPuntas;
    }
}
