package com.proyectmanager.utilities;

import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.Variables;

import com.sforce.soap.partner.LoginResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.SearchRecord;
import com.sforce.soap.partner.SearchResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class CrudSF {
    public CrudSF() {
        super();
    }
        
        public PartnerConnection config_conector_sf(){
            
            PartnerConnection conexion_sf;
            
            try{
                
                ConnectorConfig config = new ConnectorConfig();
                config.setUsername(Variables.getSF_USER());
                config.setPassword(Variables.getSF_PASSWORD());
                config.setAuthEndpoint(Variables.getSF_END_POINT());
                
                System.err.println("END POINT SF 1 : " + Variables.getSF_END_POINT());
                
                conexion_sf = new PartnerConnection(config);
                
            }catch(Exception ex){
                System.out.println("Error al generar Conexion SF : " + ex.toString());
                conexion_sf = null;
            }
            
            return conexion_sf;
            
        }
        
        
        public PartnerConnection conexion_conector_sf(String id_sesion_sf){
            PartnerConnection conexion_sf;
            
            try{
                
                ConnectorConfig config = new ConnectorConfig();
                config.setUsername(Variables.getSF_USER());
                config.setPassword(Variables.getSF_PASSWORD());
                config.setServiceEndpoint(Variables.getSF_END_POINT());
                config.setSessionId(id_sesion_sf);
                //System.err.println("END POINT SF 2 : " + Variables.getSF_END_POINT()); 
                conexion_sf = new PartnerConnection(config);
                
            }catch(Exception ex){
                System.out.println("Error al generar Conexion SF : " + ex.toString());
                conexion_sf = null;
            }
            
            return conexion_sf;
        }


        /**
         * @param queryString
         * @return QueryResult
         */
        public QueryResult query_sf(String queryString) {
            
            QueryResult response_qr_sf = new QueryResult();
            
            try {
                System.err.println(queryString);
                PartnerConnection conexion_sf = this.conexion_conector_sf(this.getSessionIDSF()) ;
                response_qr_sf = conexion_sf.query(queryString);
                
            } catch (Exception e) {                 
                System.out.println("Error en consulta : " + e.toString());
                response_qr_sf.setQueryLocator("Error en consulta : " + e.toString());
            }catch(Throwable tx){
                System.out.println("Error TX consulta sf : " + tx.toString());
                response_qr_sf.setQueryLocator("Error en consulta : " + tx.toString());
            }
            return response_qr_sf;
        }


        /**
         * @return String
         */
        public SearchRecord[] search_sf(String query_search){
            
            SearchRecord[] search_result = new SearchRecord[1];
            
            //System.err.println("Consulta : " + query_search);
            
            try{
                
                PartnerConnection conexion_sf = this.conexion_conector_sf(this.getSessionIDSF()) ;
                SearchResult sResult = conexion_sf.search(query_search);
                  
                SearchRecord[] records = sResult.getSearchRecords();
                
                search_result = records;
             
            }catch(ConnectionException ex){
                search_result[0] = null;
            }catch (Exception e) {
                search_result[0] = null;
            }
            
            return search_result;
        }

        public String getSessionIDSF() {
                        
            String sessionId = "";
            
            try {
                
                String session_id_bd = Constantes.CRUD_FFM.getSessionId_FFM();
                
                if(session_id_bd.equals("") || session_id_bd.equals("-1") || session_id_bd == null ){
                    PartnerConnection conexion_sf  = this.config_conector_sf();
                    LoginResult login = conexion_sf.login(Variables.getSF_USER(), Variables.getSF_PASSWORD());
                    sessionId = login.getSessionId();
                    
                    Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_SI_SF.replaceAll("sid_sf", sessionId));
                }else{
                    sessionId = session_id_bd;
                }
                
                //-- valida que el session id recuperado de bd esta activo
                if(!this.validaSessionID(sessionId)){
                    PartnerConnection conexion_sf  = this.config_conector_sf();
                    LoginResult login = conexion_sf.login(Variables.getSF_USER(), Variables.getSF_PASSWORD());
                    sessionId = login.getSessionId();
                    
                    Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_SI_SF.replaceAll("sid_sf", sessionId));
                }
                
                
            }catch (Exception e) {
            System.out.println("Errro al generar sesion id " + e.toString());
            }
            
            return sessionId;
        }


        /**
         * Metodo para crear elementos en salesforce
         * @param Sobjet[] arreglo de SObjects
         * @return SaveResult[] areglo con el detalle de la operacion en salesforce
         */
        public SaveResult[] create_sf(SObject[] objetos_crear){
            SaveResult[] respuesta_creacion = new SaveResult[1];
            
            try{
                
                PartnerConnection conexion_sf = this.conexion_conector_sf(this.getSessionIDSF());
                
                respuesta_creacion = conexion_sf.create(objetos_crear);
                
            }catch(Exception ex){
                System.out.println("Error al crear registros en sf : " + ex);
            }
            
            return respuesta_creacion;
            
        }
        
        
        public SaveResult[] update_sf(SObject[] objetos_actualizar){
            SaveResult[] respuesta_actualizacion = new SaveResult[1];
            
            try{
                
                PartnerConnection conexion_sf = this.conexion_conector_sf(this.getSessionIDSF());
                
                respuesta_actualizacion = conexion_sf.update(objetos_actualizar);
                           
            }catch(Exception ex){
                System.out.println("Error al actualizar datos sf : " + ex.toString());
            }
            
            return respuesta_actualizacion;
                
        }
        
        
        public String callWS_SF(String postUrl, String input_info_sf){
            
            String responseJSONString = "";
            String id_sesionsf = this.getSessionIDSF();
            
            try {
                    HttpsURLConnection sf_conection = null;
                    //HttpsURLConnection.setDefaultHostnameVerifier(new Utilerias().disableSSL());

                    URL urlGeocoding = new URL(null, postUrl, new sun.net.www.protocol.https.Handler());
                    sf_conection = (HttpsURLConnection) urlGeocoding.openConnection();
                    sf_conection.addRequestProperty("Accept","application/json");
                    sf_conection.addRequestProperty("Content-Type", "application/json");
                    sf_conection.addRequestProperty("sessionId",  id_sesionsf  );
                    sf_conection.addRequestProperty("Authorization", "OAuth "+ id_sesionsf );
                    sf_conection.setUseCaches(false);
                    sf_conection.setDoInput(true);
                    sf_conection.setDoOutput(true);
                    sf_conection.setRequestMethod("POST");
                
                    OutputStream os = sf_conection.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");    
                    osw.write(input_info_sf);
                    osw.flush();
                    osw.close();
                    os.close();
                    sf_conection.connect();
                    
                    
                    BufferedInputStream bis = new BufferedInputStream(sf_conection.getInputStream());
                    ByteArrayOutputStream buf = new ByteArrayOutputStream();
                    int result2 = bis.read();
                    while(result2 != -1) {
                        buf.write((byte) result2);
                        result2 = bis.read();
                    }
                    
                    responseJSONString = buf.toString();                                                
                
                } catch (Exception e) {
                   return "";
                }
            
            return responseJSONString;

        }
        
        public Boolean validaSessionID(String sessionID){
            Boolean bandera = false;
            
            try {
                
                QueryResult response_qr_sf = new QueryResult();
                PartnerConnection conexion_sf = this.conexion_conector_sf(sessionID) ;
                response_qr_sf = conexion_sf.query(Constantes.GENERIC_CONSULT);
                    
                bandera =  true;    
                
            } catch (Exception e) {
                bandera = false;
            }catch(Throwable er){
                bandera = false;
            }
            
            System.out.println("Valor de la bandera : " + bandera);
             
            return bandera;
        }
        
}
