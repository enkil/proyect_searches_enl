package com.proyectmanager.utilities;

import com.google.gson.Gson;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Variables;

import com.sforce.soap.partner.sobject.SObject;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utilerias {
    public Utilerias() {
        super();
    }
    
    public Object strJsonToClass(String jsonInput, Class<?> claseConversion){
        
        try{
        
            Gson gson_object = new Gson();
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(jsonInput);
            
            return gson_object.fromJson(element, claseConversion);
        
        }catch(Exception ex){
            System.err.println("error : " + ex.toString());
            return null;
        }
                
    }
    
    public String classTostrJson(Object classToJson){
        try{
            Gson gson = new Gson();
            
            return gson.toJson(classToJson);
        }catch(Exception ex){
            System.out.println("Error json to class : " + ex);
            return "{}";
        }    
    }
    
    public Properties getConfiguracion(){
        Properties prop = null;
        try{
            prop = new Properties();
            prop.load(this.getClass().getResourceAsStream(Constantes.PATH_PROPERTIES));
            
            return prop;
        }catch(Exception ex){
            System.err.println("Error al obtener propiedades = " + ex);
            return prop;
        }
    }
    
    public Properties getConfiguracionBD(){
        
        Properties prop = new Properties();
        
        try{
            
             Map<String,String> configuraciones = Constantes.CRUD_FFM.configuracionGeneral();
              
              if(!configuraciones.isEmpty()){
                  for ( Map.Entry<String, String> entry : configuraciones.entrySet()) {
                      String key = entry.getKey();
                      String value = entry.getValue();
                      prop.put(key, value);  
                  }
              }
            
        }catch(Exception ex){
            System.out.println("Error propiedades BD " + ex.toString());
        }
        
        return prop;
    }
    
    
    public void loadConfigIN_DB(){
        
        try{
            
            Properties prop_in_db = this.getConfiguracionBD();
            Variables.setSF_USER(prop_in_db.getProperty(Constantes.KEY_USER_SF)/*"middlewaretp@totalplay.com.mx"*/);
            Variables.setSF_PASSWORD(prop_in_db.getProperty(Constantes.KEY_PASSWORD_SF)/*"MFRC2305iILQgNdUE6wX10KaEgQxfW1xu"*/);
            Variables.setSF_END_POINT(prop_in_db.getProperty(Constantes.KEY_END_POINT_SF)/*"https://na130.salesforce.com/services/Soap/u/44.0/"*/);
            Variables.setSEGUNDOS_DOS_DIAS(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_DOS_DIAS));
            Variables.setSEGUNDOS_MEDIODIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_MEDIODIA));
            Variables.setSEGUNDOS_UNDIA(prop_in_db.getProperty(Constantes.KEY_SEGUNDOS_UNDIA));
            Variables.setQR_BANDEJA_MESA_CONTROL(prop_in_db.getProperty(Constantes.KEY_QR_PROYECTOS_MESA));
            Variables.setQR_BANDEJA_FACTURANDO(prop_in_db.getProperty(Constantes.KEY_QR_BANDEJA_FACTURANDO)); 
            Variables.setQR_BANDEJA_CANCELADO(prop_in_db.getProperty(Constantes.KEY_QR_BANDEJA_CANCELADO)); 
            Variables.setCONDICION_CONSULTAPROYECTOS(prop_in_db.getProperty(Constantes.KEY_CONDICION_PLAN_PROYECTO));
            Variables.setCOLOR_FUERA_TIEMPO(prop_in_db.getProperty(Constantes.KEY_COLOR_FUERA_TIEMPO));
            Variables.setCOLOR_RIESGO(prop_in_db.getProperty(Constantes.KEY_COLOR_RIESGO));
            Variables.setCOLOR_ENTIEMPO(prop_in_db.getProperty(Constantes.KEY_COLOR_ENTIEMPO));
            Variables.setPORCENTAJE_OK(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_OK));
            Variables.setPORCENTAJE_RIESGO(prop_in_db.getProperty(Constantes.KEY_PORCENTAJE_RIESGO));
            Variables.setKPI_PE(prop_in_db.getProperty(Constantes.KEY_KPI_PE_DIAS));
            Variables.setKPI_PI(prop_in_db.getProperty(Constantes.KEY_KPI_PI_DIAS));
            Variables.setURL_DASHBOARD(prop_in_db.getProperty(Constantes.KEY_URL_DASHBOARD));
            Variables.setSEARCH_GENERAL_ENL_SF(prop_in_db.getProperty(Constantes.KEY_FIND_GENERAL_ENL));
                     
        }catch(Exception ex){
            System.err.println("Error al cargar configuracion en DB : " + ex.toString());
        }    
    }    
    
    public Calendar getDateSF(String fechaAgenda_sf){
        
        try{

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(fechaAgenda_sf);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);            
            return cal; 
            
        }catch(Exception ex){
            System.out.println("error fecha sf : " + ex.toString());
            return null;
        }
        
    }
    
    public String tiempoTranscurrido(String fecha_creacion){
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            int dias=0;
            int horas=0;
            int minutos=0;
            
            if(diferencia>86400) {
                dias=(int)Math.floor(diferencia/86400);
                diferencia=diferencia-(dias*86400);
            }
            if(diferencia>3600) {
                horas=(int)Math.floor(diferencia/3600);
                diferencia=diferencia-(horas*3600);
            }
            if(diferencia>60) {
                minutos=(int)Math.floor(diferencia/60);
                diferencia=diferencia-(minutos*60);
            }
            
            return dias+" dias, "+horas+" horas, "+minutos+" minutos";
           
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public static Map<String,String> rangoUnDia(String fechaEntrada){
        
        Map<String,String> mapFechaSeleccionada = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            mapFechaSeleccionada.put("fechaIn", sdf.format(c.getTime()) + "T00:00:00.000+0000");
            mapFechaSeleccionada.put("fechaFin", sdf.format(c.getTime()) + "T23:59:59.000+0000");
            
               
        }catch(Exception ex){
            System.err.println("Error en rango semana : " + ex.toString());
        }
        
        return mapFechaSeleccionada;
    }
    
    
    public static Map<String,String> rangoSemana(String fechaEntrada){
        
        Map<String,String> mapFechasSenama = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            mapFechasSenama.put("fechaIn", sdf.format(c.getTime()) + "T00:00:00.000+0000");
            c.add(Calendar.DAY_OF_WEEK, 6);
            mapFechasSenama.put("fechaFin", sdf.format(c.getTime()) + "T23:59:59.000+0000");
            c.add(Calendar.DAY_OF_WEEK, 1); 
               
        }catch(Exception ex){
            System.err.println("Error en rango semana : " + ex.toString());
        }
        
        return mapFechasSenama;
    }
    
    public static  Map<String,String> rangoMes(String fechaEntrada){
        
        Map<String,String> mapPrimerDia = new HashMap<String,String>();
        
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fechaEntrada);
            Calendar fecha = Calendar.getInstance();
            fecha.setTime(date);
            
           
            fecha.set(Calendar.DAY_OF_MONTH,1);
            mapPrimerDia.put("fechaIn", sdf.format(fecha.getTime()) + "T00:00:00.000+0000");
            
            fecha.set(Calendar.MONTH, fecha.get(Calendar.MONTH)+1);
            fecha.set(Calendar.DAY_OF_MONTH,-1);
            mapPrimerDia.put("fechaFin", sdf.format(fecha.getTime()) + "T23:59:59.000+0000");

        }catch(Exception ex){
            System.err.println("Error en primer dia mes : " + ex.toString());
        }
            
        return mapPrimerDia;
    }
    
    public Map<String,String> fechasFiltroSf(String tipoFiltro,String fechaEntrada){
        Map<String,String> mapFechas = new HashMap<String,String>();
        
        try{
            
            if(Constantes.LABEL_SEMANA.equals(tipoFiltro)){
                mapFechas = rangoSemana(fechaEntrada);
            }else if(Constantes.LABEL_MES.equals(tipoFiltro)){
                mapFechas = rangoMes(fechaEntrada);    
            }else if(Constantes.LABEL_DIA.equals(tipoFiltro)){
                mapFechas = rangoUnDia(fechaEntrada);
            }
        
        }catch(Exception ex){
            System.err.println("Error al obtener filtros : " + ex.toString());  
        }
        
        return mapFechas;
    }
    
    public Boolean validateDataSF(SObject contenedor, String campoValidar){
        
        try{
            if(contenedor.getSObjectField(campoValidar) != null && contenedor.getSObjectField(campoValidar).getClass().equals(SObject.class)){
                 SObject objeto_sf = (SObject) contenedor.getSObjectField(campoValidar); 
                 if (objeto_sf != null) {
                     return true;
                 }else{
                    return false;    
                 }
            }else{
                return false;    
            }
        }catch(Exception ex){
            return false;    
        }
    }
    
    public String getSemaforoEnPlaneacion(String porcentaje_esperado,String porcentaje_real,String estatusPlaneacion, String enTiempo){
        
        String semaforo = "";
        
        try{
            
            Integer calculo;
            
            if(enTiempo.equals("true")){
                calculo =  1;
            }else{
                if(estatusPlaneacion.equals("0") || estatusPlaneacion.equals("")){
                    calculo =  2;    
                }else{
                    calculo =  3;
                }
            }
            
            //Plan en tiempo
            if(calculo.equals(1)){
                if(Float.valueOf(porcentaje_real) - Float.valueOf(porcentaje_esperado) >= (0 + Float.valueOf(Variables.getPORCENTAJE_OK()))){
                    semaforo = Variables.getCOLOR_ENTIEMPO();
                }else if(((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) < 0) && ((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) * -1) <= Float.valueOf(Variables.getPORCENTAJE_RIESGO())){
                    semaforo = Variables.getCOLOR_RIESGO();   
                }else{
                    semaforo = Variables.getCOLOR_FUERA_TIEMPO();
                } 
            //Plan fuera de tiempo pero con planeacion viva     
            }else if(calculo.equals(2)){
                if(((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) < 0) && ((Float.valueOf(porcentaje_real)- Float.valueOf(porcentaje_esperado)) * -1) <= Float.valueOf(Variables.getPORCENTAJE_RIESGO())){
                    semaforo = Variables.getCOLOR_RIESGO();   
                }else{
                    semaforo = Variables.getCOLOR_FUERA_TIEMPO();
                } 
            //-- Fuera de tiempo planeacion cerrada    
            }else{
                semaforo = Variables.getCOLOR_FUERA_TIEMPO();    
            }          
              
            return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error semaforo : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String getSemaforoMesa(String fecha_creacion){
        
        String semaforo = "";
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            
            
            if(diferencia <= Integer.valueOf(Variables.getSEGUNDOS_UNDIA())) {
                semaforo = Variables.getCOLOR_ENTIEMPO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_UNDIA()) && diferencia <= Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_RIESGO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_FUERA_TIEMPO();
            }
            
            return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public String getSemaforoXPlanear(String fecha_creacion){
        
        String semaforo = "";
        
        try{
            
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
     
            Date fechaFinal=new Date();
            Date fechaInicial=format.parse(fecha_creacion);
    
            int diferencia=(int) ((fechaFinal.getTime()-fechaInicial.getTime())/1000);
            
            
            if(diferencia <= Integer.valueOf(Variables.getSEGUNDOS_UNDIA())) {
                semaforo = Variables.getCOLOR_ENTIEMPO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_UNDIA()) && diferencia <= Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_RIESGO();
            }
            if(diferencia > Integer.valueOf(Variables.getSEGUNDOS_MEDIODIA())) {
                semaforo = Variables.getCOLOR_FUERA_TIEMPO();
            }
            
            return semaforo;
            
        }catch(Exception ex){
            System.out.println("Error : " + ex.toString());
            return "No se pudo estimar el tiempo de demora";
        }
    }
    
    public Date StringToDate(String fecha){
          Date date = null;
        try{
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            date = formatter.parse(fecha);
            System.out.println("Date object value: "+date);
            
        }catch(Exception ex){
            System.err.println("Error strToDate : " + ex.toString());
        }
         return date;
      }
    
    public Date StringToDateMM(String fecha){
          Date date = null;
        try{
            if(fecha!=null && !fecha.equals("")){
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                date = formatter.parse(fecha);    
            }
        }catch(Exception ex){
            System.err.println("Error strToDate : " + ex.toString());
        }
         return date;
      }
    
    
    public String dateToString(Date fecha){
        
        String todayAsString = "";
        
        try{
            
            if(fecha!=null && !fecha.equals("")){
                String pattern = "MM-dd-yyyy";

                DateFormat df = new SimpleDateFormat(pattern);
                
                todayAsString = df.format(fecha);   
            }
        
        }catch(Exception ex){
            System.err.println(ex.toString());    
        }
        
        return todayAsString;
    }
    
    public String toInitCap(String param) {
        String cadena_return = "";
        String cadena_base = "";
        if (param != null && param.length() > 0) {
            String[] palabras = param.split(" ");
            Boolean contiene_numero = false;
            
            for(String palabra : palabras){
                if(palabra.contains("@") || palabra.contains("-") || palabra.contains("$") || palabra.contains("CSP") || palabra.contains("COT") || palabra.contains("SIT") || palabra.contains("CS")){
                    cadena_base = palabra + " ";        
                }else{
                    char[] charArray = palabra.toLowerCase().toCharArray();
                    if(charArray.length > 0){
                        charArray[0] = Character.toUpperCase(charArray[0]);
                        for (int i = 0; i < palabra.length()- 2; i++){
                            if (charArray[i] == ' ' || charArray[i] == '.' || charArray[i] == ','){
                                charArray[i + 1] = Character.toUpperCase(charArray[i + 1]);    
                            }
                        }
                        
                        if(contiene_numero){
                            cadena_base = palabra + " ";
                        }else{
                            cadena_base = new String(charArray) + " ";
                        } 
                    }else{
                        cadena_base=""; 
                    }
                    
                }
                
                cadena_return = cadena_return + cadena_base;
                
            }   
            
            return cadena_return.trim();
        } else {
            return "";
        }
    }
    
    public String calculaAvance(String id_estado){
        String porcentaje = "0";
        try{
            
            if(Constantes.PENDIENTE.contains(id_estado)){
                porcentaje = Constantes.DIEZ_PORCIENTO;
            }else if(Constantes.ASIGNADA.contains(id_estado)){
                porcentaje = Constantes.VEI_PORCIENTO;
            }else if(Constantes.TRANCITO.contains(id_estado)){
                porcentaje = Constantes.CUA_PORCIENTO;
            }else if(Constantes.ST_SITIO.contains(id_estado)){
                porcentaje = Constantes.SE_PORCIENTO;
            }else if(Constantes.TRABAJO.contains(id_estado)){
                porcentaje = Constantes.SET_PORCIENTO;
            }else if(Constantes.TERMINADA.contains(id_estado)){
                porcentaje = Constantes.CIEN_PORCIENTO;
            }else if(Constantes.CANCELADA.contains(id_estado)){
                porcentaje = Constantes.CERO_PORCIENTO;
            }else if(Constantes.PAUSA.contains(id_estado)){
                porcentaje = Constantes.CIN_PORCIENTO;
            }else{
                porcentaje = Constantes.SET_PORCIENTO;
            }
            
         }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
        }
        
        return porcentaje;
    }
    
    public String jsonSanitizeTwo(String jsonString) {
        String wellFormedJson = com.google.json.JsonSanitizer.sanitize(jsonString);
        return wellFormedJson;
    }
    
    public String minDate(ArrayList<Date> listDate){
        
        String fecha = "";
        
        try{
            listDate.removeAll(Collections.singleton(null));
            
            Collections.sort(listDate);
            
            if(listDate.size() > 0){
                fecha = Constantes.UTILS.dateToString(listDate.get(0));
            }
            
            
        }catch(Exception ex){
            System.err.println("Error al calcular fechas minMax : " + ex.toString());    
        }
        
        return fecha;
    }
    
    public String maxDate(ArrayList<Date> listDate){
        
        String fecha = "";
        
        try{
            listDate.removeAll(Collections.singleton(null));
            Collections.sort(listDate);
            
            if(listDate.size() > 0){
                fecha = Constantes.UTILS.dateToString(listDate.get(listDate.size() - 1));
            }
            
        }catch(Exception ex){
            System.err.println("Error al calcular fechas minMax : " + ex.toString());    
        }
        
        return fecha;
    }
    
    public String getEnTiempo(String fechaFinPlan, String fechaFinReal){
        
        String enTiempo = ""; 
        
        try{
            
            if(fechaFinPlan!=null && fechaFinReal!=null && !fechaFinPlan.equals("") && !fechaFinReal.equals("")){
                Float diferenciaEn_ms = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFinReal).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFinPlan).getTime())) / (1000 * 60 * 60 * 24);
                
                if(diferenciaEn_ms<0){
                    enTiempo = "true";
                }else{
                    enTiempo = "false";
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error en get tiempo : " + ex.toString());
        }    
        
        return enTiempo;
    }
    
    public String getPorcentajeEsperado(String fechaIn, String fechaFin, String fechaActual){
        String porcentaje_esperado = "0.0";
        DecimalFormat df = new DecimalFormat("#.00");
        
        //System.out.println(fechaIn +" - " + fechaFin + " - " + fechaActual);
        
        try{
            
            if(fechaIn!=null && fechaFin!=null && !fechaIn.equals("") && !fechaFin.equals("")){
                Float diferenciaEn_ms = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaFin).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaIn).getTime())) / (1000 * 60 * 60 * 24);
                Float diferencia_dos = (Float.valueOf(Constantes.UTILS.StringToDateMM(fechaActual).getTime()) - Float.valueOf(Constantes.UTILS.StringToDateMM(fechaIn).getTime()))  / (1000 * 60 * 60 * 24);
                Float esperado = 0.0f;
                
                if(diferenciaEn_ms==0){
                    esperado = (100) * diferencia_dos;
                }else{
                    esperado = (100/diferenciaEn_ms) * diferencia_dos;
                }
                
                //System.out.println(diferenciaEn_ms + "- "+ diferencia_dos + " - " + esperado);
                
                if(esperado != null){
                    porcentaje_esperado = (Float.valueOf(df.format(esperado)) >= 100 ? "100" : Float.valueOf(df.format(esperado)).toString());    
                }                
            }
            
        }catch(Exception ex){
            System.err.println("Error calcula fecha : " + ex.toString());
        }     
        
        return porcentaje_esperado;
    }
    
    public String valorDosDecimales(String valor){
        
        DecimalFormat df = new DecimalFormat("#.00");
        String valorFormateado = valor;
        try{
            
            valorFormateado = df.format(Float.valueOf(valor));
        
        }catch(Exception ex){
            System.err.println("Error al calcular valor dos decimales " + ex.toString());
        } 
        
        return valorFormateado;        
    }
    
    public Map<String,Integer> getContadoresImplementacion(ArrayList<String> estatusImplementacion){
        
        Map<String,Integer> map_cont_imple = new HashMap<String,Integer>();
        
        Integer num_pendientes = 0;
        Integer num_confirmado = 0;
        Integer num_proceso = 0;
        Integer num_restcate = 0;
        Integer num_gestoria = 0;
        Integer num_suspendido = 0;
        Integer num_completado = 0;
        Integer num_detenida = 0;
        Integer num_calendarizado = 0;
        Integer num_Cancelado = 0;
        Integer num_otro = 0;
        
        try{
            
            for(String status : estatusImplementacion){
                switch(status){
                    case Constantes.OS_STATUS_PENDIENTE:
                        num_pendientes++;
                    break;
                    case Constantes.OS_STATUS_CONFIRMADO:
                        num_confirmado++;
                    break;
                    case Constantes.OS_STATUS_PROCESO:
                        num_proceso++;
                    break;
                    case Constantes.OS_STATUS_RESCATE:
                        num_restcate++;
                    break;
                    case Constantes.OS_STATUS_CANCELADO:
                        num_Cancelado++;
                    break;
                    case Constantes.OS_STATUS_DETENIDA:
                        num_detenida++;
                    break;
                    case Constantes.OS_STATUS_GESTORIA:
                        num_gestoria++;
                    break;
                    case Constantes.OS_STATUS_SUSPENDIDO:
                        num_suspendido++;
                    break;
                    case Constantes.OS_STATUS_CALENDARIZADO:
                        num_calendarizado++;
                    break;
                    case Constantes.OS_STATUS_COMPLETADO:
                        num_completado++;
                    break;
                    default:
                        num_otro++;
                    break;
                }
            }
                
            map_cont_imple.put(Constantes.OS_STATUS_PENDIENTE, num_pendientes);
            map_cont_imple.put(Constantes.OS_STATUS_CONFIRMADO, num_confirmado);
            map_cont_imple.put(Constantes.OS_STATUS_PROCESO, num_proceso);
            map_cont_imple.put(Constantes.OS_STATUS_RESCATE, num_restcate);
            map_cont_imple.put(Constantes.OS_STATUS_GESTORIA, num_gestoria);
            map_cont_imple.put(Constantes.OS_STATUS_SUSPENDIDO, num_suspendido);
            map_cont_imple.put(Constantes.OS_STATUS_COMPLETADO, num_completado);
            map_cont_imple.put(Constantes.OS_STATUS_DETENIDA, num_detenida);
            map_cont_imple.put(Constantes.OS_STATUS_CALENDARIZADO, num_calendarizado);
            map_cont_imple.put(Constantes.OS_STATUS_CANCELADO, num_Cancelado);
            map_cont_imple.put(Constantes.OS_STATUS_OTRO, num_otro);
            
        }catch(Exception ex){
            System.err.println("Error al cargar contadores");
        } 
        
        return map_cont_imple;
    }
    
    public Map<String,String> getContadoresEstatusCsp(ArrayList<String> listaEstatus){
        
        Map<String,String> map_cont_imple = new HashMap<String,String>();
        
        Integer totalPuntas = 0;
        Integer totalPuntasPorInstalar = 0;
        Integer totalPuntasCalendarizado = 0;
        Integer totalPuntasDetenido = 0;
        Integer totalPuntasDevueltoVentas = 0;
        Integer totalPuntasCancelado = 0;
        Integer totalPuntasInstalado = 0;
           
        try{
            totalPuntas = listaEstatus.size();
            for(String statusCsp : listaEstatus){
                switch(statusCsp){
                    case Constantes.C_POR_INSTALAR:
                        totalPuntasPorInstalar++;
                    break;
                    case Constantes.C_CALENDARIZADO:
                        totalPuntasCalendarizado++;
                    break;
                    case Constantes.C_DETENIDO:
                        totalPuntasDetenido++;
                    break;
                    case Constantes.C_DEVUELTO_VENTAS:
                        totalPuntasDevueltoVentas++;
                    break;
                    case Constantes.C_CANCELADO:
                        totalPuntasCancelado++;
                        break;
                    case Constantes.C_INSTALADO:
                        totalPuntasInstalado++;
                    break;
                }
                
                
            }
            
            map_cont_imple.put("Total",totalPuntas.toString());
            map_cont_imple.put(Constantes.C_POR_INSTALAR,totalPuntasPorInstalar.toString());
            map_cont_imple.put(Constantes.C_CALENDARIZADO,totalPuntasCalendarizado.toString());
            map_cont_imple.put(Constantes.C_DETENIDO,totalPuntasDetenido.toString());
            map_cont_imple.put(Constantes.C_DEVUELTO_VENTAS,totalPuntasDevueltoVentas.toString());
            map_cont_imple.put(Constantes.C_CANCELADO,totalPuntasCancelado.toString());
            map_cont_imple.put(Constantes.C_INSTALADO,totalPuntasInstalado.toString());
            
        }catch(Exception ex){
           // LocalUtils.console(Constantes.C_TYPE_GEN_ERR, "Error : " + ex.toString(), "calculaGraficaGeneral");
        }
        
        return map_cont_imple;
    }
   
    public Map<String,Integer> getContadoresPlaneacion(ArrayList<String> listSemaforo){
        
        Map<String,Integer> map_cont_imple = new HashMap<String,Integer>();
        
        Integer puntas_en_tiempo = 0;
        Integer puntas_en_riesgo = 0;
        Integer puntas_fuera_tiempo = 0;
        Integer pendienetes = 0;
        
        try{
            
            for(String semaforo : listSemaforo){
                if(semaforo.equals(Variables.getCOLOR_ENTIEMPO())){
                    ++puntas_en_tiempo;
                }else if(semaforo.equals(Variables.getCOLOR_RIESGO())){
                    ++puntas_en_riesgo;
                }else if(semaforo.equals(Variables.getCOLOR_FUERA_TIEMPO())){
                    ++puntas_fuera_tiempo;
                }else{
                    ++pendienetes;    
                }
            }
                
            map_cont_imple.put(Constantes.C_ENTIEMPO, puntas_en_tiempo);
            map_cont_imple.put(Constantes.C_ENRIESGO, puntas_en_riesgo);
            map_cont_imple.put(Constantes.C_FUERATIEMPO, puntas_fuera_tiempo);
            map_cont_imple.put(Constantes.C_PENDIENTES, pendienetes);
            
        }catch(Exception ex){
            System.err.println("Error al cargar contadores");
        } 
        
        return map_cont_imple;
    }
    
    public Map<String,String> getContadoresPlaneacion(Map<String,Actividad> listSemaforo){
        
        Map<String,String> map_cont_imple = new HashMap<String,String>();
        
        Integer puntas_en_tiempo = 0;
        Integer puntas_en_riesgo = 0;
        Integer puntas_fuera_tiempo = 0;
        Integer pendienetes = 0;
        
        try{
            
            for(Actividad semaforo : listSemaforo.values()){
                if(semaforo.getSemaforo().equals(Variables.getCOLOR_ENTIEMPO())){
                    ++puntas_en_tiempo;
                }else if(semaforo.getSemaforo().equals(Variables.getCOLOR_RIESGO())){
                    ++puntas_en_riesgo;
                }else if(semaforo.getSemaforo().equals(Variables.getCOLOR_FUERA_TIEMPO())){
                    ++puntas_fuera_tiempo;
                }else{
                    ++pendienetes;    
                }
            }
                
            map_cont_imple.put(Constantes.C_ENTIEMPO, puntas_en_tiempo.toString());
            map_cont_imple.put(Constantes.C_ENRIESGO, puntas_en_riesgo.toString());
            map_cont_imple.put(Constantes.C_FUERATIEMPO, puntas_fuera_tiempo.toString());
            map_cont_imple.put(Constantes.C_PENDIENTES, pendienetes.toString());
            
        }catch(Exception ex){
            System.err.println("Error al cargar contadores : " + ex.toString());
        } 
        
        return map_cont_imple;
    }
    
    public String cleanString(String cadena){
        
        String cadenaNueva = "";
        
        try{
            
            cadenaNueva = cadena != null ? cadena : "Sin información";
            
        }catch(Exception ex){
            System.err.println("Error al limpiar cadena : " + ex.toString());    
        }
        
        return cadenaNueva;
    }
}
