package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputDetalleObjetoSF;
import com.proyectmanager.logica.InformacionObjetoSF;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.outputs.OutputDetalleObjetoSF;

import javax.ws.rs.core.Response;

public class DetalleObjetoSF {
    
    public DetalleObjetoSF() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    
    public Response detalleObjetoSF(String InputDetalleObjetoSF){
        
        InputDetalleObjetoSF input_ws = (InputDetalleObjetoSF) Constantes.UTILS.strJsonToClass(InputDetalleObjetoSF, InputDetalleObjetoSF.class);
        OutputDetalleObjetoSF output_ws = new OutputDetalleObjetoSF();
        
        try{
            String idSf = input_ws.getIdObjectSF();
            switch (input_ws.getTypeObjectSF()){
                case Constantes.TYPE_CUENTA:
                    output_ws.setDetalleCuenta(InformacionObjetoSF.detalleCuenta(idSf));
                break;
                case Constantes.TYPE_OPORTUNIDAD:
                    output_ws.setDetalleOportunidad(InformacionObjetoSF.detalleOportunidad(idSf));
                break;
                case Constantes.TYPE_COTIZACION:
                    output_ws.setDetalleCotizacion(InformacionObjetoSF.detalleCotizacion(idSf));
                break;
                case Constantes.TYPE_COTSITIO:
                    output_ws.setDetalleCotSitio(InformacionObjetoSF.detalleCotSitio(idSf));
                break;
                case Constantes.TYPE_COTSITIOPLAN:
                    output_ws.setDetalleCotSitioPlan(InformacionObjetoSF.detalleCotSitioPlan(idSf));
                break;
                case Constantes.TYPE_CUENTAFACTURA:
                    output_ws.setDetalleCuentaFactura(InformacionObjetoSF.detalleCuentaFactura(idSf));
                break;
                case Constantes.TYPE_OS:
                    output_ws.setDetalleOs(InformacionObjetoSF.detalleOs(idSf));
                break;
                case Constantes.TYPE_TK:
                    output_ws.setDetalleTk(InformacionObjetoSF.detalleTicket(idSf));
                break;
                case Constantes.TYPE_USER:
                    output_ws.setDetalleContacto(InformacionObjetoSF.detalleContacto(idSf));
                break;
            
            }
            
                        
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
}
