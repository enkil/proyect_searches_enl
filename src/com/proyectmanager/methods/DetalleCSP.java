package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputDetalleCSP;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.DetalleInstalacion;
import com.proyectmanager.outputs.OutputDetalleCSP;

import javax.ws.rs.core.Response;

public class DetalleCSP {
    public DetalleCSP() {
        super();
    }
    
    public Response consultaDetalleInstalacion(String InputDetalleCSP){
        
        InputDetalleCSP input_ws = (InputDetalleCSP) Constantes.UTILS.strJsonToClass(Constantes.UTILS.jsonSanitizeTwo(InputDetalleCSP), InputDetalleCSP.class);
        OutputDetalleCSP output_ws = new OutputDetalleCSP();
        
        try{
            String dataInstalacion [][];
            String consulta = "";
            
            switch (input_ws.getIdUnidadNegocio()){
                case Constantes.ID_UNIDAD_ENL:
                    consulta = Constantes.QR_DET_INSTALACION_ENL.replaceAll("ORTET", input_ws.getIdOt());
                break;
                case Constantes.ID_UNIDAD_REC:
                    consulta = Constantes.QR_DET_INSTALACION_REC.replaceAll("ORTET", input_ws.getIdOt());
                break;
                case Constantes.ID_UNIDAD_PE:
                    consulta = Constantes.QR_DET_INSTALACION_PE.replaceAll("ORTET", input_ws.getIdOt());
                break;
            }
            
            System.out.println(consulta);
            
            dataInstalacion = Constantes.CRUD_FFM.consultaBD(consulta);
            DetalleInstalacion detalleInstalacion = new DetalleInstalacion();
            
            if(dataInstalacion.length > 0){
                detalleInstalacion.setNombre(dataInstalacion[0][2]);
                detalleInstalacion.setApellidoPaterno(dataInstalacion[0][3]);
                detalleInstalacion.setApellidoMaterno(dataInstalacion[0][4]);
                detalleInstalacion.setNombre_Tecnico(dataInstalacion[0][0]);
                detalleInstalacion.setNumTelefonico(dataInstalacion[0][5]);
                detalleInstalacion.setAutomovil(dataInstalacion[0][1]);
            }else{
                detalleInstalacion.setNombre("Pendiente");
                detalleInstalacion.setApellidoPaterno("De");
                detalleInstalacion.setApellidoMaterno("Asignacion");
                detalleInstalacion.setNombre_Tecnico("Pendiente De Asignacion");
                detalleInstalacion.setNumTelefonico("Pendiente");
                detalleInstalacion.setAutomovil("Sin definir");
            }
            
            output_ws.setResult(Constantes. RESULT_SUSSES);                     
            output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
            output_ws.setVersion(Constantes.WS_VERSION);
            output_ws.setDetalleInstalacion(detalleInstalacion);
        }catch(Exception ex){
            System.err.println("Error en consulta detalle instalacion : " + ex.toString());
            output_ws.setResult(Constantes.RESULT_ERROR);                     
            output_ws.setResultDescripcion("Error al cargar detalle instalacion : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
            
    }
}
