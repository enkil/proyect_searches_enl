package com.proyectmanager.methods;

import com.proyectmanager.hilos.HiloActividadesPE;
import com.proyectmanager.hilos.HiloActividadesPIEnl;
import com.proyectmanager.hilos.HiloActividadesPIRec;
import com.proyectmanager.hilos.HiloActualizaActENL;
import com.proyectmanager.inputs.InputConsultaActividades;
import com.proyectmanager.logica.ActividadesPMs;
import com.proyectmanager.logica.CargaActividades;
import com.proyectmanager.logica.CargaEstructuraBase;
import com.proyectmanager.logica.PMActividades;
import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.DetalleAvances;
import com.proyectmanager.outputs.OutputConsultaActividades;

import java.util.Map;

import javax.ws.rs.core.Response;

public class ConsultaActividades {
    public ConsultaActividades() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response consultaActividades(String InputConsultaActividades){
        
        InputConsultaActividades input_ws = (InputConsultaActividades) Constantes.UTILS.strJsonToClass(Constantes.UTILS.jsonSanitizeTwo(InputConsultaActividades), InputConsultaActividades.class);
        OutputConsultaActividades output_ws = new OutputConsultaActividades();
        
        try{
            
            if(this.loadDataActivities(input_ws)){
                //-- Carga las actividades existentes
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                Map<String,Actividad> map_actividades = new ActividadesPMs().loadMapActividades(input_ws.getId_csp());
                Map<String,String> contadores_planeacion = Constantes.UTILS.getContadoresPlaneacion(map_actividades);
                output_ws.setActividades(map_actividades.values().toArray(new Actividad[map_actividades.size()]));
                output_ws.setNum_fuera_tiempo(contadores_planeacion.get(Constantes.C_FUERATIEMPO).toString());
                output_ws.setNum_en_riesgo(contadores_planeacion.get(Constantes.C_ENRIESGO).toString());
                output_ws.setNum_en_tiempo(contadores_planeacion.get(Constantes.C_ENTIEMPO).toString());
                output_ws.setNum_pendientes(contadores_planeacion.get(Constantes.C_PENDIENTES).toString());    
            }else{
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
            }
            
            
            
        }catch(Exception ex){
            System.err.println("Error al consultar actividades : " + ex.toString());
        }
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
    
    private Boolean loadDataActivities(InputConsultaActividades input_ws){
        
        Boolean bandera = true;
        
        try{
            String id_implementacion = "";
            String arr_actividades[][]= Constantes.CRUD_FFM.consultaBD(Constantes.C_CONSULTA_ID_IMPLEMENTACION.replaceAll("idbridgesf", input_ws.getId_csp()));
            
            //-- Carga estructura base
            if(arr_actividades!= null && arr_actividades.length > 0){
                id_implementacion = arr_actividades[0][0];
            }else{
                id_implementacion = CargaEstructuraBase.cargaEstructuraBasica(input_ws.getId_cuenta(),input_ws.getId_csp(), input_ws.getId_pm(), Constantes.C_TIENE_CSP);
            }
            
            //-- Busca ots en pi y pe y crea actividades de ser necesario
            if(!input_ws.getFolio_OS().equals("-1")){
                System.out.println("dentro de la carga");
                new HiloActividadesPIRec().cargaActividadesNuevas(input_ws.getFolio_OS(),input_ws.getId_csp(),id_implementacion);
                new HiloActualizaActENL().actualizaActividades(input_ws.getId_csp());
                System.out.println("fuera de la carga");
            }
        }catch(Exception ex){
            System.err.println("Error al consultar actividades : " + ex.toString());
            bandera=false;
        }
        
        return bandera;
    }
}
