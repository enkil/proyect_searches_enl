package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputFinalizaProyecto;

import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputFinalizaProyecto;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;

import javax.ws.rs.core.Response;

public class FinalizaProyecto {
    public FinalizaProyecto() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response finalizaProyecto(String InputFinalizaProyecto){
        
        InputFinalizaProyecto input_ws = (InputFinalizaProyecto) Constantes.UTILS.strJsonToClass(Constantes.UTILS.jsonSanitizeTwo(InputFinalizaProyecto), InputFinalizaProyecto.class);
        OutputFinalizaProyecto output_ws = new OutputFinalizaProyecto();
        
        try{
            
            output_ws = this.finalizaProyecto(input_ws.getId_cs(),input_ws.getId_cot());
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            System.err.println("Error : " + tx.toString());
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();        
    }
    
    public OutputFinalizaProyecto finalizaProyecto(String id_cs,String id_cot){
        OutputFinalizaProyecto output_ws = new OutputFinalizaProyecto();
        
        try{
            System.out.println("Consulta  : " + Constantes.QR_UPDATE_END_ACTIVITIES.replaceAll("id_css", id_cs).replaceAll("KPIPI", Variables.getKPI_PI()));
            Boolean end_activities = Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_END_ACTIVITIES.replaceAll("id_css", id_cs).replaceAll("KPIPI", Variables.getKPI_PI()));
            Boolean end_proyect = false;
            if(end_activities){
                end_proyect = Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_END_PROYECT.replaceAll("id_css", id_cs)); 
                if(end_proyect){
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(this.cierraProyecto(id_cot));
                //-- valida si existen puntas pendientes de lo contrario finaliza el proyecto
                
                }else{
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion("Error al terminar punta");
                    output_ws.setVersion(Constantes.WS_VERSION);
                }
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("Error al terminar actividades");
                output_ws.setVersion(Constantes.WS_VERSION);    
            } 
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
            output_ws.setResult(Constantes.RESULT_SUSSES);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION); 
        }
        
        return output_ws;
    }
    
    public String cierraProyecto(String id_cot){
        String cadena = "";
        try{
            Integer puntas_vivas = Integer.valueOf(Constantes.CRUD_FFM.consultaBD(Constantes.QR_NUM_PUNTAS_VIVAS.replaceAll("id_cots",id_cot))[0][0]);    
            QueryResult det_op = Constantes.CRUD_SF.query_sf(Constantes.QR_GET_ID_OP.replaceAll("id_cotz", id_cot));
            if(puntas_vivas == 0){
                
                String id_oportunidad = "";
                for(SObject op_obj : det_op.getRecords()){
                    id_oportunidad = (String) op_obj.getField("Oportunidad__c");
                }
                
                SObject[] oportunidad = new SObject[1];
                SObject op = new SObject();
                op.setType(Constantes.OPPORTUNITY_TYPE);
                op.setId(id_oportunidad);
                op.setField("EstatusImplementacion__c", Constantes.STATUS_IMPLEMENTACION);
                oportunidad[0] = op; 
                SaveResult[] update_sf = Constantes.CRUD_SF.update_sf(oportunidad);
            }
        }catch(Exception ex){
            System.err.println(ex.toString());
            cadena = "Error al actualizar punta sf : " + ex.toString();               
        }    
        
        return cadena;
    }
}
