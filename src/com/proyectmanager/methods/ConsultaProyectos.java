package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputConsultaProyectos;

import com.proyectmanager.logica.EnCancelados;
import com.proyectmanager.logica.EnFacturacion;
import com.proyectmanager.logica.EnMesaControl;
import com.proyectmanager.logica.EnPlaneacion;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputConsultaProyectos;

import com.sforce.soap.partner.QueryResult;

import javax.ws.rs.core.Response;

import  org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;


public class ConsultaProyectos {
    public ConsultaProyectos() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
       
    public Response consultaProyectos(String inputConsultaProyectos){
        
        InputConsultaProyectos input_ws = (InputConsultaProyectos) Constantes.UTILS.strJsonToClass(Constantes.UTILS.jsonSanitizeTwo(inputConsultaProyectos), InputConsultaProyectos.class);        
        OutputConsultaProyectos output_ws = new OutputConsultaProyectos();
        try{
           
            switch (input_ws.getTipo_bandeja()){
                case Constantes.BANDEJA_PLANEACION :
                    output_ws = new EnPlaneacion().proyectosEnPlaneacion(input_ws);
                break;
                case Constantes.BANDEJA_MESA_CONTROL :
                    output_ws = new EnMesaControl().proyectosEnMesaControl(input_ws);
                break;
                case Constantes.BANDEJA_FACTURANDO :
                    output_ws = new EnFacturacion().proyectosEnFacturacion(input_ws);
                break;
                case Constantes.BANDEJA_CANCELADOS :
                    output_ws = new EnCancelados().getProyectosEnCancelados(input_ws);
                break;
                default:
                    output_ws.setResult(Constantes.RESULT_ERROR);
                    output_ws.setResultDescripcion("No se encontro logica para : " + input_ws.getTipo_bandeja());
                    output_ws.setVersion(Constantes.WS_VERSION);
                break;
            } 
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
    }
}
