package com.proyectmanager.methods;

import com.proyectmanager.inputs.InputFindGeneralSF;
import com.proyectmanager.logica.CargaFindGeneralSF;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputFindGeneralSF;
import com.sforce.soap.partner.SearchRecord;

import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

public class FindGeneralSF {
    public FindGeneralSF() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    public Response findGeneralSF(String InputFindGeneralSF){
        
        InputFindGeneralSF input_ws = (InputFindGeneralSF) Constantes.UTILS.strJsonToClass(InputFindGeneralSF, InputFindGeneralSF.class);
        OutputFindGeneralSF output_ws = new OutputFindGeneralSF();
        
        try{

            SearchRecord[] arr_resultados = Constantes.CRUD_SF.search_sf(Constantes.C_BASE_FIND_SF.replaceAll("parametroLimpio", input_ws.getParametroBusqueda().replaceAll("\\*", "")).replaceAll("parametro", input_ws.getParametroBusqueda().replaceAll("-", "\\\\\\\\\\\\-")) + Variables.getSEARCH_GENERAL_ENL_SF());
            ArrayList<SObject> arrListCuenta = new ArrayList<SObject>();
            ArrayList<SObject> arrListOportunidad = new ArrayList<SObject>();
            ArrayList<SObject> arrListCotizacion = new ArrayList<SObject>();
            ArrayList<SObject> arrListCotSitio = new ArrayList<SObject>();
            ArrayList<SObject> arrListCotSitioPlan = new ArrayList<SObject>();
            ArrayList<SObject> arrListCuentaFactura = new ArrayList<SObject>();
            ArrayList<SObject> arrListOs = new ArrayList<SObject>();
            ArrayList<SObject> arrListTk = new ArrayList<SObject>();
            
            if(arr_resultados.length > 0){
                for(SearchRecord item_sf : arr_resultados){
                    SObject elementoSf = item_sf.getRecord();
                    switch(elementoSf.getType()){
                        case "Account":
                            arrListCuenta.add(elementoSf);
                        break;
                        case "Opportunity":
                            arrListOportunidad.add(elementoSf);
                        break;
                        case "Cotizacion__c":
                            arrListCotizacion.add(elementoSf);
                        break;
                        case "Cot_Sitio__c":
                            arrListCotSitio.add(elementoSf);
                        break;
                        case "Cot_SitioPlan__c":
                            arrListCotSitioPlan.add(elementoSf);
                        break;
                        case "CuentaFactura__c":
                            arrListCuentaFactura.add(elementoSf);
                        break;
                        case "OrdenServicio__c":
                            arrListOs.add(elementoSf);
                        break;
                        case "Case":
                            arrListTk.add(elementoSf);
                        break;
                        
                    }
                }
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setArrCuentas(CargaFindGeneralSF.cargaInformacionCuentas(arrListCuenta));
                output_ws.setArrOportunidad(CargaFindGeneralSF.cargaInformacionOportunidad(arrListOportunidad));
                output_ws.setArrCotizacion(CargaFindGeneralSF.cargaInformacionCotizacion(arrListCotizacion));
                output_ws.setArrCotSitio(CargaFindGeneralSF.cargaInformacionCotSitio(arrListCotSitio));
                output_ws.setArrCotSitioPlan(CargaFindGeneralSF.cargaInformacionCotSitioPlan(arrListCotSitioPlan));
                output_ws.setArrCuentaFactura(CargaFindGeneralSF.cargaInformacionCuentaFactura(arrListCuentaFactura));
                output_ws.setArrOs(CargaFindGeneralSF.cargaInformacionOs(arrListOs));
                output_ws.setArrTk(CargaFindGeneralSF.cargaInformacionTicket(arrListTk));
                
            }else{
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("No se encontraron resultados");
                output_ws.setVersion(Constantes.WS_VERSION);    
            }
            
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }catch(Throwable tx){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("Error : " + tx.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        //System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return Response.status(200).entity(Constantes.UTILS.classTostrJson(output_ws)).build();
        
    }
    
}
