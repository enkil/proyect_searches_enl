package com.proyectmanager.outputs;

import com.proyectmanager.objects.CotSitio;
import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Cotizacion;
import com.proyectmanager.objects.Cuenta;
import com.proyectmanager.objects.CuentaFactura;
import com.proyectmanager.objects.Oportunidad;
import com.proyectmanager.objects.Os;
import com.proyectmanager.objects.Ticket;

public class OutputFindGeneralSF {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private Cuenta[] arrCuentas;
    private Oportunidad[] arrOportunidad;
    private Cotizacion[] arrCotizacion;
    private CotSitio[] arrCotSitio;
    private CotSitioPlan[] arrCotSitioPlan;
    private CuentaFactura[] arrCuentaFactura;
    private Os[] arrOs;
    private Ticket[] arrTk;
        
    public OutputFindGeneralSF() {
        super();
    }


    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setArrCuentas(Cuenta[] arrCuentas) {
        this.arrCuentas = arrCuentas;
    }

    public Cuenta[] getArrCuentas() {
        return arrCuentas;
    }

    public void setArrOportunidad(Oportunidad[] arrOportunidad) {
        this.arrOportunidad = arrOportunidad;
    }

    public Oportunidad[] getArrOportunidad() {
        return arrOportunidad;
    }

    public void setArrCotizacion(Cotizacion[] arrCotizacion) {
        this.arrCotizacion = arrCotizacion;
    }

    public Cotizacion[] getArrCotizacion() {
        return arrCotizacion;
    }

    public void setArrCotSitio(CotSitio[] arrCotSitio) {
        this.arrCotSitio = arrCotSitio;
    }

    public CotSitio[] getArrCotSitio() {
        return arrCotSitio;
    }

    public void setArrCotSitioPlan(CotSitioPlan[] arrCotSitioPlan) {
        this.arrCotSitioPlan = arrCotSitioPlan;
    }

    public CotSitioPlan[] getArrCotSitioPlan() {
        return arrCotSitioPlan;
    }

    public void setArrCuentaFactura(CuentaFactura[] arrCuentaFactura) {
        this.arrCuentaFactura = arrCuentaFactura;
    }

    public CuentaFactura[] getArrCuentaFactura() {
        return arrCuentaFactura;
    }

    public void setArrOs(Os[] arrOs) {
        this.arrOs = arrOs;
    }

    public Os[] getArrOs() {
        return arrOs;
    }

    public void setArrTk(Ticket[] arrTk) {
        this.arrTk = arrTk;
    }

    public Ticket[] getArrTk() {
        return arrTk;
    }
}
