package com.proyectmanager.outputs;

import com.proyectmanager.objects.Actividad;

import com.proyectmanager.objects.DetalleAvances;

import org.glassfish.jersey.internal.Version;


public class OutputConsultaActividades {
    public OutputConsultaActividades() {
        super();
    }
    
    private String result;
    private String resultDescripcion;
    private String Version;
    private String Total_puntas;
    private String Num_en_tiempo;
    private String Num_en_riesgo;
    private String Num_fuera_tiempo;
    private String Num_pendientes;
    private DetalleAvances DetalleAvances;
    private Actividad Actividades[];

    public void setDetalleAvances(DetalleAvances DetalleAvances) {
        this.DetalleAvances = DetalleAvances;
    }

    public DetalleAvances getDetalleAvances() {
        return DetalleAvances;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setTotal_puntas(String Total_puntas) {
        this.Total_puntas = Total_puntas;
    }

    public String getTotal_puntas() {
        return Total_puntas;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setActividades(Actividad[] Actividades) {
        this.Actividades = Actividades;
    }

    public Actividad[] getActividades() {
        return Actividades;
    }

}
