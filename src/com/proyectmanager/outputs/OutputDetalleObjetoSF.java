package com.proyectmanager.outputs;

import com.proyectmanager.objects.Contacto;
import com.proyectmanager.objects.CotSitio;
import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Cotizacion;
import com.proyectmanager.objects.Cuenta;
import com.proyectmanager.objects.CuentaFactura;
import com.proyectmanager.objects.Oportunidad;
import com.proyectmanager.objects.Os;
import com.proyectmanager.objects.Ticket;

public class OutputDetalleObjetoSF {
    
    private String result;
    private String resultDescripcion;
    private String version;
    private Cuenta detalleCuenta;
    private Oportunidad detalleOportunidad;
    private Cotizacion detalleCotizacion;
    private CotSitio detalleCotSitio;
    private CotSitioPlan detalleCotSitioPlan;
    private CuentaFactura detalleCuentaFactura;
    private Ticket detalleTk;
    private Os detalleOs;
    private Contacto detalleContacto;
    
    public OutputDetalleObjetoSF() {
        super();
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setDetalleCuenta(Cuenta detalleCuenta) {
        this.detalleCuenta = detalleCuenta;
    }

    public Cuenta getDetalleCuenta() {
        return detalleCuenta;
    }

    public void setDetalleOportunidad(Oportunidad detalleOportunidad) {
        this.detalleOportunidad = detalleOportunidad;
    }

    public Oportunidad getDetalleOportunidad() {
        return detalleOportunidad;
    }

    public void setDetalleCotizacion(Cotizacion detalleCotizacion) {
        this.detalleCotizacion = detalleCotizacion;
    }

    public Cotizacion getDetalleCotizacion() {
        return detalleCotizacion;
    }

    public void setDetalleCotSitio(CotSitio detalleCotSitio) {
        this.detalleCotSitio = detalleCotSitio;
    }

    public CotSitio getDetalleCotSitio() {
        return detalleCotSitio;
    }

    public void setDetalleCotSitioPlan(CotSitioPlan detalleCotSitioPlan) {
        this.detalleCotSitioPlan = detalleCotSitioPlan;
    }

    public CotSitioPlan getDetalleCotSitioPlan() {
        return detalleCotSitioPlan;
    }

    public void setDetalleCuentaFactura(CuentaFactura detalleCuentaFactura) {
        this.detalleCuentaFactura = detalleCuentaFactura;
    }

    public CuentaFactura getDetalleCuentaFactura() {
        return detalleCuentaFactura;
    }

    public void setDetalleOs(Os detalleOs) {
        this.detalleOs = detalleOs;
    }

    public Os getDetalleOs() {
        return detalleOs;
    }

    public void setDetalleContacto(Contacto detalleContacto) {
        this.detalleContacto = detalleContacto;
    }

    public Contacto getDetalleContacto() {
        return detalleContacto;
    }

    public void setDetalleTk(Ticket detalleTk) {
        this.detalleTk = detalleTk;
    }

    public Ticket getDetalleTk() {
        return detalleTk;
    }
}
