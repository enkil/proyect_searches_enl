package com.proyectmanager.outputs;

import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Proyecto;

public class OutputConsultaProyectos {
    public OutputConsultaProyectos() {
        super();
    }
    
    private String result;
    private String resultDescripcion;
    private String version;
    private String Total_proyectos;
    private String Num_total_clientes;
    private String Num_total_Puntas;
    private String Num_total_planes;
    private String Num_en_tiempo;
    private String Num_en_riesgo;
    private String Num_fuera_tiempo; 
    private String Num_pendientes;    
    private String OS_pendientes;
    private String Num_confirmado;
    private String Num_proceso;
    private String Num_restcate;
    private String Num_Cancelado;
    private String Num_detenida;
    private String Num_gestoria;
    private String Num_suspendido;
    private String Num_completado;
    private String Num_calendarizado;
    private String Num_otro;
    private String totalPuntas;
    private String totalPuntasPorInstalar;
    private String totalPuntasCalendarizado;
    private String totalPuntasDetenido;
    private String totalPuntasDevueltoVentas;
    private String totalPuntasCancelado;
    private String totalPuntasInstalado;
    private String Url_Dashboard;
    private Proyecto[] Proyectos;
    private CotSitioPlan cspFacturando[];


    public void setNum_total_clientes(String Num_total_clientes) {
        this.Num_total_clientes = Num_total_clientes;
    }

    public String getNum_total_clientes() {
        return Num_total_clientes;
    }

    public void setNum_total_Puntas(String Num_total_Puntas) {
        this.Num_total_Puntas = Num_total_Puntas;
    }

    public String getNum_total_Puntas() {
        return Num_total_Puntas;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setTotal_proyectos(String Total_proyectos) {
        this.Total_proyectos = Total_proyectos;
    }

    public String getTotal_proyectos() {
        return Total_proyectos;
    }

    public void setNum_fuera_tiempo(String Num_fuera_tiempo) {
        this.Num_fuera_tiempo = Num_fuera_tiempo;
    }

    public String getNum_fuera_tiempo() {
        return Num_fuera_tiempo;
    }

    public void setNum_en_riesgo(String Num_en_riesgo) {
        this.Num_en_riesgo = Num_en_riesgo;
    }

    public String getNum_en_riesgo() {
        return Num_en_riesgo;
    }

    public void setNum_en_tiempo(String Num_en_tiempo) {
        this.Num_en_tiempo = Num_en_tiempo;
    }

    public String getNum_en_tiempo() {
        return Num_en_tiempo;
    }

    public void setNum_pendientes(String Num_pendientes) {
        this.Num_pendientes = Num_pendientes;
    }

    public String getNum_pendientes() {
        return Num_pendientes;
    }

    public void setUrl_Dashboard(String Url_Dashboard) {
        this.Url_Dashboard = Url_Dashboard;
    }

    public String getUrl_Dashboard() {
        return Url_Dashboard;
    }

    public void setProyectos(Proyecto[] Proyectos) {
        this.Proyectos = Proyectos;
    }

    public Proyecto[] getProyectos() {
        return Proyectos;
    }

    public void setOS_pendientes(String OS_pendientes) {
        this.OS_pendientes = OS_pendientes;
    }

    public String getOS_pendientes() {
        return OS_pendientes;
    }

    public void setNum_confirmado(String Num_confirmado) {
        this.Num_confirmado = Num_confirmado;
    }

    public String getNum_confirmado() {
        return Num_confirmado;
    }

    public void setNum_proceso(String Num_proceso) {
        this.Num_proceso = Num_proceso;
    }

    public String getNum_proceso() {
        return Num_proceso;
    }

    public void setNum_restcate(String Num_restcate) {
        this.Num_restcate = Num_restcate;
    }

    public String getNum_restcate() {
        return Num_restcate;
    }

    public void setNum_Cancelado(String Num_Cancelado) {
        this.Num_Cancelado = Num_Cancelado;
    }

    public String getNum_Cancelado() {
        return Num_Cancelado;
    }

    public void setNum_detenida(String Num_detenida) {
        this.Num_detenida = Num_detenida;
    }

    public String getNum_detenida() {
        return Num_detenida;
    }

    public void setNum_gestoria(String Num_gestoria) {
        this.Num_gestoria = Num_gestoria;
    }

    public String getNum_gestoria() {
        return Num_gestoria;
    }

    public void setNum_suspendido(String Num_suspendido) {
        this.Num_suspendido = Num_suspendido;
    }

    public String getNum_suspendido() {
        return Num_suspendido;
    }

    public void setNum_completado(String Num_completado) {
        this.Num_completado = Num_completado;
    }

    public String getNum_completado() {
        return Num_completado;
    }

    public void setNum_calendarizado(String Num_calendarizado) {
        this.Num_calendarizado = Num_calendarizado;
    }

    public String getNum_calendarizado() {
        return Num_calendarizado;
    }

    public void setNum_otro(String Num_otro) {
        this.Num_otro = Num_otro;
    }

    public String getNum_otro() {
        return Num_otro;
    }

    public void setNum_total(String Num_total) {
        this.Num_total_planes = Num_total;
    }

    public String getNum_total() {
        return Num_total_planes;
    }

    public void setTotalPuntas(String totalPuntas) {
        this.totalPuntas = totalPuntas;
    }

    public String getTotalPuntas() {
        return totalPuntas;
    }

    public void setTotalPuntasPorInstalar(String totalPuntasPorInstalar) {
        this.totalPuntasPorInstalar = totalPuntasPorInstalar;
    }

    public String getTotalPuntasPorInstalar() {
        return totalPuntasPorInstalar;
    }

    public void setTotalPuntasCalendarizado(String totalPuntasCalendarizado) {
        this.totalPuntasCalendarizado = totalPuntasCalendarizado;
    }

    public String getTotalPuntasCalendarizado() {
        return totalPuntasCalendarizado;
    }

    public void setTotalPuntasDetenido(String totalPuntasDetenido) {
        this.totalPuntasDetenido = totalPuntasDetenido;
    }

    public String getTotalPuntasDetenido() {
        return totalPuntasDetenido;
    }

    public void setTotalPuntasDevueltoVentas(String totalPuntasDevueltoVentas) {
        this.totalPuntasDevueltoVentas = totalPuntasDevueltoVentas;
    }

    public String getTotalPuntasDevueltoVentas() {
        return totalPuntasDevueltoVentas;
    }

    public void setTotalPuntasCancelado(String totalPuntasCancelado) {
        this.totalPuntasCancelado = totalPuntasCancelado;
    }

    public String getTotalPuntasCancelado() {
        return totalPuntasCancelado;
    }

    public void setTotalPuntasInstalado(String totalPuntasInstalado) {
        this.totalPuntasInstalado = totalPuntasInstalado;
    }

    public String getTotalPuntasInstalado() {
        return totalPuntasInstalado;
    }

    public void setCspFacturando(CotSitioPlan[] cspFacturando) {
        this.cspFacturando = cspFacturando;
    }

    public CotSitioPlan[] getCspFacturando() {
        return cspFacturando;
    }


}
