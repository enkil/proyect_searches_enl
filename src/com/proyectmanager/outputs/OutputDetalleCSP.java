package com.proyectmanager.outputs;

import com.proyectmanager.objects.DetalleInstalacion;
import com.proyectmanager.objects.Login;

public class OutputDetalleCSP {
    public OutputDetalleCSP() {
        super();
    }
    
    private String result;
    private String resultDescripcion;
    private String Version;
    private DetalleInstalacion DetalleInstalacion;

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResultDescripcion(String resultDescripcion) {
        this.resultDescripcion = resultDescripcion;
    }

    public String getResultDescripcion() {
        return resultDescripcion;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getVersion() {
        return Version;
    }

    public void setDetalleInstalacion(DetalleInstalacion DetalleInstalacion) {
        this.DetalleInstalacion = DetalleInstalacion;
    }

    public DetalleInstalacion getDetalleInstalacion() {
        return DetalleInstalacion;
    }
}
