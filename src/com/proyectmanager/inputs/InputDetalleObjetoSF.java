package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputDetalleObjetoSF {
    
    private Login Login;
    private String idObjectSF;
    private String typeObjectSF;
    
    public InputDetalleObjetoSF() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdObjectSF(String idObjectSF) {
        this.idObjectSF = idObjectSF;
    }

    public String getIdObjectSF() {
        return idObjectSF;
    }

    public void setTypeObjectSF(String typeObjectSF) {
        this.typeObjectSF = typeObjectSF;
    }

    public String getTypeObjectSF() {
        return typeObjectSF;
    }
}
