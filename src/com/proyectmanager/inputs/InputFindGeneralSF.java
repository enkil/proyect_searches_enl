package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputFindGeneralSF {
    
    private Login Login; 
    private String parametroBusqueda;
    
    public InputFindGeneralSF() {
        super();
    }

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setParametroBusqueda(String parametroBusqueda) {
        this.parametroBusqueda = parametroBusqueda;
    }

    public String getParametroBusqueda() {
        return parametroBusqueda;
    }
}
