package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputConsultaProyectos {
    public InputConsultaProyectos() {
        super();
    }
    
    private Login Login;
    private String Tipo_bandeja;
    private String Id_pm;
    private String MesConsulta;

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setTipo_bandeja(String Tipo_bandeja) {
        this.Tipo_bandeja = Tipo_bandeja;
    }

    public String getTipo_bandeja() {
        return Tipo_bandeja;
    }

    public void setId_pm(String Id_pm) {
        this.Id_pm = Id_pm;
    }

    public String getId_pm() {
        return Id_pm;
    }

    public void setMesConsulta(String MesConsulta) {
        this.MesConsulta = MesConsulta;
    }

    public String getMesConsulta() {
        return MesConsulta;
    }
}
