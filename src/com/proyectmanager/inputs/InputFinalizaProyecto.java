package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputFinalizaProyecto {
    public InputFinalizaProyecto() {
        super();
    }
    
    private Login Login; 
    private String Id_cs;
    private String Id_cot;

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Id_cs;
    }

    public String getId_cs() {
        return Id_cs;
    }

    public void setId_cot(String Id_cot) {
        this.Id_cot = Id_cot;
    }

    public String getId_cot() {
        return Id_cot;
    }
}
