package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputDetalleCSP {
    public InputDetalleCSP() {
        super();
    }
    
    private Login Login;
    private String idOt;
    private String idUnidadNegocio;

    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setIdOt(String idOt) {
        this.idOt = idOt;
    }

    public String getIdOt() {
        return idOt;
    }

    public void setIdUnidadNegocio(String idUnidadNegocio) {
        this.idUnidadNegocio = idUnidadNegocio;
    }

    public String getIdUnidadNegocio() {
        return idUnidadNegocio;
    }
}
