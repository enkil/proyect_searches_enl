package com.proyectmanager.inputs;

import com.proyectmanager.objects.Login;

public class InputConsultaActividades {
    public InputConsultaActividades() {
        super();
    }
    
    private Login Login; 
    private String Id_cs;
    private String id_cuenta;
    private String id_csp;
    private String id_pm;
    private String folio_OS;
    
    public void setLogin(Login Login) {
        this.Login = Login;
    }

    public Login getLogin() {
        return Login;
    }

    public void setId_cs(String Id_cs) {
        this.Id_cs = Id_cs;
    }

    public String getId_cs() {
        return Id_cs;
    }

    public void setId_csp(String id_csp) {
        this.id_csp = id_csp;
    }

    public String getId_csp() {
        return id_csp;
    }

    public void setFolio_OS(String folio_OS) {
        this.folio_OS = folio_OS;
    }

    public String getFolio_OS() {
        return folio_OS;
    }

    public void setId_cuenta(String id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public String getId_cuenta() {
        return id_cuenta;
    }

    public void setId_pm(String id_pm) {
        this.id_pm = id_pm;
    }

    public String getId_pm() {
        return id_pm;
    }
}
