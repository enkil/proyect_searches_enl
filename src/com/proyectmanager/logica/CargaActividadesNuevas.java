package com.proyectmanager.logica;

import com.proyectmanager.objects.Constantes;

public class CargaActividadesNuevas {
    public CargaActividadesNuevas() {
        super();
    }
    
    
    public static void cargaActividadesNuevas(String nuevasActividades[][],String id_responsable,String id_actividad,String idImplementacion){
        
        try{
            
            for(int fila=0; fila<nuevasActividades.length; fila++){
                String id_reg_actividad = Constantes.CRUD_FFM.insertaDBGetID(Constantes.Q_REGISTRA_NEW_ACTIVIDAD.replaceAll("idact", id_actividad).replaceAll("idresp", id_responsable).replaceAll("inifechpl", nuevasActividades[fila][2]).replaceAll("finfechpl", nuevasActividades[fila][3]).replaceAll("inifechre", nuevasActividades[fila][2]).replaceAll("finfechre", nuevasActividades[fila][3]).replaceAll("porav", Constantes.C_SIN_AVANCE).replaceAll("idimple", idImplementacion), "PA_ID");
                if(id_reg_actividad!=null && !id_reg_actividad.equals("")){
                    //System.out.println("caso uno");
                    Constantes.CRUD_FFM.insertaDB(Constantes.Q_REG_OTS_OF_PE.replaceAll("idact", id_reg_actividad).replaceAll("idorder", nuevasActividades[fila][0]).replaceAll("ottip", nuevasActividades[fila][8]).replaceAll("otsubti", nuevasActividades[fila][9]).replaceAll("negs", Constantes.ID_UNIDAD_PE));
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar actividades nuevas : " + ex.toString());    
        }
        
    }
    
    public static void cargaActividadesNuevasPI(String nuevasActividades[][],String id_responsable,String id_actividad,String idImplementacion,String unidad_negocio,String idbridgeSF){
        
        try{
            
            String arr_ots_ex [][] = Constantes.CRUD_FFM.consultaBD(Constantes.C_RELACION_IMPLEMENTACION.replaceAll("idbridgesf", idbridgeSF));
            for(int fila=0; fila<nuevasActividades.length; fila++){
                String id_reg_actividad = "";
                
                if(fila == 0){
                    if(arr_ots_ex[0][1].equals("-1")){
                        id_reg_actividad = arr_ots_ex[0][0];
                    }else{
                        id_reg_actividad = Constantes.CRUD_FFM.insertaDBGetID(Constantes.Q_REGISTRA_NEW_ACTIVIDAD.replaceAll("idact", id_actividad).replaceAll("idresp", id_responsable).replaceAll("inifechpl", nuevasActividades[fila][2]).replaceAll("finfechpl", nuevasActividades[fila][3]).replaceAll("inifechre", nuevasActividades[fila][2]).replaceAll("finfechre", nuevasActividades[fila][3]).replaceAll("porav", Constantes.C_SIN_AVANCE).replaceAll("idimple", idImplementacion), "PA_ID");
                    }    
                }else{
                    id_reg_actividad = Constantes.CRUD_FFM.insertaDBGetID(Constantes.Q_REGISTRA_NEW_ACTIVIDAD.replaceAll("idact", id_actividad).replaceAll("idresp", id_responsable).replaceAll("inifechpl", nuevasActividades[fila][2]).replaceAll("finfechpl", nuevasActividades[fila][3]).replaceAll("inifechre", nuevasActividades[fila][2]).replaceAll("finfechre", nuevasActividades[fila][3]).replaceAll("porav", Constantes.C_SIN_AVANCE).replaceAll("idimple", idImplementacion), "PA_ID");
                }
                
                
                if(id_reg_actividad!=null && !id_reg_actividad.equals("")){
                    //System.out.println("caso dos");
                    Constantes.CRUD_FFM.insertaDB(Constantes.Q_REG_OTS_OF_PE.replaceAll("idact", id_reg_actividad).replaceAll("idorder", nuevasActividades[fila][0]).replaceAll("ottip", nuevasActividades[fila][8]).replaceAll("otsubti", nuevasActividades[fila][9]).replaceAll("negs", unidad_negocio));
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());    
        }
            
    }
}
