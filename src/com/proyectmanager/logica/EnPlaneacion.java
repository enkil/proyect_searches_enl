package com.proyectmanager.logica;

import com.proyectmanager.hilos.HiloPlanData;
import com.proyectmanager.hilos.HiloProyectData;
import com.proyectmanager.hilos.HiloPuntaData;
import com.proyectmanager.inputs.InputConsultaProyectos;

import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.BanerSitio;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Proyecto;
import com.proyectmanager.objects.Punta;
import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputConsultaProyectos;

import com.sforce.soap.partner.QueryResult;

import com.sforce.soap.partner.sobject.SObject;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class EnPlaneacion {
    public EnPlaneacion() {
        super();
    }
    
    
    public OutputConsultaProyectos proyectosEnPlaneacion(InputConsultaProyectos input_ws){
        
        OutputConsultaProyectos output_ws = new OutputConsultaProyectos();
        
        try{
            
            QueryResult pm_proyectos = Constantes.CRUD_SF.query_sf(Constantes.PROYECTOS_PLANING_POR_PM + " " + Variables.getCONDICION_CONSULTAPROYECTOS().replaceAll("id_pm", input_ws.getId_pm()));
            String[][] datos_planeacion = Constantes.CRUD_FFM.consultaBD(Constantes.Q_CONSULTA_DETALLE_ACTIVIDADES.replaceAll("idpm", input_ws.getId_pm()));            
            
            if(pm_proyectos.isDone()){
                if(pm_proyectos.getSize() > 0){
                    HiloProyectData hiloProyecto = new HiloProyectData(pm_proyectos);
                    HiloPuntaData hiloPunta = new HiloPuntaData(pm_proyectos);
                    Map<String,Actividad> map_actividades = new CargaActividades().loadActividades(datos_planeacion);
                    HiloPlanData hiloPlan = new HiloPlanData(pm_proyectos,map_actividades,input_ws.getId_pm());
                    
                    hiloProyecto.start();
                    hiloPunta.start();
                    hiloPlan.start();
                    
                    hiloProyecto.join();
                    hiloPunta.join();
                    hiloPlan.join();
                    
                    //System.out.println(hiloPlan.getMap_csp().size());
                    
                    Map<String,Proyecto> map_proyect = hiloProyecto.getMap_proyect();
                    Map<String,Punta> map_puntas = this.getFullPuntas(hiloPunta.getMapaPuntas(), hiloPlan.getMap_csp());
                    
                    //-- Contadores implementacion
                    Integer gen_num_pendientes = 0;
                    Integer gen_num_confirmado = 0;
                    Integer gen_num_proceso = 0;
                    Integer gen_num_restcate = 0;
                    Integer gen_num_gestoria = 0;
                    Integer gen_num_suspendido = 0;
                    Integer gen_num_completado = 0;
                    Integer gen_num_detenida = 0;
                    Integer gen_num_calendarizado = 0;
                    Integer gen_num_Cancelado = 0;
                    Integer gen_num_otro = 0;
                    Integer gen_num_total = 0;
                    //-- Contadores planeacion
                    Integer gen_puntas_en_tiempo = 0;
                    Integer gen_puntas_en_riesgo = 0;
                    Integer gen_puntas_fuera_tiempo = 0;
                    Integer gen_pendienetes = 0;
                    
                    Integer gen_totalPuntas = 0;
                    Integer gen_totalPuntasPorInstalar = 0;
                    Integer gen_totalPuntasCalendarizado = 0;
                    Integer gen_totalPuntasDetenido = 0;
                    Integer gen_totalPuntasDevueltoVentas = 0;
                    Integer gen_totalPuntasCancelado = 0;
                    Integer gen_totalPuntasInstalado = 0;
                    
                    for(Map.Entry<String,Proyecto> proyecto_entry : map_proyect.entrySet()) {
                        Proyecto proyecto = proyecto_entry.getValue();
                        Map<String,Punta> final_puntas = new HashMap<String,Punta>();
                        ArrayList<Date> listInicialPlDate = new ArrayList<Date>();
                        ArrayList<Date> listFinalPlDate = new ArrayList<Date>();
                        ArrayList<Date> listInicialDate = new ArrayList<Date>();
                        ArrayList<Date> listFinalDate = new ArrayList<Date>();
                        ArrayList<String> listaEstatus = new ArrayList<String>();
                        Float avance_punta = 0.0f;
                        Integer num_pendientes = 0;
                        Integer num_confirmado = 0;
                        Integer num_proceso = 0;
                        Integer num_restcate = 0;
                        Integer num_gestoria = 0;
                        Integer num_suspendido = 0;
                        Integer num_completado = 0;
                        Integer num_detenida = 0;
                        Integer num_calendarizado = 0;
                        Integer num_Cancelado = 0;
                        Integer num_otro = 0;
                        Integer num_total = 0;
                        //-- Contadores planeacion
                        Integer puntas_en_tiempo = 0;
                        Integer puntas_en_riesgo = 0;
                        Integer puntas_fuera_tiempo = 0;
                        Integer pendienetes = 0;
                        String fechaActual = "";
                        String planeacionCerrada = "";
                        String enTiempo = "";
                        
                        Integer totalPuntas = 0;
                        Integer totalPuntasPorInstalar = 0;
                        Integer totalPuntasCalendarizado = 0;
                        Integer totalPuntasDetenido = 0;
                        Integer totalPuntasDevueltoVentas = 0;
                        Integer totalPuntasCancelado = 0;
                        Integer totalPuntasInstalado = 0;
                        
                        for(Map.Entry<String,Punta> punta_entry : map_puntas.entrySet()) {
                            Punta punta = punta_entry.getValue();
                            if(proyecto.getId_cuenta().equals(punta.getId_cuenta())){
                                if(punta.getFechaFinReal()!=null && !punta.getFechaFinReal().equals("")){
                                    listInicialPlDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaInicioPlaneada()));
                                    listFinalPlDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaFinPlaneada()));
                                    listInicialDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaInicioReal()));
                                    listFinalDate.add(Constantes.UTILS.StringToDateMM(punta.getFechaFinReal()));
                                    fechaActual = punta.getFechaActual();
                                }
                                
                                if(punta.getPorcentajeAvance()!= null && !punta.getPorcentajeAvance().equals("")){
                                    avance_punta = avance_punta + Float.valueOf(punta.getPorcentajeAvance());
                                }
                                
                                num_pendientes += Integer.valueOf(punta.getOS_pendientes());
                                num_confirmado += Integer.valueOf(punta.getNum_confirmado());
                                num_proceso += Integer.valueOf(punta.getNum_proceso());
                                num_restcate += Integer.valueOf(punta.getNum_restcate());
                                num_gestoria += Integer.valueOf(punta.getNum_gestoria());
                                num_suspendido += Integer.valueOf(punta.getNum_suspendido());
                                num_completado += Integer.valueOf(punta.getNum_completado());
                                num_detenida += Integer.valueOf(punta.getNum_detenida());
                                num_calendarizado += Integer.valueOf(punta.getNum_calendarizado());
                                num_Cancelado += Integer.valueOf(punta.getNum_Cancelado());
                                num_otro += Integer.valueOf(punta.getNum_otro());
                                //-- Contadores implementacion
                                puntas_en_tiempo += Integer.valueOf(punta.getNum_en_tiempo());
                                puntas_en_riesgo += Integer.valueOf(punta.getNum_en_riesgo());
                                puntas_fuera_tiempo += Integer.valueOf(punta.getNum_fuera_tiempo());
                                pendienetes += Integer.valueOf(punta.getNum_pendientes());
                                
                                
                                totalPuntas += Integer.valueOf(punta.getTotalPuntas());
                                totalPuntasPorInstalar += Integer.valueOf(punta.getTotalPuntasPorInstalar());
                                totalPuntasCalendarizado += Integer.valueOf(punta.getTotalPuntasCalendarizado());
                                totalPuntasDetenido += Integer.valueOf(punta.getTotalPuntasDetenido());
                                totalPuntasDevueltoVentas += Integer.valueOf(punta.getTotalPuntasDevueltoVentas());
                                totalPuntasCancelado += Integer.valueOf(punta.getTotalPuntasCancelado());
                                totalPuntasInstalado += Integer.valueOf(punta.getTotalPuntasInstalado());
                                
                                planeacionCerrada = punta.getEstatusPlaneacion();
                                enTiempo = punta.getEnTiempo();
                                
                                final_puntas.put(punta.getId_cot_sitio(), punta);
                            }
                        }
                        
                        num_total = num_pendientes + num_confirmado + num_proceso + num_restcate + num_gestoria + num_suspendido + num_completado + num_detenida + num_calendarizado + num_Cancelado + num_otro; 
                        proyecto.setOS_pendientes(num_pendientes.toString());
                        proyecto.setNum_confirmado(num_confirmado.toString());
                        proyecto.setNum_proceso(num_proceso.toString());
                        proyecto.setNum_restcate(num_restcate.toString());
                        proyecto.setNum_gestoria(num_gestoria.toString());
                        proyecto.setNum_suspendido(num_suspendido.toString());
                        proyecto.setNum_completado(num_completado.toString());
                        proyecto.setNum_detenida(num_detenida.toString());
                        proyecto.setNum_calendarizado(num_calendarizado.toString());
                        proyecto.setNum_Cancelado(num_Cancelado.toString());
                        proyecto.setNum_otro(num_otro.toString());
                        proyecto.setNum_total(num_total.toString());
                        
                        proyecto.setTotalPuntas(totalPuntas.toString());
                        proyecto.setTotalPuntasPorInstalar(totalPuntasPorInstalar.toString());
                        proyecto.setTotalPuntasCalendarizado(totalPuntasCalendarizado.toString());
                        proyecto.setTotalPuntasDetenido(totalPuntasDetenido.toString());
                        proyecto.setTotalPuntasDevueltoVentas(totalPuntasDevueltoVentas.toString());
                        proyecto.setTotalPuntasCancelado(totalPuntasCancelado.toString());
                        proyecto.setTotalPuntasInstalado(totalPuntasInstalado.toString());
                        //-- Contadores planeacion
                        proyecto.setNum_fuera_tiempo(puntas_en_tiempo.toString());
                        proyecto.setNum_en_riesgo(puntas_en_riesgo.toString());
                        proyecto.setNum_en_tiempo(puntas_fuera_tiempo.toString());
                        proyecto.setNum_pendientes(pendienetes.toString());
                        proyecto.setFechaInicioPlaneada(Constantes.UTILS.minDate(listInicialPlDate));
                        proyecto.setFechaFinPlaneada(Constantes.UTILS.maxDate(listFinalPlDate));
                        proyecto.setFechaInicioReal(Constantes.UTILS.minDate(listInicialDate));
                        proyecto.setFechaFinReal(Constantes.UTILS.maxDate(listFinalDate));
                        proyecto.setPorcentajeAvance(avance_punta.equals(0.0) ? "0.0" : Constantes.UTILS.valorDosDecimales(String.valueOf(avance_punta/final_puntas.size())));
                        proyecto.setPorcentajeEsperado(!proyecto.getFechaFinReal().equals("") && proyecto.getFechaFinReal()!=null ? Constantes.UTILS.getPorcentajeEsperado(proyecto.getFechaInicioReal(), proyecto.getFechaFinReal(),fechaActual) : "");
                        proyecto.setSemaforo(!proyecto.getFechaFinReal().equals("") && proyecto.getFechaFinReal()!=null ? Constantes.UTILS.getSemaforoEnPlaneacion(proyecto.getPorcentajeEsperado(), proyecto.getPorcentajeAvance(),planeacionCerrada,Constantes.UTILS.getEnTiempo(proyecto.getFechaFinPlaneada(), proyecto.getFechaFinReal())) : "");
                        proyecto.setPuntas(final_puntas.values().toArray(new Punta[final_puntas.size()]));
                                                
                        gen_num_pendientes += Integer.valueOf(proyecto.getOS_pendientes());
                        gen_num_confirmado += Integer.valueOf(proyecto.getNum_confirmado());
                        gen_num_proceso += Integer.valueOf(proyecto.getNum_proceso());
                        gen_num_restcate += Integer.valueOf(proyecto.getNum_restcate());
                        gen_num_gestoria += Integer.valueOf(proyecto.getNum_gestoria());
                        gen_num_suspendido += Integer.valueOf(proyecto.getNum_suspendido());
                        gen_num_completado += Integer.valueOf(proyecto.getNum_completado());
                        gen_num_detenida += Integer.valueOf(proyecto.getNum_detenida());
                        gen_num_calendarizado += Integer.valueOf(proyecto.getNum_calendarizado());
                        gen_num_Cancelado += Integer.valueOf(proyecto.getNum_Cancelado());
                        gen_num_otro += Integer.valueOf(proyecto.getNum_otro());
                        gen_num_total += Integer.valueOf(proyecto.getNum_total());
                        
                        gen_totalPuntas += Integer.valueOf(proyecto.getTotalPuntas());
                        gen_totalPuntasPorInstalar += Integer.valueOf(proyecto.getTotalPuntasPorInstalar());
                        gen_totalPuntasCalendarizado += Integer.valueOf(proyecto.getTotalPuntasCalendarizado());
                        gen_totalPuntasDetenido += Integer.valueOf(proyecto.getTotalPuntasDetenido());
                        gen_totalPuntasDevueltoVentas += Integer.valueOf(proyecto.getTotalPuntasDevueltoVentas());
                        gen_totalPuntasCancelado += Integer.valueOf(proyecto.getTotalPuntasCancelado());
                        gen_totalPuntasInstalado += Integer.valueOf(proyecto.getTotalPuntasInstalado());
                        
                        gen_puntas_en_tiempo += Integer.valueOf(proyecto.getNum_en_tiempo());
                        gen_puntas_en_riesgo += Integer.valueOf(proyecto.getNum_en_riesgo());
                        gen_puntas_fuera_tiempo += Integer.valueOf(proyecto.getNum_fuera_tiempo());
                        gen_pendienetes += Integer.valueOf(proyecto.getNum_pendientes());
                    }
                    
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion(Constantes.RESULTSESCRIPTION_SUSSES);
                    output_ws.setVersion(Constantes.WS_VERSION);
                    // -- Contadores Implementacion
                    output_ws.setNum_total_clientes(String.valueOf(map_proyect.size()));
                    output_ws.setNum_total_Puntas(String.valueOf(map_puntas.size()));
                    output_ws.setOS_pendientes(gen_num_pendientes.toString());
                    output_ws.setNum_confirmado(gen_num_confirmado.toString());
                    output_ws.setNum_proceso(gen_num_proceso.toString());
                    output_ws.setNum_restcate(gen_num_restcate.toString());
                    output_ws.setNum_gestoria(gen_num_gestoria.toString());
                    output_ws.setNum_suspendido(gen_num_suspendido.toString());
                    output_ws.setNum_completado(gen_num_completado.toString());
                    output_ws.setNum_detenida(gen_num_detenida.toString());
                    output_ws.setNum_calendarizado(gen_num_calendarizado.toString());
                    output_ws.setNum_Cancelado(gen_num_Cancelado.toString());
                    output_ws.setNum_otro(gen_num_otro.toString());
                    output_ws.setNum_total(gen_num_total.toString());
                    //-- Contadores planeacion
                    
                    output_ws.setTotalPuntas(gen_totalPuntas.toString());
                    output_ws.setTotalPuntasPorInstalar(gen_totalPuntasPorInstalar.toString());
                    output_ws.setTotalPuntasCalendarizado(gen_totalPuntasCalendarizado.toString());
                    output_ws.setTotalPuntasDetenido(gen_totalPuntasDetenido.toString());
                    output_ws.setTotalPuntasDevueltoVentas(gen_totalPuntasDevueltoVentas.toString());
                    output_ws.setTotalPuntasCancelado(gen_totalPuntasCancelado.toString());
                    output_ws.setTotalPuntasInstalado(gen_totalPuntasInstalado.toString());
                    
                    output_ws.setNum_fuera_tiempo(gen_puntas_en_tiempo.toString());
                    output_ws.setNum_en_riesgo(gen_puntas_en_riesgo.toString());
                    output_ws.setNum_en_tiempo(gen_puntas_fuera_tiempo.toString());
                    output_ws.setNum_pendientes(gen_pendienetes.toString());
                    
                    output_ws.setProyectos(map_proyect.values().toArray(new Proyecto[map_proyect.size()]));
                    
                    
                }else{
                    System.out.println("Sin informacion");    
                }
                
            }else{
                System.err.println("Error al ejecutar consulta planeacion : " + pm_proyectos.getQueryLocator());    
            }
        
        }catch(Exception ex){
            System.err.println("Error: " + ex.toString());
        }catch(Throwable tx){
            System.err.println("Error : " + tx.toString());
        }
        
        return output_ws;
        
    }
    
    
    
    public Map<String,Punta> getFullPuntas(Map<String,Punta> map_puntas,Map<String,CotSitioPlan> map_csp){
        
        Map<String,Punta> fullPuntas = new HashMap<String,Punta>();
        
        try{
            
            Iterator it_csp = map_csp.entrySet().iterator();
            Map<String,CotSitioPlan> map_planes = null;
            
            for (Map.Entry<String,Punta> entry : map_puntas.entrySet()) {
                Punta punta = entry.getValue();
                map_planes = new HashMap<String,CotSitioPlan>();
                ArrayList<Date> listInicialPlDate = new ArrayList<Date>();
                ArrayList<Date> listFinalPlDate = new ArrayList<Date>();
                ArrayList<Date> listInicialDate = new ArrayList<Date>();
                ArrayList<Date> listFinalDate = new ArrayList<Date>();
                ArrayList<String> listaEstatus = new ArrayList<String>();
                ArrayList<String> listaEstatusCsp = new ArrayList<String>();
                ArrayList<String> listSemaforo = new ArrayList<String>();
                Float avance_punta = 0.0f;
                String fechaActual = "";
                String planeacionCerrada = "";
                String enTiempo = "";
                BanerSitio informacionContrato = new BanerSitio();
                while (it_csp.hasNext()) {
                    Map.Entry csp_entry = (Map.Entry) it_csp.next();
                    CotSitioPlan csp = (CotSitioPlan) csp_entry.getValue();
                    if(csp.getId_cot_sitio().equals(punta.getId_cot_sitio())){
                        if(csp.getFechaFinReal()!= null && !csp.getFechaFinReal().equals("")){
                            listInicialPlDate.add(Constantes.UTILS.StringToDateMM(csp.getFechaInicioPlaneada()));
                            listFinalPlDate.add(Constantes.UTILS.StringToDateMM(csp.getFechaFinPlaneada()));
                            listInicialDate.add(Constantes.UTILS.StringToDateMM(csp.getFechaInicioReal()));
                            listFinalDate.add(Constantes.UTILS.StringToDateMM(csp.getFechaFinReal()));
                            fechaActual = csp.getFechaActual();
                        }
                        if(csp.getPorcentajeAvance()!= null){
                            avance_punta = avance_punta + Float.valueOf(csp.getPorcentajeAvance());
                        }
                        
                        listaEstatus.add(csp.getStatus_OS());
                        listaEstatusCsp.add(csp.getStatusCsp());
                        listSemaforo.add(csp.getSemaforo());
                        
                        planeacionCerrada = csp.getEstatusPlaneacion();
                        enTiempo = csp.getEnTiempo();
                        
                        informacionContrato.setFolioCotSitio(csp.getFolioCotSitio());
                        informacionContrato.setDireccionSitio(csp.getDireccionSitio());
                        informacionContrato.setPlaza(csp.getPlaza());
                        informacionContrato.setSubtotalRenta(csp.getSubtotalRenta());
                        informacionContrato.setTotalRentaConImpuesto(csp.getTotalRentaConImpuesto());
                        informacionContrato.setTipoOportunidad(csp.getTipoOportunidad());
                        informacionContrato.setFolioCotizacion(csp.getFolioCotizacion());
                        informacionContrato.setNombreSitio(csp.getNombreSitio());
                        informacionContrato.setFolioSitio(csp.getFolioSitio());
                        informacionContrato.setTipoCobertura(csp.getTipoCobertura());
                        informacionContrato.setLatitude(csp.getLatitude());
                        informacionContrato.setLongitude(csp.getLongitude());
                        
                        map_planes.put(csp.getFolio_CSP(), csp);
                    }
                }
                
                it_csp = map_csp.entrySet().iterator();
                Map<String,Integer> mapContadores = Constantes.UTILS.getContadoresImplementacion(listaEstatus);
                Map<String,String> mapContStatusCsp = Constantes.UTILS.getContadoresEstatusCsp(listaEstatusCsp);
                //-- Contadores Implementacion 
                punta.setOS_pendientes(mapContadores.get(Constantes.OS_STATUS_PENDIENTE).toString());
                punta.setNum_confirmado(mapContadores.get(Constantes.OS_STATUS_CONFIRMADO).toString());
                punta.setNum_proceso(mapContadores.get(Constantes.OS_STATUS_PROCESO).toString());
                punta.setNum_restcate(mapContadores.get(Constantes.OS_STATUS_RESCATE).toString());
                punta.setNum_gestoria(mapContadores.get(Constantes.OS_STATUS_GESTORIA).toString());
                punta.setNum_suspendido(mapContadores.get(Constantes.OS_STATUS_SUSPENDIDO).toString());
                punta.setNum_completado(mapContadores.get(Constantes.OS_STATUS_COMPLETADO).toString());
                punta.setNum_detenida(mapContadores.get(Constantes.OS_STATUS_DETENIDA).toString());
                punta.setNum_calendarizado(mapContadores.get(Constantes.OS_STATUS_CALENDARIZADO).toString());
                punta.setNum_Cancelado(mapContadores.get(Constantes.OS_STATUS_CANCELADO).toString());
                punta.setNum_otro(mapContadores.get(Constantes.OS_STATUS_OTRO).toString());
                
                punta.setTotalPuntas(mapContStatusCsp.get("Total"));
                punta.setTotalPuntasPorInstalar(mapContStatusCsp.get(Constantes.C_POR_INSTALAR));
                punta.setTotalPuntasCalendarizado(mapContStatusCsp.get(Constantes.C_CALENDARIZADO));
                punta.setTotalPuntasDetenido(mapContStatusCsp.get(Constantes.C_DETENIDO));
                punta.setTotalPuntasDevueltoVentas(mapContStatusCsp.get(Constantes.C_DEVUELTO_VENTAS));
                punta.setTotalPuntasCancelado(mapContStatusCsp.get(Constantes.C_CANCELADO));
                punta.setTotalPuntasInstalado(mapContStatusCsp.get(Constantes.C_INSTALADO));
                
                punta.setNum_total(String.valueOf(mapContadores.size()));
                punta.setFechaActual(fechaActual);
                punta.setFechaInicioPlaneada(Constantes.UTILS.minDate(listInicialPlDate));
                punta.setFechaFinPlaneada(Constantes.UTILS.maxDate(listFinalPlDate));
                punta.setFechaInicioReal(Constantes.UTILS.minDate(listInicialDate));
                punta.setFechaFinReal(Constantes.UTILS.maxDate(listFinalDate));
                punta.setPorcentajeAvance(avance_punta.equals(0.0f) ? "0.0" : Constantes.UTILS.valorDosDecimales(String.valueOf(avance_punta/map_planes.size())));
                punta.setPorcentajeEsperado(Constantes.UTILS.getPorcentajeEsperado(punta.getFechaInicioReal(), punta.getFechaFinReal(),fechaActual));
                punta.setSemaforo(Constantes.UTILS.getSemaforoEnPlaneacion(punta.getPorcentajeEsperado(), punta.getPorcentajeAvance(),planeacionCerrada,Constantes.UTILS.getEnTiempo(punta.getFechaFinPlaneada(), punta.getFechaFinReal())));
                punta.setPlanes(map_planes.values().toArray(new CotSitioPlan[map_planes.size()]));     
                punta.setEstatusPlaneacion(planeacionCerrada);
                punta.setEnTiempo(enTiempo);
                punta.setInformacionContrato(informacionContrato);
                //-- Contadores de planeacion
                
                Map<String,Integer> mapContadorPlaneacion = Constantes.UTILS.getContadoresPlaneacion(listSemaforo);
                punta.setNum_fuera_tiempo(mapContadorPlaneacion.get(Constantes.C_FUERATIEMPO).toString());
                punta.setNum_en_riesgo(mapContadorPlaneacion.get(Constantes.C_ENRIESGO).toString());
                punta.setNum_en_tiempo(mapContadorPlaneacion.get(Constantes.C_ENTIEMPO).toString());
                punta.setNum_pendientes(mapContadorPlaneacion.get(Constantes.C_PENDIENTES).toString());
                
                fullPuntas.put(punta.getId_cot_sitio(), punta);
            }
        
        }catch(Exception ex){
            System.err.println("Error : puntas " + ex.toString());    
        }
        
        return fullPuntas;
            
    }
}
