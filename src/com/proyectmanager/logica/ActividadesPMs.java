package com.proyectmanager.logica;

import com.proyectmanager.objects.Actividad;
import com.proyectmanager.objects.Constantes;

import java.util.HashMap;
import java.util.Map;

public class ActividadesPMs{
    
    public ActividadesPMs() {
        super();
    }
    
    public Map<String,Actividad> loadMapActividades(String id_bridge_sf){
        
        Map<String,Actividad> map_actividades = new HashMap<String,Actividad>();
        
        try{
            
            System.out.println("consultando actividades : " + Constantes.Q_CONSULTA_ACTIVIDADES_IMP.replaceAll("idcspsf", id_bridge_sf));
            
            String matriz_actividatdes[][] = Constantes.CRUD_FFM.consultaBD(Constantes.Q_CONSULTA_ACTIVIDADES_IMP.replaceAll("idcspsf", id_bridge_sf));
            
            for(int fila = 0;fila < matriz_actividatdes.length; fila++){
                Actividad actividad = new Actividad();
                
                actividad.setId_actividad(matriz_actividatdes[fila][0]);
                actividad.setNombre_actividad(matriz_actividatdes[fila][1]);
                actividad.setNombre_responsable(matriz_actividatdes[fila][2] != null ? matriz_actividatdes[fila][2] : "");
                actividad.setFecha_inicio_planeada(matriz_actividatdes[fila][3] != null ? matriz_actividatdes[fila][3] : "");
                actividad.setFecha_fin_planeada(matriz_actividatdes[fila][4] != null ? matriz_actividatdes[fila][4] : "");
                actividad.setFecha_inicio_real(matriz_actividatdes[fila][5] != null ? matriz_actividatdes[fila][5] : "");
                actividad.setFecha_fin_real(matriz_actividatdes[fila][6] != null ? matriz_actividatdes[fila][6] : "");
                actividad.setHoy(matriz_actividatdes[fila][7] != null ? matriz_actividatdes[fila][7] : "");                          
                actividad.setPorcentaje(matriz_actividatdes[fila][8] != null ? matriz_actividatdes[fila][8] : "");
                actividad.setId_dependencia(matriz_actividatdes[fila][9] != null ? matriz_actividatdes[fila][9] :"");
                actividad.setTiene_Ot(matriz_actividatdes[fila][10]);
                actividad.setPorcentajeEsperado(!actividad.getFecha_fin_real().equals("") ? Constantes.UTILS.getPorcentajeEsperado(actividad.getFecha_inicio_real(),actividad.getFecha_fin_real(),actividad.getHoy()) : "");
                actividad.setSemaforo(!actividad.getFecha_fin_real().equals("") ? Constantes.UTILS.getSemaforoEnPlaneacion(actividad.getPorcentajeEsperado(), actividad.getPorcentaje(),matriz_actividatdes[fila][18],matriz_actividatdes[fila][19]) : "");
                actividad.setSe_puede_eliminar(matriz_actividatdes[fila][12]);
                actividad.setId_responsable(matriz_actividatdes[fila][13]);
                actividad.setTipo_intervencion(matriz_actividatdes[fila][14]);
                actividad.setSubtipo_intervencion(matriz_actividatdes[fila][15]);
                actividad.setUnidad_negocio(matriz_actividatdes[fila][16]);
                actividad.setId_tipo_actividad(matriz_actividatdes[fila][17]);
                actividad.setPlaneacionCerrada(matriz_actividatdes[fila][18]);
                actividad.setEnTiempo(matriz_actividatdes[fila][19]);
                actividad.setId_OT(matriz_actividatdes[fila][20]);
                actividad.setId_implementacion(matriz_actividatdes[fila][21]);
                map_actividades.put(matriz_actividatdes[fila][0], actividad);
            } 
            
        }catch(Exception ex){
            System.err.println("Error al cargar mapa de actividades : " + ex.toString());
        } 
        
        return map_actividades;
    }
}
