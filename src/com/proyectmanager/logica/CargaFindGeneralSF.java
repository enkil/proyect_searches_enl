package com.proyectmanager.logica;

import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.CotSitio;
import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Cotizacion;
import com.proyectmanager.objects.Cuenta;
import com.proyectmanager.objects.CuentaFactura;
import com.proyectmanager.objects.Oportunidad;
import com.proyectmanager.objects.Os;
import com.proyectmanager.objects.Ticket;

import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;

public class CargaFindGeneralSF {
    public CargaFindGeneralSF() {
        super();
    }
    
    public static Cuenta[] cargaInformacionCuentas(ArrayList<SObject> arrListCuenta){
        
        Cuenta[] arrCuenta = new Cuenta[arrListCuenta.size()];
        
        try{
            Integer contador = 0;
            for(SObject cuenta : arrListCuenta){
                Cuenta newCuenta = new Cuenta();
                newCuenta.setId((String) cuenta.getField("Id"));
                newCuenta.setNombre((String) cuenta.getField("Name"));
                newCuenta.setRazonSocial((String) cuenta.getField("RazonSocial__c"));
                newCuenta.setFolioCuenta((String) cuenta.getField("FolioCuenta__c"));
                newCuenta.setKeyObject(Constantes.TYPE_CUENTA);
                arrCuenta[contador++] = newCuenta;
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }        
        
        return arrCuenta;
    }
    
    public static Oportunidad[] cargaInformacionOportunidad(ArrayList<SObject> arrListOportunidad){
        
        Oportunidad[] arrOportunidad = new Oportunidad[arrListOportunidad.size()];
        
        try{
            Integer contador = 0;
            for(SObject oportunidad : arrListOportunidad){
                Oportunidad newOportunidad = new Oportunidad();
                newOportunidad.setId((String) oportunidad.getField("Id"));
                newOportunidad.setNombre((String) oportunidad.getField("Name"));
                newOportunidad.setNumeroOportunidad((String) oportunidad.getField("NumeroOportunidad__c"));
                newOportunidad.setEtapa((String) oportunidad.getField("StageName"));
                newOportunidad.setKeyObject(Constantes.TYPE_OPORTUNIDAD);
                arrOportunidad[contador++] = newOportunidad;
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }            
        
        return arrOportunidad;
    }
    
    public static Cotizacion[] cargaInformacionCotizacion(ArrayList<SObject> arrListCotizacion){
        
        Cotizacion[] arrCotizacion = new Cotizacion[arrListCotizacion.size()];
        
        try{
            Integer contador = 0;
            for(SObject cotizacion : arrListCotizacion){
                Cotizacion newCotizacion = new Cotizacion();
                newCotizacion.setId((String) cotizacion.getField("Id"));
                newCotizacion.setNombre((String) cotizacion.getField("Name"));
                newCotizacion.setEstatus((String) cotizacion.getField("Estatus__c"));
                newCotizacion.setVigenciaCotizacion((String) cotizacion.getField("VigenciaCotizacion__c"));
                newCotizacion.setKeyObject(Constantes.TYPE_COTIZACION);
                arrCotizacion[contador++] = newCotizacion;
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }        
        
        return arrCotizacion;
    }
    
    public static CotSitio[] cargaInformacionCotSitio(ArrayList<SObject> arrListCotSitio){
        
        CotSitio[] arrCotSitio = new CotSitio[arrListCotSitio.size()];
        
        try{
            Integer contador = 0;
            for(SObject cotSitio : arrListCotSitio){
                CotSitio newCotSitio = new CotSitio();
                newCotSitio.setId((String) cotSitio.getField("Id"));
                newCotSitio.setNombre((String) cotSitio.getField("Name"));
                newCotSitio.setPlaza((String) cotSitio.getField("Plaza__c"));
                newCotSitio.setDireccionSitio((String) cotSitio.getField("DireccionSitio__c"));
                newCotSitio.setKeyObject(Constantes.TYPE_COTSITIO);
                arrCotSitio[contador++] = newCotSitio;
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }        
        
        return arrCotSitio;
    }
    
    public static CotSitioPlan[] cargaInformacionCotSitioPlan(ArrayList<SObject> arrListCotSitioPlan){
        
        CotSitioPlan[] arrCotSitioPlan = new CotSitioPlan[arrListCotSitioPlan.size()];
        
        try{
            Integer contador = 0;
            for(SObject cotSitioPlan : arrListCotSitioPlan){
                CotSitioPlan newCotSitioPlan = new CotSitioPlan();
                newCotSitioPlan.setId((String) cotSitioPlan.getField("Id"));
                newCotSitioPlan.setNombre((String) cotSitioPlan.getField("Name"));
                newCotSitioPlan.setNombrePlan((String) cotSitioPlan.getField("Dp_Familia__c"));
                newCotSitioPlan.setAccesoPrincipal((String) cotSitioPlan.getField("AcessoPrincipal__c"));
                newCotSitioPlan.setKeyObject(Constantes.TYPE_COTSITIOPLAN);
                arrCotSitioPlan[contador++] = newCotSitioPlan;
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }        
        
        return arrCotSitioPlan;
    }
    
    public static CuentaFactura[] cargaInformacionCuentaFactura(ArrayList<SObject> arrListCuentaFactura){
        CuentaFactura[] arrCuentaFactura = new CuentaFactura[arrListCuentaFactura.size()];
        try{
            Integer contador = 0;
            for(SObject cuentaFactura : arrListCuentaFactura){
                CuentaFactura newCuentaFactura = new CuentaFactura();
                newCuentaFactura.setId((String) cuentaFactura.getField("Id"));
                newCuentaFactura.setNombre((String) cuentaFactura.getField("Name"));
                newCuentaFactura.setNumCuentaFactura((String) cuentaFactura.getField("IdCuentaBRM__c"));
                newCuentaFactura.setNombreResponsableComercial((String) cuentaFactura.getField("ResponsableCuentaComercial__c"));
                newCuentaFactura.setKeyObject(Constantes.TYPE_CUENTAFACTURA);
                arrCuentaFactura[contador++] = newCuentaFactura;
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar datos de cuenta : " + ex.toString());   
        }        
        
        return arrCuentaFactura;
    }
    
    public static Os[] cargaInformacionOs(ArrayList<SObject> arrListOs){
        Os[] arrOss = new Os[arrListOs.size()];
        try{
            Integer contador = 0;
            for(SObject os : arrListOs){
                Os newOs = new Os();
                newOs.setId((String) os.getField("Id"));
                newOs.setNombre((String) os.getField("Name"));
                newOs.setEstatus((String) os.getField("Estatus__c"));
                newOs.setNumeroCuentaFactura((String) os.getField("NumeroCuenta__c"));
                newOs.setKeyObject(Constantes.TYPE_OS);
                arrOss[contador++] = newOs;
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar datos de OS : " + ex.toString());   
        }        
        
        return arrOss;
    }
    
    public static Ticket[] cargaInformacionTicket(ArrayList<SObject> arrListTk){
        Ticket[] arrTks = new Ticket[arrListTk.size()];
        try{
            Integer contador = 0;
            for(SObject os : arrListTk){
                Ticket newTk = new Ticket();
                newTk.setId((String) os.getField("Id"));
                newTk.setCaseNumber((String) os.getField("CaseNumber"));
                newTk.setNivel1((String) os.getField("Nivel1__c"));
                newTk.setStatus((String) os.getField("Status"));
                newTk.setKeyObject(Constantes.TYPE_TK);
                arrTks[contador++] = newTk;
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar datos de OS : " + ex.toString());   
        }        
        
        return arrTks;
    }
}
