package com.proyectmanager.logica;

import com.proyectmanager.objects.Actividad;

import java.util.HashMap;
import java.util.Map;

public class CargaActividades{
    
    public CargaActividades() {
        super();
    }
    
    
    public Map<String,Actividad> loadActividades(String actividades[][]){
        
        Map<String,Actividad> map_actividades = new HashMap<String,Actividad>();
        
        try{
            
            for(int fila=0; fila < actividades.length; fila++){
                Actividad activ = new Actividad();
                activ.setFecha_inicio_planeada(actividades[fila][1]);
                activ.setFecha_fin_planeada(actividades[fila][2]);
                activ.setFecha_inicio_real(actividades[fila][3]);
                activ.setFecha_fin_real(actividades[fila][4]);
                activ.setPorcentaje(actividades[fila][5]);
                activ.setFecha_creacion(actividades[fila][6]);
                activ.setFechaActual(actividades[fila][7]);
                activ.setHorasFavor(actividades[fila][8]);
                activ.setPlaneacionCerrada(actividades[fila][9]);
                activ.setPlanTermindo(actividades[fila][11]);
                activ.setIdDetallePlaneacion(actividades[fila][10]);
                activ.setEstatusPlaneacion(actividades[fila][11]);
                activ.setEnTiempo(actividades[fila][12]);
                map_actividades.put(actividades[fila][0], activ);
            }
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
        }
        
        return map_actividades;
            
    }

}
