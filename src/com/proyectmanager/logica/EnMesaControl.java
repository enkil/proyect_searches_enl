package com.proyectmanager.logica;

import com.proyectmanager.inputs.InputConsultaProyectos;
import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.Proyecto;
import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputConsultaProyectos;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import javax.ws.rs.core.Response;

public class EnMesaControl {
    public EnMesaControl() {
        super();
    }
    
    public OutputConsultaProyectos proyectosEnMesaControl(InputConsultaProyectos input_ws){
        
        OutputConsultaProyectos output_ws = new OutputConsultaProyectos();
        
        try{
            QueryResult proyectos_sf = Constantes.CRUD_SF.query_sf(Variables.getQR_BANDEJA_MESA_CONTROL().replaceAll("Id_pm", input_ws.getId_pm()));
            
            if(proyectos_sf.getDone()){
                int total_puntas = 0;
                int puntas_en_tiempo = 0;
                int puntas_en_riesgo = 0;
                int puntas_fuera_tiempo = 0;
                int contador = 0;
                Proyecto [] arr_proyectos = new Proyecto[proyectos_sf.getSize()];
                for(SObject proyecto : proyectos_sf.getRecords()){
                    Proyecto obj_proyecto = new Proyecto();
                    obj_proyecto.setId_cotizacion((String) proyecto.getField("Id"));    
                    obj_proyecto.setFolio_cotizacion((String) proyecto.getField("Name"));
                    
                    if(proyecto.getSObjectField("Oportunidad__r") != null && proyecto.getSObjectField("Oportunidad__r").getClass().equals(SObject.class)){
                        SObject oportunidad = (SObject) proyecto.getSObjectField("Oportunidad__r"); 
                            if (oportunidad != null) {
                                obj_proyecto.setFecha_creacion((String) oportunidad.getField("CreatedDate"));
                                obj_proyecto.setTiempo_mesa(Constantes.UTILS.tiempoTranscurrido((String) oportunidad.getField("CreatedDate")));
                                obj_proyecto.setSemaforo(Constantes.UTILS.getSemaforoMesa((String) oportunidad.getField("CreatedDate")));
                                if(Constantes.UTILS.getSemaforoMesa((String) oportunidad.getField("CreatedDate")).equals(Variables.getCOLOR_ENTIEMPO())){
                                    ++puntas_en_tiempo;
                                }else if(Constantes.UTILS.getSemaforoMesa((String) oportunidad.getField("CreatedDate")).equals(Variables.getCOLOR_RIESGO())){
                                    ++puntas_en_riesgo;
                                }else if(Constantes.UTILS.getSemaforoMesa((String) oportunidad.getField("CreatedDate")).equals(Variables.getCOLOR_FUERA_TIEMPO())){
                                    ++puntas_fuera_tiempo;
                                }
                                if(oportunidad.getSObjectField("Account") != null && oportunidad.getSObjectField("Account").getClass().equals(SObject.class)){
                                    SObject cuenta = (SObject) oportunidad.getSObjectField("Account"); 
                                        if (cuenta != null) {
                                            obj_proyecto.setNombre_cliente((String) cuenta.getField("RazonSocial__c"));
                                            if(cuenta.getSObjectField("ContactoPrincipal__r") != null && cuenta.getSObjectField("ContactoPrincipal__r").getClass().equals(SObject.class)){
                                                SObject contacto = (SObject) cuenta.getSObjectField("ContactoPrincipal__r"); 
                                                    if (contacto != null) {
                                                        obj_proyecto.setNombre_Contacto((String) contacto.getField("Name"));
                                                        obj_proyecto.setTelefono_contacto((String) contacto.getField("MobilePhone"));
                                                    }
                                            }             
                                        }
                                } 
                            }
                    }
                    ++total_puntas;
                    arr_proyectos[contador++] = obj_proyecto;
                    
                }
                
                output_ws.setResult(Constantes.RESULT_SUSSES);
                output_ws.setResultDescripcion("Informacion de proyectos");
                output_ws.setVersion(Constantes.WS_VERSION);
                output_ws.setTotal_proyectos(String.valueOf(total_puntas));
                output_ws.setNum_en_tiempo(String.valueOf(puntas_en_tiempo));
                output_ws.setNum_en_riesgo(String.valueOf(puntas_en_riesgo));
                output_ws.setNum_fuera_tiempo(String.valueOf(puntas_fuera_tiempo));
                output_ws.setProyectos(arr_proyectos);
                
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("NO SE EJECUTO LA CONSULTA : " + proyectos_sf.getQueryLocator());
                output_ws.setVersion(Constantes.WS_VERSION);
            }
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR BANDEJA MESA : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        System.out.println(Constantes.UTILS.classTostrJson(output_ws));
        
        return output_ws;    
    }
}
