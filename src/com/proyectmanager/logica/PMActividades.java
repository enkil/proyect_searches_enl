package com.proyectmanager.logica;

import com.proyectmanager.hilos.HiloActividadesPE;
import com.proyectmanager.hilos.HiloActividadesPIEnl;
import com.proyectmanager.hilos.HiloActividadesPIRec;
import com.proyectmanager.inputs.InputConsultaActividades;
import com.proyectmanager.objects.Actividad;

import com.proyectmanager.objects.Constantes;

import com.proyectmanager.objects.CotSitioPlan;

import com.proyectmanager.objects.DetalleAvances;
import com.proyectmanager.objects.OrtdenTrabajo;

import com.proyectmanager.objects.Variables;

import com.proyectmanager.outputs.OutputConsultaActividades;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class PMActividades {
    public PMActividades() {
        super();
        Constantes.UTILS.loadConfigIN_DB();
    }
    
    /*public Map<String,Actividad> procesoPrincipal(InputConsultaActividades input_ws,Map<String,CotSitioPlan> map_csp_sf){
        Map<String,Actividad> map_actividades = new HashMap<String,Actividad>();
        try{
            
            System.out.println(" ===== Cargando OSs");
            String OSs = this.getOSsPunta(map_csp_sf);
            System.out.println(" ===== Cargando actividades existentes");
            map_actividades = this.loadActivities(input_ws,map_csp_sf,OSs);
                        
            System.out.println(" ===== Cargando nuevas actividades PE");
            new HiloActividadesPE(OSs,map_actividades,input_ws.getId_cs()).start();
            System.out.println(" ===== Cargando nuevas actividades Empresarial");
            new HiloActividadesPIEnl(OSs,map_actividades,input_ws.getId_cs()).start();
            System.out.println(" ===== Cargando nuevas actividades Recidencial");
            new HiloActividadesPIRec(OSs,map_actividades,input_ws.getId_cs()).start();
            
            System.out.println(" ===== Actualizando lista de actividades");
            map_actividades = this.loadActivities(input_ws,map_csp_sf,OSs);
            
            System.out.println(" ===== Conciliando el proyecto de PMS");
            this.conciliaProyecto(input_ws.getId_cs(),map_csp_sf);
                                             
        }catch(Exception ex){
            System.err.println("Error en logica principal : " + ex.toString());    
        }
        
        return map_actividades;
        
    }
    
    
    public Map<String,Actividad> loadActivities(InputConsultaActividades input_ws,Map<String,CotSitioPlan> mapCsp,String OSs){
        
        Map<String,Actividad> map_actividades = new HashMap<String,Actividad>();
        
        try{
            
            String matriz_actividatdes [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_ACIVIDADES_PMO.replaceAll("id_cot_sitio", input_ws.getId_cs()));
            
            if(matriz_actividatdes.length > 0){
                map_actividades = this.loadMapActividades(matriz_actividatdes, OSs, mapCsp);
            }else{
                if(this.creaActibidadesBase(mapCsp, input_ws.getId_cs())){
                    matriz_actividatdes = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_ACIVIDADES_PMO.replaceAll("id_cot_sitio", input_ws.getId_cs()));
                    map_actividades = this.loadMapActividades(matriz_actividatdes, OSs, mapCsp);
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error load activities : " + ex.toString());
        }
        
        return map_actividades;
    }
    
    
    public Map<String,CotSitioPlan> loadCSPInformation(InputConsultaActividades input_ws){
        
        Map<String,CotSitioPlan> csp_map = new HashMap<String,CotSitioPlan>();
        
        try{
            
            QueryResult qRcsp_sf = Constantes.CRUD_SF.query_sf(Constantes.QR_GET_CSP_INFO.replaceAll("id_cs", input_ws.getId_cs()));
            
            if(qRcsp_sf.getDone()){
                if(qRcsp_sf.getSize() > 0){
                    for(SObject so_csp : qRcsp_sf.getRecords()){
                        CotSitioPlan csp_sf = new CotSitioPlan();
                        Map<String,String> conf_ot = this.determinaPropiedadesOT((String) so_csp.getField("CuadrillaFFM__c"));
                        csp_sf.setId_csp((String) so_csp.getField("Id"));
                        csp_sf.setNombre_csp((String) so_csp.getField("Name")); 
                        csp_sf.setPaquete((String) so_csp.getField("NombrePlan__c")); 
                        csp_sf.setTipo_intervencion(conf_ot.get(Constantes.KEY_INTERVENCION));
                        csp_sf.setSubtipo_intervencion(conf_ot.get(Constantes.KEY_SUBINTERVENCION));
                        csp_sf.setUnidad_negocio(conf_ot.get(Constantes.KEY_UNIDAD_NEGOCIO));
                        csp_sf.setCuandrilla((String) so_csp.getField("CuadrillaFFM__c"));
                        //--- Cotsitio
                        if(so_csp.getSObjectField("Cot_Sitio__r")!=null && so_csp.getSObjectField("Cot_Sitio__r").getClass().equals(SObject.class)){
                            SObject cot_sitio = (SObject) so_csp.getSObjectField("Cot_Sitio__r");
                            if (cot_sitio != null) {
                                //--- sitio
                                if(cot_sitio.getSObjectField("Sitio__r")!=null && cot_sitio.getSObjectField("Sitio__r").getClass().equals(SObject.class)){
                                    SObject sitio = (SObject) cot_sitio.getSObjectField("Sitio__r");
                                    if (sitio != null) {
                                        csp_sf.setId_sitio((String) sitio.getField("Id"));
                                        csp_sf.setLatitud((String) sitio.getField("Geolocalizacion__Latitude__s"));
                                        csp_sf.setLongitud((String) sitio.getField("Geolocalizacion__Longitude__s"));
                                        csp_sf.setCalle((String) sitio.getField("Calle__c"));
                                        csp_sf.setColonia((String) sitio.getField("Colonia__c"));
                                        csp_sf.setEstado((String) sitio.getField("Estado__c"));
                                        csp_sf.setMunicipio((String) sitio.getField("DelegacionMunicipio__c"));
                                        csp_sf.setNum_interior((String) sitio.getField("NumeroInterior__c"));
                                        csp_sf.setNum_exterior((String) sitio.getField("NumeroExterior__c"));
                                        
                                        //--- Contacto principal
                                        if(sitio.getSObjectField("ContactoPrincipalSitio__r")!=null && sitio.getSObjectField("ContactoPrincipalSitio__r").getClass().equals(SObject.class)){
                                            SObject contacto = (SObject) sitio.getSObjectField("ContactoPrincipalSitio__r");
                                            if (contacto != null) {
                                                csp_sf.setNombre_responsable_sitio((String) contacto.getField("Name"));
                                                csp_sf.setTelefono_contacto((String) contacto.getField("MobilePhone"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        //--- Cuenta factura
                        if(so_csp.getSObjectField("CuentaFactura__r")!=null && so_csp.getSObjectField("CuentaFactura__r").getClass().equals(SObject.class)){
                            SObject cuenta_factura = (SObject) so_csp.getSObjectField("CuentaFactura__r");
                            if (cuenta_factura != null) {
                                csp_sf.setId_cuenta_factura((String) cuenta_factura.getField("Id")); 
                                csp_sf.setNum_cuenta_factura((String) cuenta_factura.getField("IdCuentaBRM__c")); 
                                csp_sf.setCP((String) cuenta_factura.getField("CodigoPostalInstalacion__c")); 
                                csp_sf.setRegion((String) cuenta_factura.getField("RegionInstalacion__c")); 
                                csp_sf.setCiudad((String) cuenta_factura.getField("Plaza__c")); 
                                csp_sf.setDistrito((String) cuenta_factura.getField("DistritoInstalacion__c")); 
                                csp_sf.setCluster((String) cuenta_factura.getField("ClusterInstalacion__c")); 
                            }
                        }
                        
                        //---- Orden de servicio
                        if(so_csp.getSObjectField("OrdenServicio__r")!=null && so_csp.getSObjectField("OrdenServicio__r").getClass().equals(SObject.class)){
                            SObject orden_servicio = (SObject) so_csp.getSObjectField("OrdenServicio__r");
                            if (orden_servicio != null) {
                                csp_sf.setId_os((String) orden_servicio.getField("Id"));    
                                csp_sf.setFolio_os((String) orden_servicio.getField("Name"));
                             }
                        }
                        
                        //----- Cotizacion
                        if(so_csp.getSObjectField("Cotizacion__r")!=null && so_csp.getSObjectField("Cotizacion__r").getClass().equals(SObject.class)){
                            SObject cotizacion = (SObject) so_csp.getSObjectField("Cotizacion__r");
                            if (cotizacion != null) {
                                csp_sf.setId_cotizacion((String) cotizacion.getField("Id"));    
                                csp_sf.setId_pm((String) cotizacion.getField("AsignadoPM__c"));
                             }
                        }
                        
                        csp_map.put((String) so_csp.getField("Id"), csp_sf);
                        
                    }    
                }else{
                    System.err.println("No se encontraron datos");    
                }
            }else{
                System.err.println("No se ejecuto la consulta : " + qRcsp_sf.getQueryLocator().toString());    
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar datos del CSP : " + ex.toString());
        }
        
        return csp_map;
        
    }
    
    
    public Boolean loadPEActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_pe = false;
        
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPE(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_PE)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_PE)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PE()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                       
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            this.creaNuevaActividadPE(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PE : " + ex.toString());
        }
        
        return carga_pe;
    }
    
    public Boolean loadPIEnlActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_enl = false;
        
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPIEnl(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_ENL)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_ENL)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PI()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                       
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            carga_enl = this.creaNuevaActividadPI(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }    
            
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PIEnl : " + ex.toString());
        }
        
        return carga_enl;
        
    }
    
    public Boolean loadPIRecActivities(String OSs,Map<String,Actividad> map_actividades,String id_cs){
        
        Boolean carga_rec = false;
        
        try{
            
            Map<String,OrtdenTrabajo> mapOts = this.loadMapOTsPIRec(OSs);
            SortedSet<String> ots_order = new TreeSet<>(mapOts.keySet());
            Map<String,Actividad> mapOtAct_update = new HashMap<String,Actividad>();
            
            //-- actualiza
            for(Map.Entry<String,Actividad> entry_act : map_actividades.entrySet()){
                Actividad actividad = entry_act.getValue();
                if(actividad.getTiene_Ot().equals("true") && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_REC)){
                    if(mapOts.containsKey(actividad.getId_OT())){
                        OrtdenTrabajo ots = mapOts.get(actividad.getId_OT());
                        String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                        Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                
                        if(actividad.getId_OT().equals(ots.getId_ot()) && actividad.getUnidad_negocio().equals(Constantes.ID_UNIDAD_REC)){
                            // Actualizacion
                            if(responsable){
                                mapOtAct_update.put(ots.getId_ot(), actividad);
                                //-- actualiza el valor de la ot en la tabla de otsfor activities
                                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_DETAILL_ACT.replaceAll("fecha_inicio",(String)ots.getFecha_agenda()).replaceAll("kpi_pi",(String) Variables.getKPI_PI()).replaceAll("avance",ots.getAvance()).replaceAll("id_responsable",(String) id_responsableNuevo).replaceAll("id_actividad", actividad.getId_actividad()).replaceAll("fecha_fin",(String) ots.getFecha_termino()));    
                            }
                        }
                    }
                }
            }
            
            
            for(String key : ots_order) {
                OrtdenTrabajo ots = mapOts.get(key); 
                if(!mapOtAct_update.containsKey(ots.getId_ot())){
                    String id_responsableNuevo = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_RESP)[0][0];
                    Boolean responsable = this.creaResponsableActividad(ots,id_responsableNuevo);
                    String id_actividadNueva = "";
                    // se crea nueva
                        if(responsable){
                            id_actividadNueva = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_SECUENCIA_ACT)[0][0];
                            this.creaNuevaActividadPI(id_responsableNuevo, id_actividadNueva, id_cs, ots);
                        }    
                }
            }    
                
        }catch(Exception ex){
            System.err.println("Error al cargar actividades PiRec : " + ex.toString());
        }
        
        return carga_rec;
    }
    
    public String getOSsPunta(Map<String,CotSitioPlan> map_csp_sf){
        
        ArrayList<String> arrOSs = new ArrayList<String>();
        String OSs = "";
        
        try{
            for(Map.Entry<String,CotSitioPlan> entry : map_csp_sf.entrySet()) {
                CotSitioPlan csp = entry.getValue();
                if(!csp.getFolio_os().equals("") || csp.getFolio_os()!= null){
                    arrOSs.add(csp.getFolio_os());
                }
            }
            
            if(arrOSs.size() > 0){
                OSs = arrOSs.toString().replace("[", "'").replace("]", "'").replaceAll(", ", "','"); 
            }else{
                OSs = "-1";
            }
            
        }catch(Exception ex){
            System.err.println("Error en getOSs : "+ ex.toString());
            OSs = "-1";
        }   
        
        return OSs;
    }
    
    
    public Map<String,OrtdenTrabajo> loadMapOTsPE(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesPE [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_PE.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesPE.length > 0){
                    
                    for(int fila = 0 ; fila < arr_actividadesPE.length; fila++){
                        OrtdenTrabajo ot_pe = new OrtdenTrabajo();
                        ot_pe.setId_ot(arr_actividadesPE[fila][0]);
                        ot_pe.setOs(arr_actividadesPE[fila][1]);
                        ot_pe.setDesc_status(arr_actividadesPE[fila][2]);
                        ot_pe.setId_estado(arr_actividadesPE[fila][3]);
                        ot_pe.setDesc_estado(arr_actividadesPE[fila][4]);
                        ot_pe.setDesc_motivo(arr_actividadesPE[fila][5]);
                        ot_pe.setFecha_termino(arr_actividadesPE[fila][7]);
                        ot_pe.setNombre_responsable(arr_actividadesPE[fila][8].equals("") ? "POR" : arr_actividadesPE[fila][8]);
                        ot_pe.setApellidoPaterno_responsable(arr_actividadesPE[fila][9].equals("") ? "ASIGNAR" : arr_actividadesPE[fila][9]);
                        ot_pe.setApellidoMaterno_responsable(arr_actividadesPE[fila][10].equals("") ? "RESPONSABLE" : arr_actividadesPE[fila][10]);
                        ot_pe.setTelefono_responsable(arr_actividadesPE[fila][11].equals("") ? "PENDIENTE" : arr_actividadesPE[fila][11]);
                        ot_pe.setDetencion_pe(arr_actividadesPE[fila][6]);
                        ot_pe.setId_intervencion(arr_actividadesPE[fila][12]);
                        ot_pe.setId_subIntervencion(arr_actividadesPE[fila][13]);
                        ot_pe.setFecha_agenda(arr_actividadesPE[fila][14]);
                        ot_pe.setUnidad_negocio(Constantes.ID_UNIDAD_PE);
                        ot_pe.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesPE[fila][3]));
                        
                        mapOTs.put(arr_actividadesPE[fila][0], ot_pe);
                    }
                    
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs PE: " + ex.toString());    
        }
        
        return mapOTs;
    }
    
    public Map<String,OrtdenTrabajo> loadMapOTsPIRec(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesRec [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_REC.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesRec!= null){
                    
                    for(int fila = 0 ; fila < arr_actividadesRec.length; fila++){
                        OrtdenTrabajo ot_re = new OrtdenTrabajo();
                        ot_re.setId_ot(arr_actividadesRec[fila][0]); 
                        ot_re.setFecha_agenda(arr_actividadesRec[fila][1]); 
                        ot_re.setTurno(arr_actividadesRec[fila][2]); 
                        ot_re.setOs(arr_actividadesRec[fila][3]); 
                        ot_re.setTecnico(arr_actividadesRec[fila][4]); 
                        ot_re.setAutomovil(arr_actividadesRec[fila][5]); 
                        ot_re.setDesc_status(arr_actividadesRec[fila][6]); 
                        ot_re.setId_estado(arr_actividadesRec[fila][9]); 
                        ot_re.setDesc_estado(arr_actividadesRec[fila][7]); 
                        ot_re.setDesc_motivo(arr_actividadesRec[fila][8]); 
                        ot_re.setFecha_termino(arr_actividadesRec[fila][10]); 
                        ot_re.setUnidad_negocio(Constantes.ID_UNIDAD_REC);
                        ot_re.setDescripcion(arr_actividadesRec[fila][11]);
                        ot_re.setNombre_responsable(arr_actividadesRec[fila][12].equals("") ? "POR" : arr_actividadesRec[fila][12]);
                        ot_re.setApellidoPaterno_responsable(arr_actividadesRec[fila][13].equals("") ? "ASIGNAR" : arr_actividadesRec[fila][13]);
                        ot_re.setApellidoMaterno_responsable(arr_actividadesRec[fila][14].equals("") ? "RESPONSABLE" : arr_actividadesRec[fila][14]);
                        ot_re.setTelefono_responsable(arr_actividadesRec[fila][15].equals("") ? "PENDIENTE" : arr_actividadesRec[fila][15]);
                        ot_re.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesRec[fila][9]));
                        ot_re.setId_intervencion(arr_actividadesRec[fila][16]);
                        ot_re.setId_subIntervencion(arr_actividadesRec[fila][17]);
                        
                        mapOTs.put(arr_actividadesRec[fila][0], ot_re);
                    }
                    
                }else{
                    System.err.println("Error en la consulta de OTs Rec");    
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs REC: " + ex.toString());    
        }
        
        return mapOTs;
    }
    
    public Map<String,OrtdenTrabajo> loadMapOTsPIEnl(String OSs){
        
        Map<String,OrtdenTrabajo> mapOTs = new HashMap<String,OrtdenTrabajo>();
        
        try{
            
            if(!OSs.equals("-1")){
                String arr_actividadesEnl [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OTS_ENL.replaceAll("FOLIOS_OS", OSs));
                
                if(arr_actividadesEnl != null){
                    
                    for(int fila = 0 ; fila < arr_actividadesEnl.length; fila++){
                        OrtdenTrabajo ot_en = new OrtdenTrabajo();
                        
                        ot_en.setId_ot(arr_actividadesEnl[fila][0]); 
                        ot_en.setFecha_agenda(arr_actividadesEnl[fila][1]); 
                        ot_en.setTurno(arr_actividadesEnl[fila][2]); 
                        ot_en.setOs(arr_actividadesEnl[fila][3]); 
                        ot_en.setTecnico(arr_actividadesEnl[fila][4]);                    
                        //ot_en.setAutomovil(arr_actividadesEnl[fila][5]); 
                        ot_en.setDesc_status(arr_actividadesEnl[fila][6]); 
                        ot_en.setId_estado(arr_actividadesEnl[fila][9]); 
                        ot_en.setDesc_estado(arr_actividadesEnl[fila][7]); 
                        ot_en.setDesc_motivo(arr_actividadesEnl[fila][8]); 
                        ot_en.setFecha_termino(arr_actividadesEnl[fila][10]);
                        ot_en.setDescripcion(arr_actividadesEnl[fila][11]);
                        ot_en.setNombre_responsable(arr_actividadesEnl[fila][12]);
                        ot_en.setApellidoPaterno_responsable(arr_actividadesEnl[fila][13]);
                        ot_en.setApellidoMaterno_responsable(arr_actividadesEnl[fila][14]);
                        ot_en.setTelefono_responsable(arr_actividadesEnl[fila][15]);
                        ot_en.setId_intervencion(arr_actividadesEnl[fila][16]);
                        ot_en.setId_subIntervencion(arr_actividadesEnl[fila][17]);
                        ot_en.setUnidad_negocio(Constantes.ID_UNIDAD_ENL);
                        ot_en.setAvance(Constantes.UTILS.calculaAvance(arr_actividadesEnl[fila][9]));
                        
                        mapOTs.put(arr_actividadesEnl[fila][0], ot_en);
                    }
                    
                }else{
                    System.err.println("Error en la consulta de OTs Enl");        
                }    
            }
                
        }catch(Exception ex){
            System.err.println("Error al cargar OTs ENL: " + ex.toString());    
        }
        
        return mapOTs;
    }
    
    public Boolean creaResponsableActividad(OrtdenTrabajo ot,String id_responsable){
        Boolean reg_respo = false;
        try{
            //System.out.println("Datos responsable = "+ id_responsable +" - "+ Constantes.QR_SET_RESPONSABLE + " - " + Constantes.INACTIVE_REG);
            String concat_data_resp = ot.getNombre_responsable()+ ot.getApellidoPaterno_responsable()+ ot.getApellidoMaterno_responsable()+ ot.getTelefono_responsable();
            
            reg_respo = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_RESPONSABLE.replaceAll("cadena_concat", concat_data_resp).replaceAll("id_respo", id_responsable).replaceAll("nombre_res", ot.getNombre_responsable()).replaceAll("apellidop_res", ot.getApellidoPaterno_responsable()).replaceAll("apellidom_res", ot.getApellidoMaterno_responsable()).replaceAll("telefono", ot.getTelefono_responsable()).replaceAll("act_inac", Constantes.INACTIVE_REG));            
            
        }catch(Exception ex){
            System.err.println("Error al crear responsable : " + ex.toString());
        }
        
        return reg_respo;
    }
    
    public Boolean creaNuevaActividadPE(String id_responsable, String id_actividad, String id_cs, OrtdenTrabajo ot){
        
        Boolean res_actividad = false;
        
        try{
            
            String descactividad = Constantes.DESC_ACTIVIDAD_REPARACIONESPE + " "+ ot.getDetencion_pe();
                        
            res_actividad = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_ACTIVIDAD_PE.replaceAll("id_actividad", id_actividad).replaceAll("id_tipo_act", Constantes.ACTIVIDAD_REPARACIONESPE).replaceAll("desc_actividad",descactividad).replaceAll("id_cs", id_cs).replaceAll("user_id", Constantes.SYSTEM_USER).replaceAll("status_act", Constantes.STATUS_PENDIENTE_ACT).replaceAll("kpi_pe", Variables.getKPI_PE()).replaceAll("id_responsable", id_responsable).replaceAll("avance", ot.getAvance()));   
            
            if(res_actividad){
                res_actividad = Constantes.CRUD_FFM.insertaDB(Constantes.QR_REGISTRA_OTACT.replaceAll("IDACTIV", id_actividad).replaceAll("TIPOOT", ot.getId_intervencion()).replaceAll("IDCSP", id_cs).replaceAll("SUBTOT", ot.getId_subIntervencion()).replaceAll("OTPE", ot.getId_ot()).replaceAll("UNIDADNEG", ot.getUnidad_negocio()));
            }           
            
        }catch(Exception ex){
            System.err.println("Error al crear nueva actividad " + ex.toString());
        }    
        
        return res_actividad;
    }
    
    public Boolean creaNuevaActividadPI(String id_responsable, String id_actividad, String id_csp, OrtdenTrabajo ot){
        
        Boolean res_actividad = false;
        
        try{
            
            String descactividad = Constantes.DESC_ACTIVIDAD_IMPLEMENTACION + " "+ ot.getDescripcion();
            res_actividad = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_ACTIVIDAD_PE.replaceAll("id_actividad", id_actividad).replaceAll("id_tipo_act", Constantes.ACTIVIDAD_IMPLEMENTACION).replaceAll("desc_actividad",descactividad).replaceAll("id_cs", id_csp).replaceAll("user_id", Constantes.SYSTEM_USER).replaceAll("status_act", Constantes.STATUS_PENDIENTE_ACT).replaceAll("kpi_pe", Variables.getKPI_PI()).replaceAll("id_responsable", id_responsable).replaceAll("avance", ot.getAvance()));   
            
            System.err.println("Aqui adentro ========== " + id_actividad + " == "+ ot.getId_intervencion()+" == " + id_csp + " == " + ot.getId_subIntervencion() +" == " +ot.getId_ot() + " == " + ot.getUnidad_negocio());
            
            if(res_actividad){
                res_actividad = Constantes.CRUD_FFM.insertaDB(Constantes.QR_REGISTRA_OTACT.replaceAll("IDACTIV", id_actividad).replaceAll("TIPOOT", ot.getId_intervencion()).replaceAll("IDCSP", id_csp).replaceAll("SUBTOT", ot.getId_subIntervencion()).replaceAll("OTPE", ot.getId_ot()).replaceAll("UNIDADNEG", ot.getUnidad_negocio()));
            }  
        
        }catch(Exception ex){
            System.err.println("Error al crear nueva actividad " + ex.toString());
        }    
        
        return res_actividad;
    }
    
    
    public String actualizaOTForActivities(String os, String cuadrilla,String id_actividad){
        String id_ot ="";
        
        try{
            System.out.println("Valor de cuandrilla : " + cuadrilla);
            
            if(cuadrilla.equals(Constantes.ID_UNIDAD_ENL)){
                id_ot = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OT_ENL.replaceAll("FOLIO_OS", os))[0][0];
            }else{
                id_ot = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_OT_REC.replaceAll("FOLIO_OS", os))[0][0];
            }
            
            if(!id_ot.equals("-1")){
                Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_OTACT.replaceAll("ID_OT", id_ot).replaceAll("IDACTIVIDAD", id_actividad));
                System.out.println("OT actualizada en actividad ");
            }
        
        }catch(Exception ex){
            System.err.println("Error en actualizar ot de actividades : " + ex.toString());
        }   
        
        return id_ot;
        
    }
    
    
    public Boolean creaActibidadesBase(Map<String,CotSitioPlan> csp_in_sf,String id_cs){
        
        Boolean cargaActBase = false;
        
        try{
            
            Boolean crea_actividades_base = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_ACTIVIDADES_BASE_DB.replaceAll("system_user", Constantes.SYSTEM_USER).replaceAll("id_cs", id_cs));
            Boolean status_reg_act = false;
            Boolean status_reg_ot = false;
            
            System.err.println("Tama�o map de csp : " + csp_in_sf.size());
            
            if(crea_actividades_base){
                for(Map.Entry<String, CotSitioPlan> csp : csp_in_sf.entrySet()) {
                        status_reg_act = Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_ACTIVIDADES_CSP_DB.replaceAll("name_csp",csp.getValue().getNombre_csp()).replaceAll("id_cs", id_cs).replaceAll("system_user", Constantes.SYSTEM_USER)); 
                        if(status_reg_act){
                            status_reg_ot = Constantes.CRUD_FFM.insertaDB(Constantes.QR_ST_STRUCTURA_OTS.replaceAll("id_css",id_cs).replaceAll("id_tipo_ot", csp.getValue().getTipo_intervencion()).replaceAll("id_subtipo_ot", csp.getValue().getSubtipo_intervencion()).replaceAll("id_unidad_negocio", csp.getValue().getUnidad_negocio()).replaceAll("id_csp",csp.getValue().getId_csp()));
                            cargaActBase = true;
                        }else{
                            System.err.println("ERROR : CREA ESTRUCTURA BASICA");
                        }    
                }        
            }else{
                System.err.println("ERROR : CREA ESTRUCTURA BASICA");
            }
            
        }catch(Exception ex){
            System.err.println("Error crea actividad : " + ex.toString());
        }
        
        return cargaActBase;
    }
    
    public Map<String,String> determinaPropiedadesOT(String key_config){
        
        Map<String,String> config_ot = new HashMap<String,String>();
        
        try{
            
            String[] config = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_CONFIG_OT.replaceAll("key_value",key_config == null ? "Empresarial" : key_config))[0][0].split(",");
            config_ot.put(Constantes.KEY_INTERVENCION, config[0]);                        
            config_ot.put(Constantes.KEY_SUBINTERVENCION, config[1]);
            config_ot.put(Constantes.KEY_UNIDAD_NEGOCIO, config[2]);
        
        }catch(Exception ex){
            System.err.println("Error al determinar configuracion " + ex.toString());
        }
        
        return config_ot;
        
    }
    
    
    public Map<String,Actividad> loadMapActividades(String matriz_actividatdes[][],String OSs,Map<String,CotSitioPlan> mapCsp){
        
        Map<String,Actividad> map_actividades = new HashMap<String,Actividad>();
        
        try{
            
            for(int fila = 0;fila < matriz_actividatdes.length; fila++){
                Actividad actividad = new Actividad();
                
                actividad.setId_actividad(matriz_actividatdes[fila][0]);
                actividad.setNombre_actividad(matriz_actividatdes[fila][1]);
                actividad.setFecha_inicio_planeada(matriz_actividatdes[fila][2]);
                actividad.setFecha_fin_planeada(matriz_actividatdes[fila][3]);
                actividad.setFecha_inicio_real(matriz_actividatdes[fila][4]);
                actividad.setFecha_fin_real(matriz_actividatdes[fila][5]);
                actividad.setNombre_responsable(matriz_actividatdes[fila][6]);
                actividad.setPorcentaje(matriz_actividatdes[fila][7]);
                actividad.setId_dependencia(matriz_actividatdes[fila][8]);
                actividad.setStatus(matriz_actividatdes[fila][9]);
                actividad.setUsuario_crea(matriz_actividatdes[fila][10]);
                actividad.setFecha_creacion(matriz_actividatdes[fila][11]);
                actividad.setId_responsable(matriz_actividatdes[fila][12]); 
                actividad.setHoras_favor(matriz_actividatdes[fila][13]);
                actividad.setPorcentajeEsperado(matriz_actividatdes[fila][14]);
                actividad.setSe_puede_eliminar(matriz_actividatdes[fila][15]);
                actividad.setTiene_Ot(matriz_actividatdes[fila][16]);
                actividad.setUnidad_negocio(matriz_actividatdes[fila][17]);
                actividad.setTipo_intervencion(matriz_actividatdes[fila][18]);
                actividad.setSubtipo_intervencion(matriz_actividatdes[fila][19]);
                actividad.setId_csp(matriz_actividatdes[fila][20]);
                
                if(matriz_actividatdes[fila][14] != null){
                    String semaforo = (String) Constantes.UTILS.getSemaforoEnPlaneacion((String) matriz_actividatdes[fila][14], (String) matriz_actividatdes[fila][7]);
                    actividad.setSemaforo(semaforo);
                    if(matriz_actividatdes[fila][14] != null){
                        actividad.setPorcentajeEsperado(Float.valueOf((String) matriz_actividatdes[fila][14]) >= 100 ? "100" : (String) matriz_actividatdes[fila][14]);    
                    }else{
                        actividad.setPorcentajeEsperado("0");
                    }
                }
                
                String id_ot = matriz_actividatdes[fila][21];
                
                if(id_ot.equals("-1") && !OSs.equals("-1") && matriz_actividatdes[fila][16].equals("true")){
                    System.out.println("Dentro de OTs");
                    CotSitioPlan csp = mapCsp.get(matriz_actividatdes[fila][20]);
                    
                    if(csp != null){
                        String orden_Servicio = csp.getFolio_os();
                        id_ot = this.actualizaOTForActivities(orden_Servicio, csp.getCuandrilla() == null ? Constantes.ID_UNIDAD_ENL : Constantes.ID_UNIDAD_REC,matriz_actividatdes[fila][0]);
                    }
                }
                
                
                if(matriz_actividatdes[fila][16].equals("true")){
                    CotSitioPlan csp = mapCsp.get(matriz_actividatdes[fila][20]);
                    
                    if(csp != null){
                    actividad.setId_csp(matriz_actividatdes[fila][20]);
                    actividad.setId_sitio(csp.getId_sitio());
                    actividad.setPaquete(csp.getPaquete());
                    actividad.setId_cuenta_factura(csp.getId_cuenta_factura());
                    actividad.setNum_cuenta_factura(csp.getNum_cuenta_factura());
                    actividad.setRegion(csp.getRegion());
                    actividad.setCiudad(csp.getCiudad());
                    actividad.setDistrito(csp.getDistrito());
                    actividad.setCluster(csp.getCluster());
                    actividad.setCalle(csp.getCalle());
                    actividad.setColonia(csp.getColonia());
                    actividad.setEstado(csp.getEstado());
                    actividad.setMunicipio(csp.getMunicipio());
                    actividad.setCp(csp.getCP());
                    actividad.setNum_interior(csp.getNum_interior());
                    actividad.setNum_exterior(csp.getNum_exterior());
                    actividad.setLatitud(csp.getLatitud());
                    actividad.setLongitud(csp.getLongitud());
                    actividad.setNombre_responsable_sitio(csp.getNombre_responsable_sitio());
                    actividad.setTelefono_contacto(csp.getTelefono_contacto());
                    actividad.setNombre_cot_sitio(csp.getNombre_csp());
                    }
                }
                
                actividad.setId_OT(id_ot);
                map_actividades.put(matriz_actividatdes[fila][0], actividad);
            } 
        }catch(Exception ex){
            System.err.println("Error al cargar mapa de actividades : " + ex.toString());
        } 
        
        return map_actividades;
    }
    
    public Map<String,String> getContadoresActividades(Map<String,Actividad> mapActividades){
        
        Map<String,String> mapContadores = new HashMap<String,String>();
        
        try{
            Integer total_actividades = 0;
            Integer total_tiempo = 0;
            Integer total_riesgo = 0;
            Integer total_fueratiempo = 0;
            Integer total_pendientes = 0;
            for(Map.Entry<String, Actividad> entyactividad : mapActividades.entrySet()) {
                Actividad actividad = entyactividad.getValue();
                if(actividad.getPorcentajeEsperado() != null){
                    String semaforo = (String) Constantes.UTILS.getSemaforoEnPlaneacion((String) actividad.getPorcentajeEsperado(), (String) actividad.getPorcentaje());
                    
                    if(semaforo.equals(Variables.getCOLOR_ENTIEMPO())){
                        ++total_tiempo;
                    }else if(semaforo.equals(Variables.getCOLOR_RIESGO())){
                        ++total_riesgo;
                    }else if(semaforo.equals(Variables.getCOLOR_FUERA_TIEMPO())){
                        ++total_fueratiempo;
                    }
                }
            }
            total_actividades = mapActividades.size();
            total_pendientes = total_actividades - total_tiempo - total_riesgo - total_fueratiempo;
            
            mapContadores.put(Constantes.KEY_TOTAL, total_actividades.toString());
            mapContadores.put(Constantes.KEY_TIEMPO, total_tiempo.toString());
            mapContadores.put(Constantes.KEY_RIESGO, total_riesgo.toString());
            mapContadores.put(Constantes.KEY_FUERATIEMPO, total_fueratiempo.toString());
            mapContadores.put(Constantes.KEY_PENDIENTE, total_pendientes.toString());
        
        }catch(Exception ex){
            System.err.println("Error en contadores : " + ex.toString());
        }    
        
        return mapContadores;
    }
    
    
    public Boolean conciliaProyecto(String id_cs,Map<String,CotSitioPlan> map_csp_sf){
        Boolean bandera = false;
        
        try{
            
            Integer contador = Integer.valueOf(Constantes.CRUD_FFM.consultaBD(Constantes.QR_CONT_PROYECT_DET.replaceAll("id_cs",id_cs))[0][0]);
            
            if(contador == 0){
                String id_cot = "";
                String id_pm = "";
                Z:
                for(Map.Entry<String, CotSitioPlan> entyty : map_csp_sf.entrySet()) {
                    CotSitioPlan csp = entyty.getValue();
                        id_cot = csp.getId_cotizacion();
                        id_pm = csp.getId_pm();
                    break;
                }
                
                Constantes.CRUD_FFM.insertaDB(Constantes.QR_SET_DETALLE_PUNTAS.replaceAll("id_cs",id_cs).replaceAll("status", "1").replaceAll("id_cotizacion", id_cot).replaceAll("avance", "0").replaceAll("id_proyect_manager", id_pm));
            }
            
            String[][] avance_promedio_punta = Constantes.CRUD_FFM.consultaBD(Constantes.QR_GET_PROMEDIO_PUNTA.replaceAll("id_cs",id_cs)); 
            
            if(avance_promedio_punta[0][0]!= null && avance_promedio_punta[0][0]!= ""){
                
              
               Boolean actualiza_avance_punta = false;
               
                if(avance_promedio_punta[0][3].equals("0")){
                    actualiza_avance_punta = Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_AVANCE_PUNTA.replaceAll("fecha_inicio",avance_promedio_punta[0][0])
                    .replaceAll("fecha_fin", avance_promedio_punta[0][1]).replaceAll("avance", avance_promedio_punta[0][2]).replaceAll("id_cp", id_cs));
                }else{
                    actualiza_avance_punta = Constantes.CRUD_FFM.actualizaDB(Constantes.QR_UPDATE_PUNTA_CERRADA.replaceAll("fecha_inicio",avance_promedio_punta[0][0])
                    .replaceAll("fecha_fin", avance_promedio_punta[0][1]).replaceAll("avance", avance_promedio_punta[0][2]).replaceAll("id_cp", id_cs));
                }
                
                if(actualiza_avance_punta){
                    bandera = true;
                }else{
                    bandera = false; 
                }
                
            }else{
                bandera = false;
            }
            
        }catch(Exception ex){
            System.err.println("Error conciliacion : " + ex.toString());
            bandera = false;
        }
        
        return bandera;
    }
    
    
    public DetalleAvances avancesPuntaProyecto(String id_cs,Map<String,CotSitioPlan> map_csp_sf){
        DetalleAvances avanceas = new DetalleAvances();
        try{
            String id_cot = "";
            
            Z:
            for(Map.Entry<String, CotSitioPlan> entyty : map_csp_sf.entrySet()) {
                CotSitioPlan csp = entyty.getValue();
                    id_cot = csp.getId_cotizacion();
                break;
            }
            
            String avance_punta [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_AVANCE_PUNTA.replaceAll("IDCS", id_cs));
            String avance_cot [][] = Constantes.CRUD_FFM.consultaBD(Constantes.QR_AVANCE_COT.replaceAll("IDCOTS", id_cot));
            
            avanceas.setPuntaFechaFinPlaneada(avance_punta[0][0]);
            avanceas.setPuntaFechaInicioReal(avance_punta[0][1]);
            avanceas.setPuntaFechaFinReal(avance_punta[0][2]);
            avanceas.setPuntaPorcentaje(avance_punta[0][3]);
            avanceas.setPuntaPorcentajeEsperado(avance_punta[0][4]);
            avanceas.setPuntaSemaforo(Constantes.UTILS.getSemaforoEnPlaneacion(avance_punta[0][4], avance_punta[0][3]));
            
            avanceas.setCotFechaFinPlaneada(avance_cot[0][0]);
            avanceas.setCotFechaInicioReal(avance_cot[0][1]);
            avanceas.setCotFechaFinReal(avance_cot[0][2]);
            avanceas.setCotPorcentaje(avance_cot[0][3]);
            avanceas.setCotPorcentajeEsperado(avance_cot[0][4]);
            avanceas.setCotSemaforo(Constantes.UTILS.getSemaforoEnPlaneacion(avance_cot[0][4], avance_cot[0][3]));
            
        }catch(Exception ex){
            System.err.println("Error : " + ex.toString());
        }    
        
        return avanceas;
    }
    */
}
