package com.proyectmanager.logica;

import com.proyectmanager.objects.Constantes;

public class ActualizaActividades {
    public ActualizaActividades() {
        super();
    }
    
    public static void actualizaActividades(String actividades[][],String responsable){
        try{
            String porcentaje = "0.0";
            
            for(int fila = 0; fila<actividades.length; fila++){
                porcentaje = Constantes.UTILS.calculaAvance(actividades[fila][4]);
                Constantes.CRUD_FFM.actualizaDB(Constantes.C_ACT_ACTIVIDADES.replaceAll("idort", actividades[fila][0]).replaceAll("idrespo", responsable).replaceAll("inidate", actividades[fila][1]).replaceAll("findate", actividades[fila][2]).replaceAll("iniredate", actividades[fila][1]).replaceAll("finredate", actividades[fila][2]).replaceAll("resava", porcentaje));
            }
        
        }catch(Exception ex){
            System.err.println("Error al actualizar actividades : " + ex.toString());
        }            
    }
    
}
