package com.proyectmanager.logica;

import com.proyectmanager.objects.Constantes;

public class CargaEstructuraBase {
    public CargaEstructuraBase() {
        super();
    }
    
    public static String cargaEstructuraBasica(String id_cuenta_sf, String id_puente_sf, String id_pm_sf,String tienecsp){
        
        String id_proyect = "";
        
        try{
                
            String id_proyecto = Constantes.CRUD_FFM.insertaDBGetID(Constantes.Q_REGISTRA_INFORMACION_PROYECTO.replaceAll("idcsf", id_cuenta_sf).replaceAll("idbrsf", id_puente_sf).replaceAll("idpmsf", id_pm_sf).replaceAll("hvcsp", tienecsp).replaceAll("stpro", Constantes.C_ESTATUS_PROYECTO_NUEVO).replaceAll("cmpro", Constantes.C_COMENTARIO_BASE_PR),"PDI_ID");
            Constantes.CRUD_FFM.insertaDB(Constantes.Q_REGISTRA_ACTIVIDADES_BASE.replaceAll("idimple", id_proyecto));
            
            id_proyect = id_proyecto;
            
        }catch(Exception ex){
            System.err.println("Error al cargar estructura basica : " + ex.toString());
        }
        
        return id_proyect;
        
    }
}
