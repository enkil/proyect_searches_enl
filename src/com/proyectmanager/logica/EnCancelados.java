package com.proyectmanager.logica;

import com.proyectmanager.inputs.InputConsultaProyectos;
import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Variables;
import com.proyectmanager.outputs.OutputConsultaProyectos;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import java.util.Map;

public class EnCancelados {
    public EnCancelados() {
        super();
    }
    
    public OutputConsultaProyectos getProyectosEnCancelados(InputConsultaProyectos input_ws){
        OutputConsultaProyectos output_ws = new OutputConsultaProyectos();
        
        try{
            Map<String,String> map_fechas = Constantes.UTILS.fechasFiltroSf("M", input_ws.getMesConsulta());
            System.out.println(Variables.getQR_BANDEJA_CANCELADO().replaceAll("idPm", input_ws.getId_pm()).replaceAll("fechaIn", map_fechas.get("fechaIn")).replaceAll("fechaFin", map_fechas.get("fechaFin")));
            QueryResult proyectos_sf = Constantes.CRUD_SF.query_sf(Variables.getQR_BANDEJA_CANCELADO().replaceAll("idPm", input_ws.getId_pm()).replaceAll("fechaIn", map_fechas.get("fechaIn")).replaceAll("fechaFin", map_fechas.get("fechaFin")));
            
            if(proyectos_sf.getDone()){
                if(proyectos_sf.getSize()>0){
                    CotSitioPlan arrCsps [] = new CotSitioPlan[proyectos_sf.getSize()];
                    int cont = 0;
                    for(SObject proyecto : proyectos_sf.getRecords()){
                        CotSitioPlan csp = new CotSitioPlan();
                        
                        csp.setId_csp((String) proyecto.getField("Id"));
                        csp.setFolio_CSP((String) proyecto.getField("Name"));
                        csp.setNombrePlan((String) proyecto.getField("NombrePlan__c"));
                        csp.setFecha_cierre(((String) proyecto.getField("TSganada__c")).replaceAll("T", " ").replaceAll(".000Z", ""));
                        csp.setTop5000((String) proyecto.getField("Top5000__c"));
                        csp.setEstatusActivacion((String) proyecto.getField("EstatusActivacion__c"));
                        
                        if(Constantes.UTILS.validateDataSF(proyecto,"CuentaFactura__r")){
                            SObject cuentaFactura = (SObject) proyecto.getField("CuentaFactura__r");  
                            csp.setId_cuenta_factura((String) cuentaFactura.getField("Id"));
                            csp.setNum_cuenta_factura((String) cuentaFactura.getField("IdCuentaBRM__c"));
                        }
                             
                        if(Constantes.UTILS.validateDataSF(proyecto,"OrdenServicio__r")){
                            SObject os = (SObject) proyecto.getField("OrdenServicio__r");  
                            csp.setId_OS((String) os.getField("Id"));
                            csp.setFolio_OS((String) os.getField("Name"));
                            csp.setFechaOsCompletada((String) os.getField("TSCancelado__c"));
                        }
                        
                        if(Constantes.UTILS.validateDataSF(proyecto,"Cotizacion__r")){
                            SObject cotizacion = (SObject) proyecto.getField("Cotizacion__r");
                            
                            csp.setId_cotizacion((String) cotizacion.getField("Id"));
                            csp.setFolioCotizacion((String) cotizacion.getField("Name"));
                            
                            if(Constantes.UTILS.validateDataSF(cotizacion,"Oportunidad__r")){
                                SObject oportunidad = (SObject) cotizacion.getField("Oportunidad__r");
                                
                                if(Constantes.UTILS.validateDataSF(oportunidad,"Account")){
                                    SObject cuenta = (SObject) oportunidad.getField("Account");
                                    csp.setNombreCliente((String) cuenta.getField("RazonSocial__c"));
                                }
                            }
                        }
                        
                        arrCsps[cont++] = csp;
                    }
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion("Informacion de proyectos");
                    output_ws.setVersion(Constantes.WS_VERSION);
                    output_ws.setCspFacturando(arrCsps);
                }else{
                    output_ws.setResult(Constantes.RESULT_SUSSES);
                    output_ws.setResultDescripcion("Sin informacion");
                    output_ws.setVersion(Constantes.WS_VERSION);
                }
            }else{
                output_ws.setResult(Constantes.RESULT_ERROR);
                output_ws.setResultDescripcion("NO SE EJECUTO LA CONSULTA : " + proyectos_sf.getQueryLocator());
                output_ws.setVersion(Constantes.WS_VERSION);
            }
        
        }catch(Exception ex){
            output_ws.setResult(Constantes.RESULT_ERROR);
            output_ws.setResultDescripcion("ERROR : " + ex.toString());
            output_ws.setVersion(Constantes.WS_VERSION);
        }
        
        return output_ws;    
    }    
    
}
