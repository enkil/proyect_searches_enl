package com.proyectmanager.logica;

import com.proyectmanager.objects.Constantes;
import com.proyectmanager.objects.Contacto;
import com.proyectmanager.objects.CotSitio;
import com.proyectmanager.objects.CotSitioPlan;
import com.proyectmanager.objects.Cotizacion;
import com.proyectmanager.objects.Cuenta;
import com.proyectmanager.objects.CuentaFactura;
import com.proyectmanager.objects.LocalizacionInstalacion;
import com.proyectmanager.objects.Oportunidad;
import com.proyectmanager.objects.Os;

import com.proyectmanager.objects.ResumenCotizacion;
import com.proyectmanager.objects.Sitio;

import com.proyectmanager.objects.Ticket;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

import com.sun.xml.internal.bind.v2.model.core.ID;

public class InformacionObjetoSF {
    public InformacionObjetoSF() {
        super();
    }
    
    public static Cuenta detalleCuenta(String idSf){
        
        Cuenta cuenta = null;
        
        try{
            
            QueryResult detCuenta = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_CUENTA_SF.replaceAll("id_sf", idSf));
            
            if(detCuenta.getSize()>0){
                for(SObject cuentasf : detCuenta.getRecords()){
                    cuenta = new Cuenta();
                    
                    QueryResult detOportunidad = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_OPORTUNIDAD_CU.replaceAll("id_cuenta_sf", (String) cuentasf.getField("Id")));
                    QueryResult detCuentaFactura = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_CUENTAFACTURA_CU.replaceAll("id_cuenta_sf", (String) cuentasf.getField("Id")));
                    
                    Contacto contactoPrincipal = cargaContactoPrincipal(cuentasf);
                    Contacto creadoPor = cargaCreador(cuentasf);
                    Contacto modificadoPor = cargaModificador(cuentasf);
                    Contacto propietario = cargaPropietario(cuentasf);
                    CotSitioPlan detalleUbicacion = new CotSitioPlan();
                    
                    cuenta.setTipoPersona(Constantes.UTILS.cleanString((String) cuentasf.getField("TipoPersona__c")));
                    cuenta.setNombre(Constantes.UTILS.cleanString((String) cuentasf.getField("Name")));
                    cuenta.setRazonSocial(Constantes.UTILS.cleanString((String) cuentasf.getField("RazonSocial__c")));
                    cuenta.setRfc(Constantes.UTILS.cleanString((String) cuentasf.getField("RFC__c")));
                    cuenta.setTop5000(Constantes.UTILS.cleanString((String) cuentasf.getField("Top_5000__c")));
                    cuenta.setTelefono(Constantes.UTILS.cleanString((String) cuentasf.getField("Phone")));
                    cuenta.setSector(Constantes.UTILS.cleanString((String) cuentasf.getField("Industry")));
                    cuenta.setFolioCuenta(Constantes.UTILS.cleanString((String) cuentasf.getField("FolioCuenta__c")));
                    cuenta.setSegmentoFacturacion(Constantes.UTILS.cleanString((String) cuentasf.getField("SegmentoFacturacion__c")));
                    cuenta.setMontoFacturacion(Constantes.UTILS.cleanString((String) cuentasf.getField("MontoPotencial__c")));
                    detalleUbicacion.setCalle(Constantes.UTILS.cleanString((String) cuentasf.getField("Calle__c")));
                    detalleUbicacion.setNum_exterior(Constantes.UTILS.cleanString((String) cuentasf.getField("NumeroExterior__c")));
                    detalleUbicacion.setNum_interior(Constantes.UTILS.cleanString((String) cuentasf.getField("NumeroInterior__c")));
                    detalleUbicacion.setColonia(Constantes.UTILS.cleanString((String) cuentasf.getField("Colonia__c")));
                    detalleUbicacion.setMunicipio(Constantes.UTILS.cleanString((String) cuentasf.getField("DelegacionMunicipio__c")));
                    detalleUbicacion.setCiudad(Constantes.UTILS.cleanString((String) cuentasf.getField("Ciudad__c")));
                    detalleUbicacion.setEstado(Constantes.UTILS.cleanString((String) cuentasf.getField("Estado__c")));
                    detalleUbicacion.setCP(Constantes.UTILS.cleanString((String) cuentasf.getField("CodigoPostal__c")));  
                    
                    cuenta.setArrOportinidades(cargaArregloOportunidades(detOportunidad));
                    cuenta.setArrCuentasFacturas(cargaArregloCuentasFactura(detCuentaFactura));
                    cuenta.setContactoPrincipal(contactoPrincipal);
                    cuenta.setCreadoPor(creadoPor);
                    cuenta.setModificadoPor(modificadoPor);
                    cuenta.setPropietarioCuenta(propietario);
                    cuenta.setDomicilioCuenta(detalleUbicacion);
                }
            }    
            
        }catch(Exception ex){
            System.err.println("Error al consultar cuenta sf : " + ex.toString());
        }
        
        return cuenta;            
    }
    
    public static Oportunidad detalleOportunidad(String idSf){
        
        Oportunidad oportunidad = null;
        
        try{
            
            QueryResult detOportunidad = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_OPORTUNIDAD_SF.replace("id_sf", idSf));
            
                if(detOportunidad.getSize()>0){
                    for(SObject oportunidadSf : detOportunidad.getRecords()){
                        oportunidad = new Oportunidad();
                        Cuenta cuenta = new Cuenta();
                        Contacto contactoPrincipal = new Contacto();
                        Contacto creadoPor = cargaCreador(oportunidadSf);
                        Contacto modificadoPor = cargaModificador(oportunidadSf);
                        Contacto propietario = cargaPropietario(oportunidadSf);
                        
                        oportunidad.setNombre(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Name")));
                        oportunidad.setNumeroOportunidad(Constantes.UTILS.cleanString((String) oportunidadSf.getField("NumeroOportunidad__c")));
                        oportunidad.setEtapa(Constantes.UTILS.cleanString((String) oportunidadSf.getField("StageName")));
                        oportunidad.setTipo(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Type")));
                        oportunidad.setSubTipo(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Subtipo__c")));
                        oportunidad.setOrigenProspecto(Constantes.UTILS.cleanString((String) oportunidadSf.getField("LeadSource")));
                        oportunidad.setSegmentoFacturacion(Constantes.UTILS.cleanString((String) oportunidadSf.getField("SegmentoFacturacion__c")));
                        oportunidad.setMontoFacturacion(Constantes.UTILS.cleanString((String) oportunidadSf.getField("FacturacionPotencial__c")));
                        oportunidad.setFechaCierre(Constantes.UTILS.cleanString((String) oportunidadSf.getField("CloseDate")));
                        oportunidad.setPlazo(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Plazo__c")));
                        oportunidad.setPlaza(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Plaza__c")));
                        oportunidad.setSegmento(Constantes.UTILS.cleanString((String) oportunidadSf.getField("Segmento__c")));
                        
                        if(Constantes.UTILS.validateDataSF(oportunidadSf, "Account")){
                            SObject  cuentaSf = (SObject) oportunidadSf.getField("Account");
                            cuenta.setId(Constantes.UTILS.cleanString((String) cuentaSf.getField("Id")));
                            cuenta.setNombre(Constantes.UTILS.cleanString((String) cuentaSf.getField("Name")));
                            cuenta.setRazonSocial(Constantes.UTILS.cleanString((String) cuentaSf.getField("RazonSocial__c")));
                            cuenta.setRfc(Constantes.UTILS.cleanString((String) cuentaSf.getField("RFC__c")));
                            cuenta.setTop5000(Constantes.UTILS.cleanString((String) cuentaSf.getField("Top_5000__c")));
                            cuenta.setKeyObject(Constantes.TYPE_CUENTA);
                            if(Constantes.UTILS.validateDataSF(cuentaSf, "ContactoPrincipal__r")){
                                SObject  contactoSf = (SObject) cuentaSf.getField("ContactoPrincipal__r");
                                contactoPrincipal.setNombre(Constantes.UTILS.cleanString((String) contactoSf.getField("NombreCompleto__c")));
                                cuenta.setContactoPrincipal(contactoPrincipal);
                            }
                        }
                        
                        oportunidad.setDatosCuenta(cuenta);
                        oportunidad.setCreadoPor(creadoPor);
                        oportunidad.setModificadoPor(modificadoPor);
                        oportunidad.setPropietarioCuenta(propietario);
                        
                    }    
                }
        
        }catch(Exception ex){
            System.err.println("Error al consultar oportunidad sf : " + ex.toString());
        }
        
        return oportunidad;
    }
    
    public static Cotizacion detalleCotizacion(String idSf){
        
        Cotizacion cotizacion = null;
        
        try{
            
            QueryResult detalleCot = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_COTIZACION_SF.replaceAll("id_sf", idSf));
                
                if(detalleCot.getSize()>0){
                    for(SObject cotSf : detalleCot.getRecords()){
                        cotizacion  = new Cotizacion();
                        
                        QueryResult arrCotSitio = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_COTSITIO_COT.replaceAll("id_cot_sf", (String) cotSf.getField("Id")));
                        QueryResult arrCotSitioPlan = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_COTSITIOPLAN_COT.replaceAll("id_cot_sf", (String) cotSf.getField("Id")));
                        
                        Contacto creadoPor = cargaCreador(cotSf);
                        Contacto modificadoPor = cargaModificador(cotSf);
                        Contacto propietario = cargaPropietario(cotSf);
                        Contacto asignadoPM = new Contacto();
                        Oportunidad detalleOportunidad = new Oportunidad();
                        Cuenta detalleCuenta = new Cuenta();
                        CotSitioPlan detalleCotSitioPlan = new CotSitioPlan();
                        CotSitioPlan domicilioCuenta = new CotSitioPlan();
                        ResumenCotizacion resumenCotizacion = new ResumenCotizacion();
                        
                        if(Constantes.UTILS.validateDataSF(cotSf, "AsignadoPM__r")){
                            SObject pmAsignadoSf = (SObject) cotSf.getField("AsignadoPM__r");
                            asignadoPM.setNombre(Constantes.UTILS.cleanString((String) pmAsignadoSf.getField("Name")));
                        }
                        
                        if(Constantes.UTILS.validateDataSF(cotSf, "Oportunidad__r")){
                            SObject aportunidadSf = (SObject) cotSf.getField("Oportunidad__r");
                            detalleOportunidad.setId(Constantes.UTILS.cleanString((String) aportunidadSf.getField("Id")));
                            detalleOportunidad.setNombre(Constantes.UTILS.cleanString((String) aportunidadSf.getField("Name")));
                            detalleOportunidad.setKeyObject(Constantes.TYPE_OPORTUNIDAD);
                            
                            if(Constantes.UTILS.validateDataSF(aportunidadSf, "Account")){
                                SObject cuentaSf = (SObject) aportunidadSf.getField("Account");
                                detalleCuenta.setTipoPersona(Constantes.UTILS.cleanString((String) cuentaSf.getField("TipoPersona__c")));
                                detalleCuenta.setNombre(Constantes.UTILS.cleanString((String) cuentaSf.getField("Name")));
                                detalleCuenta.setRazonSocial(Constantes.UTILS.cleanString((String) cuentaSf.getField("RazonSocial__c")));
                                detalleCuenta.setRfc(Constantes.UTILS.cleanString((String) cuentaSf.getField("RFC__c")));
                                detalleCuenta.setTelefono(Constantes.UTILS.cleanString((String) cuentaSf.getField("Phone")));
                                detalleCuenta.setTipoPersona(Constantes.UTILS.cleanString((String) cuentaSf.getField("Industry")));
                                detalleCuenta.setFolioCuenta(Constantes.UTILS.cleanString((String) cuentaSf.getField("FolioCuenta__c")));
                                detalleCuenta.setSegmentoFacturacion(Constantes.UTILS.cleanString((String) cuentaSf.getField("SegmentoFacturacion__c")));
                                detalleCuenta.setMontoFacturacion(Constantes.UTILS.cleanString((String) cuentaSf.getField("MontoPotencial__c")));
                                
                                domicilioCuenta.setCalle(Constantes.UTILS.cleanString((String) cuentaSf.getField("Calle__c")));
                                domicilioCuenta.setNum_exterior(Constantes.UTILS.cleanString((String) cuentaSf.getField("NumeroExterior__c")));
                                domicilioCuenta.setNum_interior(Constantes.UTILS.cleanString((String) cuentaSf.getField("NumeroInterior__c")));
                                domicilioCuenta.setColonia(Constantes.UTILS.cleanString((String) cuentaSf.getField("Colonia__c")));
                                domicilioCuenta.setMunicipio(Constantes.UTILS.cleanString((String) cuentaSf.getField("DelegacionMunicipio__c")));
                                domicilioCuenta.setCiudad(Constantes.UTILS.cleanString((String) cuentaSf.getField("Ciudad__c")));
                                domicilioCuenta.setEstado(Constantes.UTILS.cleanString((String) cuentaSf.getField("Estado__c")));
                                domicilioCuenta.setCP(Constantes.UTILS.cleanString((String) cuentaSf.getField("CodigoPostal__c")));
                                
                                detalleCuenta.setDomicilioCuenta(domicilioCuenta);
                                detalleCuenta.setKeyObject(Constantes.TYPE_CUENTA);
                            }
                        }
                        
                        if(Constantes.UTILS.validateDataSF(cotSf, "CotSitioPlanPrincipal__r")){
                            SObject csp = (SObject) cotSf.getField("CotSitioPlanPrincipal__r");
                            detalleCotSitioPlan.setId(Constantes.UTILS.cleanString((String) csp.getField("Id")));
                            detalleCotSitioPlan.setNombre(Constantes.UTILS.cleanString((String) csp.getField("Name")));
                            detalleCotSitioPlan.setKeyObject(Constantes.TYPE_COTSITIOPLAN);
                        }
                        
                        cotizacion.setId(Constantes.UTILS.cleanString((String) cotSf.getField("Id")));
                        cotizacion.setNombre(Constantes.UTILS.cleanString((String) cotSf.getField("Name")));
                        cotizacion.setSegmentoFacturacion(Constantes.UTILS.cleanString((String) cotSf.getField("Segmento_Facturacion__c")));
                        cotizacion.setTipoCotizacion(Constantes.UTILS.cleanString((String) cotSf.getField("TipoCotizacion__c")));
                        cotizacion.setPropietarioOportunidad(Constantes.UTILS.cleanString((String) cotSf.getField("Propietario_Oportunidad__c")));
                        cotizacion.setEstatus(Constantes.UTILS.cleanString((String) cotSf.getField("Estatus__c")));
                        cotizacion.setVigenciaCotizacion(Constantes.UTILS.cleanString((String) cotSf.getField("VigenciaCotizacion__c")));
                        cotizacion.setPlazo(Constantes.UTILS.cleanString((String) cotSf.getField("Plazo__c")));
                        cotizacion.setCotizacionPrincipal(Constantes.UTILS.cleanString((String) cotSf.getField("CotizacionPrincipal__c")));
                        cotizacion.setTipoCotizacionPm(Constantes.UTILS.cleanString((String) cotSf.getField("TipoCotizacionPM__c")));
                        cotizacion.setRegion(Constantes.UTILS.cleanString((String) cotSf.getField("Region__c")));
                        cotizacion.setPlaza(Constantes.UTILS.cleanString((String) cotSf.getField("Plaza__c")));
                        cotizacion.setSegmentoGerencia(Constantes.UTILS.cleanString((String) cotSf.getField("Segmento_Gerencia__c")));
                        cotizacion.setInicioProyecto(Constantes.UTILS.cleanString((String) cotSf.getField("Inicio_de_Proyecto__c")));
                        cotizacion.setPuntasTotales(Constantes.UTILS.cleanString((String) cotSf.getField("PuntasTotales__c")));
                        cotizacion.setTop5mil(Constantes.UTILS.cleanString((String) cotSf.getField("Top5mil__c")));
                        
                        resumenCotizacion.setTotalIncluidos(Constantes.UTILS.cleanString((String) cotSf.getField("c_TotalIncluidos__c")));
                        resumenCotizacion.setTotalAdicionales(Constantes.UTILS.cleanString((String) cotSf.getField("c_TotalAdicionales__c")));
                        resumenCotizacion.setSubtotalRenta(Constantes.UTILS.cleanString((String) cotSf.getField("c_SubtotalRenta__c")));
                        resumenCotizacion.setTotalRentaConDescuento(Constantes.UTILS.cleanString((String) cotSf.getField("c_TotalRenta_ConDescuento__c")));
                        resumenCotizacion.setTotalRentaConImpuesto(Constantes.UTILS.cleanString((String) cotSf.getField("c_TotalRenta_ConImpuesto__c")));
                        
                        cotizacion.setArrCotSitio(cargaArregloCotSitio(arrCotSitio));
                        cotizacion.setArrCotSitioPlan(cargaArregloCotSitioPlan(arrCotSitioPlan));
                        cotizacion.setAsignadoPM(asignadoPM);          
                        cotizacion.setCreadoPor(creadoPor);
                        cotizacion.setModificadoPor(modificadoPor);
                        cotizacion.setPropietario(propietario);
                        cotizacion.setResumenCotizacion(resumenCotizacion);
                    }
                }
            
        
        }catch(Exception ex){
            System.err.println("Error al consultar cotizacion sf : " + ex.toString());
        }
        
        return cotizacion;        
    }
    
    public static CotSitio detalleCotSitio(String idSf){
        
        CotSitio cotSitio = null;
        
        try{
            
            QueryResult detCotSitio = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_COTSITIO_SF.replace("id_sf", idSf));
            
                if(detCotSitio.getSize()>0){
                    for(SObject csSf : detCotSitio.getRecords()){
                        cotSitio = new CotSitio();
                        Cotizacion detalleCotizacion = new Cotizacion();
                        Sitio detalleSitio = new Sitio();
                        Contacto creadoPor = cargaCreador(csSf);
                        Contacto editadoPor = cargaModificador(csSf);
                        Contacto propietario = cargaPropietario(csSf);
                        
                        QueryResult arrCotSitioPlan = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_COTSITIOPLAN_CS.replaceAll("id_cs_sf", (String) csSf.getField("Id")));
                        
                        cotSitio.setId(Constantes.UTILS.cleanString((String) csSf.getField("Id")));
                        cotSitio.setNombre(Constantes.UTILS.cleanString((String) csSf.getField("Name")));
                        cotSitio.setDireccionSitio(Constantes.UTILS.cleanString((String) csSf.getField("DireccionSitio__c")));
                        cotSitio.setPlaza(Constantes.UTILS.cleanString((String) csSf.getField("Plaza__c")));
                        cotSitio.setTotalRentaConImpuesto(Constantes.UTILS.cleanString((String) csSf.getField("TotalRenta_ConImpuesto__c")));
                        cotSitio.setSubtotalRenta(Constantes.UTILS.cleanString((String) csSf.getField("SubtotalRenta__c")));
                        
                        if(Constantes.UTILS.validateDataSF(csSf, "Cotizacion__r")){
                            SObject cotSf = (SObject) csSf.getField("Cotizacion__r");
                            detalleCotizacion.setId(Constantes.UTILS.cleanString((String) cotSf.getField("Id")));
                            detalleCotizacion.setNombre(Constantes.UTILS.cleanString((String) cotSf.getField("Name")));
                            detalleCotizacion.setKeyObject(Constantes.TYPE_COTIZACION);
                        }
                        
                        if(Constantes.UTILS.validateDataSF(csSf, "Sitio__r")){
                            SObject sitiosf = (SObject) csSf.getField("Sitio__r");
                            detalleSitio.setNombreSitio(Constantes.UTILS.cleanString((String) sitiosf.getField("IdSitio__c")));
                            detalleSitio.setNombre(Constantes.UTILS.cleanString((String) sitiosf.getField("Name")));
                            detalleSitio.setFactibilidad(Constantes.UTILS.cleanString((String) sitiosf.getField("Factibilidad__c")));
                            detalleSitio.setCalle(Constantes.UTILS.cleanString((String) sitiosf.getField("Calle__c")));
                            detalleSitio.setNumeroExterior(Constantes.UTILS.cleanString((String) sitiosf.getField("NumeroExterior__c")));
                            detalleSitio.setNumeroInterior(Constantes.UTILS.cleanString((String) sitiosf.getField("NumeroInterior__c")));
                            detalleSitio.setColonia(Constantes.UTILS.cleanString((String) sitiosf.getField("Colonia__c")));
                            detalleSitio.setDelegacionMunicipio(Constantes.UTILS.cleanString((String) sitiosf.getField("DelegacionMunicipio__c")));
                            detalleSitio.setCodigoPostal(Constantes.UTILS.cleanString((String) sitiosf.getField("CodigoPostal__c")));
                            detalleSitio.setTipoCobertura(Constantes.UTILS.cleanString((String) sitiosf.getField("TipoCobertura__c")));
                            detalleSitio.setCobertura(Constantes.UTILS.cleanString((String) sitiosf.getField("Cobertura__c")));
                            detalleSitio.setPlaza(Constantes.UTILS.cleanString((String) sitiosf.getField("Plaza__c")));
                            detalleSitio.setRegion(Constantes.UTILS.cleanString((String) sitiosf.getField("Region__c")));
                            detalleSitio.setRegionId(Constantes.UTILS.cleanString((String) sitiosf.getField("RegionId__c")));
                            detalleSitio.setZona(Constantes.UTILS.cleanString((String) sitiosf.getField("Zona__c")));
                            detalleSitio.setDistrito(Constantes.UTILS.cleanString((String) sitiosf.getField("Distrito2__c")));
                            detalleSitio.setCiudad(Constantes.UTILS.cleanString((String) sitiosf.getField("Ciudad__c")));
                            detalleSitio.setEstado(Constantes.UTILS.cleanString((String) sitiosf.getField("Estado__c")));
                            detalleSitio.setLatitude(Constantes.UTILS.cleanString((String) sitiosf.getField("Geolocalizacion__Latitude__s")));
                            detalleSitio.setLongitude(Constantes.UTILS.cleanString((String) sitiosf.getField("Geolocalizacion__Longitude__s")));
                        }
                        
                        cotSitio.setArrCotSitioPlan(cargaArregloCotSitioPlan(arrCotSitioPlan));
                        cotSitio.setDetalleCotizacion(detalleCotizacion);
                        cotSitio.setDetalleSitio(detalleSitio);
                        cotSitio.setCreadoPor(creadoPor);
                        cotSitio.setEditadoPor(editadoPor);
                        cotSitio.setPropietario(propietario);
                        
                        
                    }
                }
            
        }catch(Exception ex){
            System.err.println("Error al consultar cs sf : " + ex.toString());
        }
        
        return cotSitio;
    }
    
    public static CotSitioPlan detalleCotSitioPlan(String idSf){
        
        CotSitioPlan cotSitioPlan = null;
        
        try{
            
            QueryResult detCsp = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_COTSITIOPLAN_SF.replaceAll("id_sf", idSf));
            
                if(detCsp.getSize()>0){
                     for(SObject cspSf : detCsp.getRecords()){
                         cotSitioPlan = new CotSitioPlan();
                         Cotizacion detalleCotizacion = new Cotizacion();
                         CotSitio detalleCotSitio = new CotSitio();
                         CuentaFactura detalleCuentaFactura = new CuentaFactura();
                         Os detalleOs = new Os();
                         
                         Contacto creadoPor = cargaCreador(cspSf);
                         Contacto editadoPor = cargaModificador(cspSf);
                         Contacto propietario = cargaPropietario(cspSf);
                         
                         if(Constantes.UTILS.validateDataSF(cspSf, "Cot_Sitio__r")){
                             SObject cotSitioSF = (SObject) cspSf.getField("Cot_Sitio__r");
                             detalleCotSitio.setId(Constantes.UTILS.cleanString((String) cotSitioSF.getField("Id")));
                             detalleCotSitio.setNombre(Constantes.UTILS.cleanString((String) cotSitioSF.getField("Name")));
                             detalleCotSitio.setKeyObject(Constantes.TYPE_COTSITIO);
                         }
                         
                         if(Constantes.UTILS.validateDataSF(cspSf, "CuentaFactura__r")){
                             SObject cuentaFacturaSf = (SObject) cspSf.getField("CuentaFactura__r");
                             detalleCuentaFactura.setId(Constantes.UTILS.cleanString((String) cuentaFacturaSf.getField("Id")));
                             detalleCuentaFactura.setNombre(Constantes.UTILS.cleanString((String) cuentaFacturaSf.getField("Name")));
                             detalleCuentaFactura.setKeyObject(Constantes.TYPE_CUENTAFACTURA);
                         }
                         
                         if(Constantes.UTILS.validateDataSF(cspSf, "Cotizacion__r")){
                             SObject cotizacionSf = (SObject) cspSf.getField("Cotizacion__r");
                             detalleCotizacion.setId(Constantes.UTILS.cleanString((String) cotizacionSf.getField("Id")));
                             detalleCotizacion.setNombre(Constantes.UTILS.cleanString((String) cotizacionSf.getField("Name")));
                             detalleCotizacion.setKeyObject(Constantes.TYPE_COTIZACION);
                         }
                         
                         if(Constantes.UTILS.validateDataSF(cspSf, "OrdenServicio__r")){
                             SObject osSf = (SObject) cspSf.getField("OrdenServicio__r");
                             detalleOs.setId(Constantes.UTILS.cleanString((String) osSf.getField("Id")));
                             detalleOs.setNombre(Constantes.UTILS.cleanString((String) osSf.getField("Name")));
                             detalleOs.setKeyObject(Constantes.TYPE_OS);
                         }
                         
                         
                         cotSitioPlan.setId(Constantes.UTILS.cleanString((String) cspSf.getField("Id")));
                         cotSitioPlan.setNombre_csp(Constantes.UTILS.cleanString((String) cspSf.getField("Name")));
                         cotSitioPlan.setEstatusActivacion(Constantes.UTILS.cleanString((String) cspSf.getField("EstatusActivacion__c")));
                         cotSitioPlan.setFecha_aprovisionamiento(Constantes.UTILS.cleanString((String) cspSf.getField("FechaAprovisionamiento__c")));
                         cotSitioPlan.setTipoPlan(Constantes.UTILS.cleanString((String) cspSf.getField("Dp_Familia__c")));
                         cotSitioPlan.setPaquete(Constantes.UTILS.cleanString((String) cspSf.getField("NombrePlan__c")));
                         cotSitioPlan.setTipoRegistroCf(Constantes.UTILS.cleanString((String) cspSf.getField("Tipo_de_registro_CF__c")));
                         cotSitioPlan.setTipo_Oportunidad(Constantes.UTILS.cleanString((String) cspSf.getField("Tipo_Oportunidad__c")));
                         cotSitioPlan.setFecha_cierre(Constantes.UTILS.cleanString((String) cspSf.getField("TSganada__c")));
                         cotSitioPlan.setSobrePrecio(Constantes.UTILS.cleanString((String) cspSf.getField("Sobre_Precio__c")));
                         cotSitioPlan.setAccesoPrincipal(Constantes.UTILS.cleanString((String) cspSf.getField("AcessoPrincipal__c")));
                         cotSitioPlan.setAprovisionamientoTe(Constantes.UTILS.cleanString((String) cspSf.getField("aprovisionamientoTE__c")));
                         cotSitioPlan.setOportunidad_Tipo(Constantes.UTILS.cleanString((String) cspSf.getField("Oportunidad_Tipo__c")));
                         cotSitioPlan.setNombreCuentaComercial(Constantes.UTILS.cleanString((String) cspSf.getField("NombreCuentaComercial__c")));
                         cotSitioPlan.setNombreOportunidad(Constantes.UTILS.cleanString((String) cspSf.getField("NombreOportunidad__c")));
                         cotSitioPlan.setTipoCotizacionPM(Constantes.UTILS.cleanString((String) cspSf.getField("TipoCotizacionPM__c")));
                         cotSitioPlan.setCuandrilla(Constantes.UTILS.cleanString((String) cspSf.getField("CuadrillaFFM__c")));
                         
                         cotSitioPlan.setDetalleCotizacion(detalleCotizacion);
                         cotSitioPlan.setDetalleCotSitio(detalleCotSitio);
                         cotSitioPlan.setDetalleCuentaFactura(detalleCuentaFactura);
                         cotSitioPlan.setDetalleOs(detalleOs);
                         cotSitioPlan.setCreadoPor(creadoPor);
                         cotSitioPlan.setEditadoPor(editadoPor);
                         cotSitioPlan.setPropietario(propietario);
                         
                     }
                 } 
        
        }catch(Exception ex){
            System.err.println("Error al consultar csp sf : " + ex.toString());
        }
        
        return cotSitioPlan;        
    }
    
    public static CuentaFactura detalleCuentaFactura(String idSf){
        
        CuentaFactura cuentaFactura = null;
        
        try{
            
            QueryResult detCuentaFactura = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_CUENTAFACTURA_SF.replaceAll("id_sf", idSf));
            
                if(detCuentaFactura.getSize()>0){
                    for(SObject cf : detCuentaFactura.getRecords()){
                        cuentaFactura = new CuentaFactura();
                        LocalizacionInstalacion detalleGeograficoInstalacion = new LocalizacionInstalacion();
                        Cuenta detalleCuenta = new Cuenta();
                        Contacto creadoPor = cargaCreador(cf);
                        Contacto editadoPor = cargaModificador(cf);
                        Contacto propietario = cargaPropietario(cf);
                        
                        QueryResult arrOs = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_OS_CF.replaceAll("id_cf_sf", (String) cf.getField("Id")));
                        QueryResult arrTicket = Constantes.CRUD_SF.query_sf(Constantes.Q_ARR_TK_CF.replaceAll("id_cf_sf", (String) cf.getField("Id")));
                        
                        cuentaFactura.setId(Constantes.UTILS.cleanString((String) cf.getField("Id")));
                        cuentaFactura.setNombre(Constantes.UTILS.cleanString((String) cf.getField("Name")));
                        cuentaFactura.setNumCuentaFactura(Constantes.UTILS.cleanString((String) cf.getField("IdCuentaBRM__c")));
                        cuentaFactura.setNombreResponsableComercial(Constantes.UTILS.cleanString((String) cf.getField("ResponsableCuentaComercial__c")));
                        cuentaFactura.setCelular(Constantes.UTILS.cleanString((String) cf.getField("Celular__c")));
                        cuentaFactura.setTelefonoPrincipal(Constantes.UTILS.cleanString((String) cf.getField("TelefonoPrincipal__c")));
                        cuentaFactura.setEmail(Constantes.UTILS.cleanString((String) cf.getField("Email__c")));
                        cuentaFactura.setEmailFacturacion1(Constantes.UTILS.cleanString((String) cf.getField("EmailFacturacion1__c")));
                        cuentaFactura.setEmailFacturacion2(Constantes.UTILS.cleanString((String) cf.getField("EmailFacturacion2__c")));
                        cuentaFactura.setFacebook(Constantes.UTILS.cleanString((String) cf.getField("Facebook__c")));
                        cuentaFactura.setTwitter(Constantes.UTILS.cleanString((String) cf.getField("Twitter__c")));
                        cuentaFactura.setLinkedin(Constantes.UTILS.cleanString((String) cf.getField("Linkedin__c")));
                        cuentaFactura.setRfc(Constantes.UTILS.cleanString((String) cf.getField("RFC__c")));
                        cuentaFactura.setPortabilidad(Constantes.UTILS.cleanString((String) cf.getField("Portabilidad__c")));
                        cuentaFactura.setFechaVencimiento(Constantes.UTILS.cleanString((String) cf.getField("Fecha_vencimiento__c")));
                        cuentaFactura.setTipoFacturacion(Constantes.UTILS.cleanString((String) cf.getField("TipoFacturacion__c")));
                        cuentaFactura.setPlazo(Constantes.UTILS.cleanString((String) cf.getField("Plazo__c")));
                        cuentaFactura.setFechaActivacion(Constantes.UTILS.cleanString((String) cf.getField("Fecha_activacion__c")));
                        cuentaFactura.setFolioVenta(Constantes.UTILS.cleanString((String) cf.getField("FolioVenta__c")));
                        cuentaFactura.setFormaPago(Constantes.UTILS.cleanString((String) cf.getField("Forma_de_pago__c")));
                        cuentaFactura.setTipoPago(Constantes.UTILS.cleanString((String) cf.getField("TipoPago__c")));
                        cuentaFactura.setPlanServicio(Constantes.UTILS.cleanString((String) cf.getField("PlanServicio__c")));
                        cuentaFactura.setPlanPostVenta(Constantes.UTILS.cleanString((String) cf.getField("PlanPostVenta__c")));
                        cuentaFactura.setRentaMensualPL(Constantes.UTILS.cleanString((String) cf.getField("Renta_mensual_PL__c")));
                        cuentaFactura.setRentaMensualPP(Constantes.UTILS.cleanString((String) cf.getField("Renta_mensual_PP__c")));
                        cuentaFactura.setActivada(Constantes.UTILS.cleanString((String) cf.getField("Activada__c")));
                        
                        detalleGeograficoInstalacion.setRegionInstalacionId(Constantes.UTILS.cleanString((String) cf.getField("RegionInstalacionId__c")));
                        detalleGeograficoInstalacion.setRegionInstalacion(Constantes.UTILS.cleanString((String) cf.getField("RegionInstalacion__c")));
                        detalleGeograficoInstalacion.setPlazaInstalacion(Constantes.UTILS.cleanString((String) cf.getField("Plaza__c")));
                        detalleGeograficoInstalacion.setDistritoInstalacion(Constantes.UTILS.cleanString((String) cf.getField("DistritoInstalacion__c")));
                        detalleGeograficoInstalacion.setZonaInstalacion(Constantes.UTILS.cleanString((String) cf.getField("ZonaInstalacion__c")));
                        detalleGeograficoInstalacion.setClusterInstalacion(Constantes.UTILS.cleanString((String) cf.getField("ClusterInstalacion__c")));
                        detalleGeograficoInstalacion.setCiudadInstalacion(Constantes.UTILS.cleanString((String) cf.getField("CiudadInstalacion__c")));
                        detalleGeograficoInstalacion.setEstadoInstalacion(Constantes.UTILS.cleanString((String) cf.getField("EstadoInstalacion__c")));
                        detalleGeograficoInstalacion.setDelegacionInstalacion(Constantes.UTILS.cleanString((String) cf.getField("DelegacionInstalacion__c")));
                        detalleGeograficoInstalacion.setColoniaInstalacion(Constantes.UTILS.cleanString((String) cf.getField("ColoniaInstalacion__c")));
                        detalleGeograficoInstalacion.setCalleInstalacion(Constantes.UTILS.cleanString((String) cf.getField("CalleInstalacion__c")));
                        detalleGeograficoInstalacion.setNumeroExteriorInstalacion(Constantes.UTILS.cleanString((String) cf.getField("NumeroExterior__c")));
                        detalleGeograficoInstalacion.setNumeroInteriorInstalacion(Constantes.UTILS.cleanString((String) cf.getField("NumeroInteriorInstalacion__c")));
                        detalleGeograficoInstalacion.setCpInstalacion(Constantes.UTILS.cleanString((String) cf.getField("CodigoPostalInstalacion__c")));
                        detalleGeograficoInstalacion.setLatitudeInstalacion(Constantes.UTILS.cleanString((String) cf.getField("GeolocalizacionInstalacion__Latitude__s")));
                        detalleGeograficoInstalacion.setLongitudeInstalacion(Constantes.UTILS.cleanString((String) cf.getField("GeolocalizacionInstalacion__Longitude__s")));
                        
                        if(Constantes.UTILS.validateDataSF(cf, "IdCuenta__r")){
                            SObject cuentasf = (SObject) cf.getField("IdCuenta__r");
                            detalleCuenta.setId(Constantes.UTILS.cleanString((String) cuentasf.getField("Id")));
                            detalleCuenta.setNombre(Constantes.UTILS.cleanString((String) cuentasf.getField("Name")));
                            detalleCuenta.setKeyObject(Constantes.TYPE_CUENTA);
                        }    
                        
                        cuentaFactura.setArrOs(cargaArregloOs(arrOs));
                        cuentaFactura.setArrTicket(cargaArregloTicket(arrTicket));
                        cuentaFactura.setDetalleGeograficoInstalacion(detalleGeograficoInstalacion);
                        cuentaFactura.setDetalleCuenta(detalleCuenta);
                        cuentaFactura.setCreadoPor(creadoPor);
                        cuentaFactura.setEditadoPor(editadoPor);
                        cuentaFactura.setPropietario(propietario);
                    }
                }
        
        }catch(Exception ex){
            System.err.println("Error al consultar cuenta factura sf : " + ex.toString());
        }
        
        return cuentaFactura;
    }
    
    public static Os detalleOs(String idSf){
        
        Os os = null;
        
        try{
            
            QueryResult detOs = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_OS_SF.replaceAll("id_sf", idSf));
            
                if(detOs.getSize()>0){
                    for(SObject osSf : detOs.getRecords()){
                        os = new Os();
                        Contacto creadoPor = cargaCreador(osSf);
                        Oportunidad detalleOportunidad = new Oportunidad();
                        Cotizacion detalleCotizacion = new Cotizacion();
                        Sitio detalleSitio = new Sitio();
                        CotSitioPlan detalleCotSitioPlan = new CotSitioPlan();
                        CuentaFactura detalleCuentaFactura = new CuentaFactura();
                        Contacto editadoPor = cargaModificador(osSf);
                        Contacto propietario = cargaPropietario(osSf);
                        
                        os.setId(Constantes.UTILS.cleanString((String) osSf.getField("Id")));
                        os.setNombre(Constantes.UTILS.cleanString((String) osSf.getField("Name")));
                        os.setEstatus(Constantes.UTILS.cleanString((String) osSf.getField("Estatus__c")));
                        os.setMotivoCancelacion(Constantes.UTILS.cleanString((String) osSf.getField("Motivo_Cancelacion__c")));
                        os.setTSCompletado(Constantes.UTILS.cleanString((String) osSf.getField("TSCompletado__c")));
                        os.setTSConfirmado(Constantes.UTILS.cleanString((String) osSf.getField("TSConfirmado__c")));
                        os.setPropietarioOportunidad(Constantes.UTILS.cleanString((String) osSf.getField("Propietario_Oportunidad__c")));
                        os.setIdOt(Constantes.UTILS.cleanString((String) osSf.getField("IdOtGIM__c")));
                        os.setCanalVenta(Constantes.UTILS.cleanString((String) osSf.getField("Canal_de_Venta__c")));
                        os.setFechaAgendada(Constantes.UTILS.cleanString((String) osSf.getField("FechaAgendada__c")));
                        os.setTurnoAg(Constantes.UTILS.cleanString((String) osSf.getField("TurnoAg__c")));
                        os.setOsConfirmada(Constantes.UTILS.cleanString((String) osSf.getField("Confirmada__c")));
                        os.setTipoOrden(Constantes.UTILS.cleanString((String) osSf.getField("Tipo_Orden__c")));
                        os.setComentariosOs(Constantes.UTILS.cleanString((String) osSf.getField("ComentariosGIM__c")));
                        
                        detalleSitio.setCalle(Constantes.UTILS.cleanString((String) osSf.getField("Calle__c"))); 
                        detalleSitio.setColonia(Constantes.UTILS.cleanString((String) osSf.getField("Colonia__c"))); 
                        detalleSitio.setNumeroExterior(Constantes.UTILS.cleanString((String) osSf.getField("Num_exterior__c"))); 
                        detalleSitio.setNumeroInterior(Constantes.UTILS.cleanString((String) osSf.getField("Num_interior__c"))); 
                        detalleSitio.setDelegacionMunicipio(Constantes.UTILS.cleanString((String) osSf.getField("Delegacion_Municipio__c")));
                        detalleSitio.setEstado(Constantes.UTILS.cleanString((String) osSf.getField("Estado__c"))); 
                        detalleSitio.setRegion(Constantes.UTILS.cleanString((String) osSf.getField("Region__c"))); 
                        detalleSitio.setPlaza(Constantes.UTILS.cleanString((String) osSf.getField("Plaza__c"))); 
                        detalleSitio.setCiudad(Constantes.UTILS.cleanString((String) osSf.getField("Ciudad__c")));
                        detalleSitio.setDistrito(Constantes.UTILS.cleanString((String) osSf.getField("Distrito__c")));
                        detalleSitio.setZona(Constantes.UTILS.cleanString((String) osSf.getField("Zona__c")));
                        detalleSitio.setCluster(Constantes.UTILS.cleanString((String) osSf.getField("Cluster__c")));
                        detalleSitio.setCodigoPostal(Constantes.UTILS.cleanString((String) osSf.getField("CodigoPostal__c")));
                        detalleSitio.setLatitude(Constantes.UTILS.cleanString((String) osSf.getField("Latitud__c")));
                        detalleSitio.setLongitude(Constantes.UTILS.cleanString((String) osSf.getField("Longitud__c")));
                        
                        if(Constantes.UTILS.validateDataSF(osSf, "Oportunidad__r")){
                            SObject opor = (SObject) osSf.getField("Oportunidad__r");
                            detalleOportunidad.setId(Constantes.UTILS.cleanString((String) opor.getField("Id")));
                            detalleOportunidad.setNombre(Constantes.UTILS.cleanString((String) opor.getField("Name")));
                            detalleOportunidad.setKeyObject(Constantes.TYPE_OPORTUNIDAD);
                        }
                        
                        if(Constantes.UTILS.validateDataSF(osSf, "Cotizacion__r")){
                            SObject cot = (SObject) osSf.getField("Cotizacion__r");
                            detalleCotizacion.setId(Constantes.UTILS.cleanString((String) cot.getField("Id")));
                            detalleCotizacion.setNombre(Constantes.UTILS.cleanString((String) cot.getField("Name")));
                            detalleCotizacion.setKeyObject(Constantes.TYPE_COTIZACION);
                        }
                        
                        if(Constantes.UTILS.validateDataSF(osSf, "BCot_SitioPlan__r")){
                            SObject csp = (SObject) osSf.getField("BCot_SitioPlan__r");
                            detalleCotSitioPlan.setId(Constantes.UTILS.cleanString((String) csp.getField("Id")));
                            detalleCotSitioPlan.setNombre(Constantes.UTILS.cleanString((String) csp.getField("Name")));
                            detalleCotSitioPlan.setKeyObject(Constantes.TYPE_COTSITIOPLAN);
                        }
                        
                        if(Constantes.UTILS.validateDataSF(osSf, "CuentaFactura__r")){
                            SObject cuentafac = (SObject) osSf.getField("CuentaFactura__r");
                            detalleCuentaFactura.setId(Constantes.UTILS.cleanString((String) cuentafac.getField("Id")));
                            detalleCuentaFactura.setNombre(Constantes.UTILS.cleanString((String) cuentafac.getField("Name")));
                            detalleCuentaFactura.setKeyObject(Constantes.TYPE_CUENTAFACTURA);
                        }
                        
                        os.setDetalleOportunidad(detalleOportunidad);
                        os.setDetalleCotizacion(detalleCotizacion);
                        os.setDetalleSitio(detalleSitio);
                        os.setDetalleCotSitioPlan(detalleCotSitioPlan);
                        os.setDetalleCuentaFactura(detalleCuentaFactura);
                        
                        os.setCreadoPor(creadoPor);
                        os.setEditadoPor(editadoPor);
                        os.setPropietario(propietario);
                    }
                }
            
        }catch(Exception ex){
            System.err.println("Error al consultar os sf : " + ex.toString());
        }
        
        return os;
    }
    
    public static Ticket detalleTicket(String idSf){
        
        Ticket ticket = null;
        
        try{
            
            QueryResult detTicket = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_TK_SF.replaceAll("id_sf", idSf));
            
                if(detTicket.getSize()>0){
                    for(SObject ticketSf : detTicket.getRecords()){
                        ticket = new Ticket();
                        CuentaFactura detalleCuentaFactura = new CuentaFactura();
                        Os detalleOs = new Os();
                        Contacto creadoPor = cargaCreador(ticketSf);
                        Contacto editadoPor = cargaModificador(ticketSf);
                        Contacto propietario = cargaPropietario(ticketSf);
                        
                        ticket.setId(Constantes.UTILS.cleanString((String) ticketSf.getField("Id")));
                        ticket.setCaseNumber(Constantes.UTILS.cleanString((String) ticketSf.getField("CaseNumber")));
                        ticket.setNivel1(Constantes.UTILS.cleanString((String) ticketSf.getField("Nivel1__c")));
                        ticket.setNivel2(Constantes.UTILS.cleanString((String) ticketSf.getField("Nivel2__c")));
                        ticket.setNivel3(Constantes.UTILS.cleanString((String) ticketSf.getField("Nivel3__c")));
                        ticket.setStatus(Constantes.UTILS.cleanString((String) ticketSf.getField("Status")));
                        ticket.setMotivo(Constantes.UTILS.cleanString((String) ticketSf.getField("Motivo__c")));
                        ticket.setSolucionOT(Constantes.UTILS.cleanString((String) ticketSf.getField("SolucionOT__c")));
                        ticket.setPrimerFechaAgendamiento(Constantes.UTILS.cleanString((String) ticketSf.getField("PrimerFechaAgendamiento__c")));
                        ticket.setFechaAgendamiento(Constantes.UTILS.cleanString((String) ticketSf.getField("FechaAgendamiento__c")));
                        ticket.setOrigin(Constantes.UTILS.cleanString((String) ticketSf.getField("Origin")));
                        ticket.setAsunto(Constantes.UTILS.cleanString((String) ticketSf.getField("Subject")));
                        ticket.setDescription(Constantes.UTILS.cleanString((String) ticketSf.getField("Description")));
                        ticket.setFolioSd(Constantes.UTILS.cleanString((String) ticketSf.getField("FolioSD__c")));
                        ticket.setComentariosSd(Constantes.UTILS.cleanString((String) ticketSf.getField("ComentariosSD__c")));
                        ticket.setCreatedDate(Constantes.UTILS.cleanString((String) ticketSf.getField("CreatedDate")));
                        
                        if(Constantes.UTILS.validateDataSF(ticketSf, "CuentaFactura__r")){
                            SObject cuentaFactura = (SObject) ticketSf.getField("CuentaFactura__r");
                            detalleCuentaFactura.setId(Constantes.UTILS.cleanString((String) cuentaFactura.getField("Id")));
                            detalleCuentaFactura.setNombre(Constantes.UTILS.cleanString((String) cuentaFactura.getField("Name")));
                            detalleCuentaFactura.setKeyObject(Constantes.TYPE_CUENTAFACTURA);
                        }
                        
                        if(Constantes.UTILS.validateDataSF(ticketSf, "idOS__r")){
                            SObject oss = (SObject) ticketSf.getField("idOS__r");
                            detalleOs.setId(Constantes.UTILS.cleanString((String) oss.getField("Id")));
                            detalleOs.setNombre(Constantes.UTILS.cleanString((String) oss.getField("Name")));
                            detalleOs.setKeyObject(Constantes.TYPE_OS);
                        }
                        
                        ticket.setDetalleCuentaFactura(detalleCuentaFactura);
                        ticket.setDetalleOs(detalleOs);
                        ticket.setCreadoPor(creadoPor);
                        ticket.setEditadoPor(editadoPor);
                        ticket.setPropietario(propietario);
                    }
                }
        
        }catch(Exception ex){
            System.err.println("Error al cargar detalle ticket : " + ex.toString());
        }
        return ticket;
    }
    
    public static Contacto detalleContacto(String idSf){
        Contacto contacto = null;
        
        try{
            
            QueryResult detUsuario = Constantes.CRUD_SF.query_sf(Constantes.Q_DETALLE_USUARIO.replaceAll("id_sf", idSf));
            
                if(detUsuario.getSize()>0){
                    for(SObject user : detUsuario.getRecords()){
                        contacto = new Contacto();
                        contacto.setNombre(Constantes.UTILS.cleanString((String) user.getField("Name")));
                        contacto.setTelefono(Constantes.UTILS.cleanString((String) user.getField("Phone")));
                        contacto.setEmail(Constantes.UTILS.cleanString((String) user.getField("Email")));
                        contacto.setPuesto(Constantes.UTILS.cleanString((String) user.getField("Title")));
                        contacto.setUrlFoto(Constantes.UTILS.cleanString((String) user.getField("FullPhotoUrl")));
                        
                        if(Constantes.UTILS.validateDataSF(user, "Manager")){
                            SObject  managerSf = (SObject) user.getField("Manager");
                            contacto.setIdJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Id")));
                            contacto.setNombreJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Name")));
                        }
                    }    
                }
            
        }catch(Exception ex){
            System.err.println("Error al cargar creador : " + ex.toString());    
        }
        
        return contacto;
    }
    
    private static Contacto cargaCreador(SObject objecSf){
        
        Contacto creador = new Contacto();
        
        try{
            
            if(Constantes.UTILS.validateDataSF(objecSf, "CreatedBy")){
                SObject  creadoPorSf = (SObject) objecSf.getField("CreatedBy");
                creador.setNombre(Constantes.UTILS.cleanString((String) creadoPorSf.getField("Name")));
                creador.setTelefono(Constantes.UTILS.cleanString((String) creadoPorSf.getField("Phone")));
                creador.setEmail(Constantes.UTILS.cleanString((String) creadoPorSf.getField("Email")));
                creador.setPuesto(Constantes.UTILS.cleanString((String) creadoPorSf.getField("Title")));
                creador.setUrlFoto(Constantes.UTILS.cleanString((String) creadoPorSf.getField("FullPhotoUrl")));
                
                if(Constantes.UTILS.validateDataSF(creadoPorSf, "Manager")){
                    SObject  managerSf = (SObject) creadoPorSf.getField("Manager");
                    creador.setIdJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Id")));
                    creador.setNombreJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Name")));
                    creador.setKeyObject(Constantes.TYPE_USER);
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar creador : " + ex.toString());    
        }
        
        return creador;        
    }
    
    private static Contacto cargaModificador(SObject objecSf){
        
        Contacto modificador = new Contacto();
        
        try{
            if(Constantes.UTILS.validateDataSF(objecSf, "LastModifiedBy")){
                SObject  modificadoPorSf = (SObject) objecSf.getField("LastModifiedBy");
                modificador.setNombre(Constantes.UTILS.cleanString((String) modificadoPorSf.getField("Name")));
                modificador.setTelefono(Constantes.UTILS.cleanString((String) modificadoPorSf.getField("Phone")));
                modificador.setEmail(Constantes.UTILS.cleanString((String) modificadoPorSf.getField("Email")));
                modificador.setPuesto(Constantes.UTILS.cleanString((String) modificadoPorSf.getField("Title")));
                modificador.setUrlFoto(Constantes.UTILS.cleanString((String) modificadoPorSf.getField("FullPhotoUrl")));
                
                if(Constantes.UTILS.validateDataSF(modificadoPorSf, "Manager")){
                    SObject  managerSf = (SObject) modificadoPorSf.getField("Manager");
                    modificador.setIdJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Id")));
                    modificador.setNombreJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Name")));
                    modificador.setKeyObject(Constantes.TYPE_USER);
                }
            }
        }catch(Exception ex){
            System.err.println("Error al cargar editor : " + ex.toString());    
        }
        
        return modificador;
    }
    
    private static Contacto cargaPropietario(SObject objecSf){
        
        Contacto propietario = new Contacto();
        
        try{
            if(Constantes.UTILS.validateDataSF(objecSf, "Owner")){
                SObject  propietarioSf = (SObject) objecSf.getField("Owner");
                propietario.setNombre(Constantes.UTILS.cleanString((String) propietarioSf.getField("Name")));
                propietario.setTelefono(Constantes.UTILS.cleanString((String) propietarioSf.getField("Phone")));
                propietario.setEmail(Constantes.UTILS.cleanString((String) propietarioSf.getField("Email")));
                propietario.setPuesto(Constantes.UTILS.cleanString((String) propietarioSf.getField("Title")));
                propietario.setUrlFoto(Constantes.UTILS.cleanString((String) propietarioSf.getField("FullPhotoUrl")));
                
                if(Constantes.UTILS.validateDataSF(propietarioSf, "Manager")){
                    SObject  managerSf = (SObject) propietarioSf.getField("Manager");
                    propietario.setIdJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Id")));
                    propietario.setNombreJefe(Constantes.UTILS.cleanString((String) managerSf.getField("Name")));
                    propietario.setKeyObject(Constantes.TYPE_USER);
                }
            }
        }catch(Exception ex){
            System.err.println("Error al cargar propietario : " + ex.toString());    
        }
        
        return propietario;
    }
    
    private static Contacto cargaContactoPrincipal(SObject objecSf){
        
        Contacto contacto = new Contacto();
        
        try{
            if(Constantes.UTILS.validateDataSF(objecSf, "ContactoPrincipal__r")){
                SObject  contactoSf = (SObject) objecSf.getField("ContactoPrincipal__r");
                contacto.setNombre(Constantes.UTILS.cleanString((String) contactoSf.getField("NombreCompleto__c")));
                contacto.setTelefono(Constantes.UTILS.cleanString((String) contactoSf.getField("Phone")));
                contacto.setCelular(Constantes.UTILS.cleanString((String) contactoSf.getField("MobilePhone")));
                contacto.setEmail(Constantes.UTILS.cleanString((String) contactoSf.getField("Email")));
            }
        }catch(Exception ex){
            System.err.println("Error al crear contacto principal : " + ex.toString());
        }
        
        return contacto;    
    }
    
    private static Oportunidad[] cargaArregloOportunidades(QueryResult oportunidadesSF){
        
        Oportunidad arrOportunidad[] = new Oportunidad[oportunidadesSF.getSize()];
        
        try{
            
            if(oportunidadesSF.getSize()>0){
                int cont = 0;
                for(SObject oportunidadSf : oportunidadesSF.getRecords()){
                    Oportunidad oportunidad = new Oportunidad();
                    oportunidad.setId((String) oportunidadSf.getField("Id"));
                    oportunidad.setNombre((String) oportunidadSf.getField("Name"));
                    oportunidad.setNumeroOportunidad((String) oportunidadSf.getField("NumeroOportunidad__c"));
                    oportunidad.setFechaCierre((String) oportunidadSf.getField("CloseDate"));
                    oportunidad.setKeyObject(Constantes.TYPE_OPORTUNIDAD);
                    arrOportunidad[cont++] = oportunidad;
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo oportunidades : " + ex.toString());
        }
        
        return arrOportunidad;
    }
    
    private static CuentaFactura[] cargaArregloCuentasFactura(QueryResult cuentaFacturaSf){
        
        CuentaFactura arrCuentaFactura[] = new CuentaFactura[cuentaFacturaSf.getSize()];
        
        try{
            
            if(cuentaFacturaSf.getSize()>0){
                int cont = 0;
                for(SObject cuentaFacSf : cuentaFacturaSf.getRecords()){
                    CuentaFactura cuentaFactura = new CuentaFactura();
                    cuentaFactura.setId((String) cuentaFacSf.getField("Id"));
                    cuentaFactura.setNombre((String) cuentaFacSf.getField("Name"));
                    cuentaFactura.setNumCuentaFactura((String) cuentaFacSf.getField("IdCuentaBRM__c"));
                    cuentaFactura.setFolioVenta((String) cuentaFacSf.getField("FolioVenta__c"));
                    cuentaFactura.setKeyObject(Constantes.TYPE_CUENTAFACTURA);
                    arrCuentaFactura[cont++] = cuentaFactura;
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo CuentaFactura : " + ex.toString());
        }
        
        return arrCuentaFactura;
    }
    
    private static CotSitioPlan[] cargaArregloCotSitioPlan(QueryResult cotSitioPlanSf){
        
        CotSitioPlan arrCotSitioPlan[] = new CotSitioPlan[cotSitioPlanSf.getSize()];
        
        try{
            
            if(cotSitioPlanSf.getSize()>0){
                int cont = 0;
                for(SObject cspsf : cotSitioPlanSf.getRecords()){
                    CotSitioPlan csp = new CotSitioPlan();
                    csp.setId((String) cspsf.getField("Id"));
                    csp.setNombre((String) cspsf.getField("Name"));
                    csp.setNombrePlan((String) cspsf.getField("NombrePlan__c"));
                    csp.setFecha_cierre((String) cspsf.getField("TSganada__c"));
                    csp.setKeyObject(Constantes.TYPE_COTSITIOPLAN);
                    arrCotSitioPlan[cont++] = csp;
                }
            }
            
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo CotSitioPlan : " + ex.toString());
        }
        
        return arrCotSitioPlan;
    }
    
    private static CotSitio[] cargaArregloCotSitio(QueryResult cotSitioSf){
        
        CotSitio arrCotSitio[] = new CotSitio[cotSitioSf.getSize()];
        
        try{
            
            if(cotSitioSf.getSize()>0){
                int cont = 0;
                for(SObject csSf : cotSitioSf.getRecords()){
                    CotSitio cs = new CotSitio();
                    cs.setId((String) csSf.getField("Id"));
                    cs.setNombre((String) csSf.getField("Name"));
                    cs.setDireccionSitio((String) csSf.getField("DireccionSitio__c"));
                    cs.setSubtotalRenta((String) csSf.getField("SubtotalRenta__c"));
                    cs.setTotalRentaConImpuesto((String) csSf.getField("TotalRenta_ConImpuesto__c"));
                    cs.setKeyObject(Constantes.TYPE_COTSITIO);
                    arrCotSitio[cont++] = cs;
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo CotSitio : " + ex.toString());
        }
        
        return arrCotSitio;
    }
    
    private static Os[] cargaArregloOs(QueryResult osSf){
        
        Os arrOs[] = new Os[osSf.getSize()];
        
        try{
            
            if(osSf.getSize()>0){
                int cont = 0;
                for(SObject osEnSf : osSf.getRecords()){
                    Os os = new Os();
                    os.setId((String) osEnSf.getField("Id"));
                    os.setNombre((String) osEnSf.getField("Name"));
                    os.setFechaAgendada((String) osEnSf.getField("FechaAgendada__c"));
                    os.setTurnoAg((String) osEnSf.getField("TurnoAg__c"));
                    os.setOsConfirmada((String) osEnSf.getField("Confirmada__c"));
                    os.setEstatus((String) osEnSf.getField("Estatus__c"));
                    os.setKeyObject(Constantes.TYPE_OS);
                    arrOs[cont++]=os;
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo Os : " + ex.toString());
        }
        
        return arrOs;
    }
    
    private static Ticket[] cargaArregloTicket(QueryResult ticketSf){
        
        Ticket arrTicket[] = new Ticket[ticketSf.getSize()];
        
        try{
            
            if(ticketSf.getSize()>0){
                int cont = 0;
                for(SObject tickSf : ticketSf.getRecords()){
                    Ticket tiket = new Ticket();
                    tiket.setId((String) tickSf.getField("Id"));
                    tiket.setCaseNumber((String) tickSf.getField("CaseNumber"));
                    tiket.setNivel1((String) tickSf.getField("Nivel1__c"));
                    tiket.setNivel2((String) tickSf.getField("Nivel2__c"));
                    tiket.setNivel3((String) tickSf.getField("Nivel3__c"));
                    tiket.setCreatedDate((String) tickSf.getField("CreatedDate"));
                    tiket.setStatus((String) tickSf.getField("Status"));
                    tiket.setKeyObject(Constantes.TYPE_TK);
                    arrTicket[cont++] = tiket;
                }
            }
        
        }catch(Exception ex){
            System.err.println("Error al cargar arreglo Ticket : " + ex.toString());
        }
        
        return arrTicket;
    }
    
}
